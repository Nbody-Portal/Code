// std headers
#include <iostream>
#include <string>

// ginac headers
#include <ginac/ginac.h>
#include <ginac/flags.h>

// Nbody headers
#include "Func.h"
#include "Nbody.h"
#include "Function.h"
#include "IRPrinter.h"

using namespace std;
using namespace GiNaC;

#ifndef KC_HPP
#define KC_HPP
////////////////////
// Parser Section //
////////////////////

static ex ret_contents(const ex &x){
	return x;
}

static ex sqrt_(const ex &x){
	return pow(x,0.5);
}

static ex rsqrt_(const ex &x){
	return pow(x,-0.5);
}

static ex pow_(const ex &x,const ex &x2){
	return pow(x,x2);
}


class KernelChecker{
	
	private:
		ex threshold;
		symbol x;
		
		Nbody::Func kernel;
		ex func;
		symtab table;
		
		int start;
		int end;
		int step;

		////////////////////////////
		// Function Test Section ///
		////////////////////////////
		
		// check if input ex is within threashold
		// input diff should be a numerical value and should be checked before calling this function
		bool checkInThreshold(ex diff);

		bool analyze_deriv();

		// checks to see if limit of a given function aproaches a constant value or infinity
		// currently only works with 2 variables
		bool analyze_limit();

		bool analyze_integral();
		
		void baseSetup(){
			x = symbol("x");
			start = 1;
			end = 100;
			step = 1;
			threshold = 0.001;
			
		}
		
	public:
		KernelChecker(Nbody::Func &f){
			baseSetup();
			kernel = f;
		}
		
		KernelChecker(){
			baseSetup();
		}
		
		void setFunc(Nbody::Func f){
			kernel = f;
		}
		
		void setThreshold(ex thresh){
			threshold = thresh;
		}
		
		void setStart(int s){
			start = s;
		}
		
		void setEnd(int e){
			end = e;
		}
		
		void setStep(int s){
			step = s;
		}
		
		void setupTable();
		
		bool check();
		
		bool test();
		



};

#endif