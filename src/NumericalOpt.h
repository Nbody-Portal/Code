#ifndef INC_NUMERICAL_OPT_H
#define INC_NUMERICAL_OPT_H

#include "IRPass.h"


/* Defines a pass that applies the Numerical Optimizations */
namespace Nbody {

class NumericalOpt : public IRPass {


   void visit (const Call* op) {

    // Expr b = mutate(op->args);
    if (op->name == "Gaussian") {
      expr = Call::make (op->type, "Gaussian", op->args, Call::Extern);
      // expr = Call::make (op->type, "Gaussian", vec(b), Call::Extern);
    }
    else
      expr = op;

  }

};

inline Stmt Numerical_Optimizations(Stmt s) {
  return NumericalOpt().mutate(s);
}

}
#endif
