#ifndef INC_DATA_H
#define INC_DATA_H

#include <stdlib.h>
#include "buffer_t.h"
#include "Argument.h"
#include "Util.h"

/* A wrapper around buffer_t */

namespace Nbody {

  struct DataContents {

    /* The buffer_t object */
    buffer_t d;

    /* The type of the data */
    Type type;

    /* Allocate memory for the data */
    uint8_t* alloc;

    /* Number of Data objects pointing to DataContents */
    mutable RefCount count;

    /* Name of the data item */
    std::string name;

    DataContents(Type t, int m, int n, uint8_t* data, const std::string& name) :
      type(t), alloc(NULL), name(name.empty() ? generate_name('b') : name) {
        uint64_t size = 1;
        if(m)
          size *= m;
        if(n)
          size *= n;
        d.elem_size = t.bytes();

        if(!data) {
          alloc = (uint8_t*) calloc(size, t.bytes());
          assert (alloc && "Out of memory");
          d.data = alloc;
        }
        else
          d.data = data;
        d.begin[0] = 0;
        d.begin[1] = 0;
        d.end[0] = m;
        d.end[1] = n;
        d.stride[0] = n;
        d.stride[1] = 1;
        }

    DataContents(Type t, const buffer_t* data, const std::string& name) :
      type(t), alloc(NULL), name(name.empty() ? generate_name('b') : name) {
        d = *data;
      }
  };

class Data {
private:
  Ptr<DataContents> contents;

public:
  Data() : contents(NULL) {}

  Data(Type t, int m = 0, int n = 0, uint8_t* data = NULL, const std::string& name = "") :
    contents(new DataContents(t, m, n, data, name)) {}

  Data(Type t, const buffer_t* d, const std::string& name) :
    contents(new DataContents(t, d, name)) {}

  /* Get a pointer to array in main memory */
  void* data_ptr() const {
    assert(defined() && "Data undefined");
    return (void*)contents.ptr->d.data;
  }

  /* Get a pointer to the raw buffer_t struct */
  buffer_t* raw_buffer() const {
    assert(defined() && "Buffer undefined");
    return &(contents.ptr->d);
  }

  /* Get the number of dimensions. */
  int dims() const {
    if (size(0) == 0)
      return 0;
    else if (size(1) == 0)
      return 1;
    return 2;
  }

  /* Get the base address of the data in each dimension */
  int begin(int dim) const {
    assert(defined() && "Data undefined");
    assert ((dim == 0 || dim == 1) && "We only support upto 2-dimensional arrays");
    return contents.ptr->d.begin[dim];
  }

  /* Get the number of data elements in each dimension */
  int size(int dim) const {
    assert(defined() && "Data undefined");
    assert ((dim == 0 || dim == 1) && "We only support upto 2-dimensional arrays");
    return contents.ptr->d.end[dim];
  }

  /* Get the stride of data elements in each dimension */
  int stride(int dim) const {
    assert(defined() && "Data undefined");
    assert ((dim == 0 || dim == 1) && "We only support upto 2-dimensional arrays");
    return contents.ptr->d.stride[dim];
  }

  /* Get the type of the data element */
  Type type() const {
    return contents.ptr->type;
  }

  /* Compare if two data objects are pointing to the same data */
  bool same_as(const Data& other) const {
    return contents.same_as(other.contents);
  }

  /* Check if the data handle is valid */
  bool defined() const {
    return contents.defined();
  }

  /* Get the name of the data */
  const std::string& name() const {
    return contents.ptr->name;
  }

  /* Convert this data to an argument that can be passed */
  operator Argument() const {
    return Argument(name(), true, type());
  }
};

template<>
inline RefCount& count<DataContents>(const DataContents* d) {
  return d->count;
}

/* Free the memory allocated */
template<>
inline void destroy<DataContents>(const DataContents* d) {
  free(d->alloc);
  delete d;
}

}

#endif
