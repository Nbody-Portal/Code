#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#include <iostream>
#include <fstream>
#include <algorithm>
#include "IRNode.h"
#include "Lower.h"
#include "Func.h"
#include "FlattenDimensions.h"
#include "Simplify.h"
#include "NumericalOpt.h"
#include "Vectorize.h"
#include "StrengthReduction.h"
#include "Inline.h"
#include "OrderedArrayPass.h"
#include "FindCalls.h"
#include "RealizationOrder.h"
#include "ScheduleFunction.h"
#include "BoundsInference.h"
#include "IRPrinter.cpp"


namespace Nbody {

/* Structure containing LetStmt and For loops */
struct Container {
  // Index in the dim list. -1 for LetStmt
  int dim_index;
  std::string name;
  Expr value;
};

Expr qualify_expr(std::string prefix, Expr value) {
  QualifyExpr q(prefix);
  return q.mutate(value);
}

/* Build a loop nest using a schedule. We start inside out, generating
 * a store node, then wrapping the body in for loops */
Stmt build_store_loop_nest(Function f, std::string prefix, const std::vector<Expr>& loc, const Expr value, const Schedule& s) {

  // Make a multi-dimensional store node
  Stmt stmt = MultiStore::make(f.name(), value, loc);

  // Contains LetStmt's and For's
  std::vector<Container> nest;

  // Add the loop nest to the container starting with the outermost loop
  for (int i = (int)s.dim.size() - 1; i >= 0; --i) {
    const Schedule::Dim& dim = s.dim[i];
    DEBUG_MSG("dim var: " << dim.var << std::endl);
    Container c = {i, prefix + dim.var, Expr()};
    nest.push_back(c);
  }

  // Add LetStmt's into the container
  while (const LetStmt* let = stmt.ir<LetStmt>()) {
    Container c = {-1, let->name, let->value};
    nest.push_back(c);
    stmt = let->body;
  }

  DEBUG_MSG("s.dim size: " << s.dim.size() << "\t nest size: " << nest.size() << std::endl);
  // Re-arrange the container to move up LetStmt's
  for (int i = (int)s.dim.size(); i < nest.size(); ++i) {
    assert(nest[i].value.defined());
    for (size_t j = i-1; j >= 0; --j) {
      assert(nest[j+1].value.defined());
      if (nest[j+1].name == nest[j].name)
        std::swap(nest[j+1], nest[j]);
      else
        break;
    }
  }
  // Build the loop nest
  for (int i = (int)nest.size() - 1; i >= 0; --i) {
    if (nest[i].value.defined()) {
      DEBUG_MSG("Let stmt name: " << nest[i].name << std::endl);
      stmt = LetStmt::make(nest[i].name, nest[i].value, stmt);
    }
    else {
      DEBUG_MSG("Variable name: " << nest[i].name << std::endl);
      const Schedule::Dim &dim = s.dim[nest[i].dim_index];
      Expr begin = Variable::make(Int(32), nest[i].name + ".loop_begin");
      Expr end = Variable::make(Int(32), nest[i].name + ".loop_end");
      stmt = For::make(nest[i].name, begin, end, dim.stride, dim.ftype, stmt);
    }
  }

  // Define the loop begin/end in terms of begin/end produced by bounds inference
  for (size_t i = 0; i < f.args().size(); ++i) {
    std::string var = prefix + f.args()[i];
    Expr begin = Variable::make(Int(32), var + ".begin");
    Expr end = Variable::make(Int(32), var + ".end");
    stmt = LetStmt::make(var + ".loop_begin", begin, stmt);
    stmt = LetStmt::make(var + ".loop_end", end, stmt);
  }
  return stmt;
}

/* Convert the given function into a loop that computes it. */
Stmt build_produce(Function f) {
  DEBUG_MSG( "Build produce....\n");
  std::string prefix = f.name() + ".s0.";
  // Compute the location to store
  std::vector<Expr> loc;
  Expr value = qualify_expr(prefix, f.value());
  DEBUG_MSG("Function arg name: " << prefix + f.args()[0] << std::endl);
  for (size_t i = 0; i < f.args().size(); ++i) {
    DEBUG_MSG("Inside loop...\n");
    loc.push_back(Variable::make(Int(32), prefix + f.args()[i]));
  }
  return build_store_loop_nest(f, prefix, loc, value, f.schedule());
}

/* Create the loops that updates a function (handles reduction) */
Stmt build_update(Function f) {
  DEBUG_MSG( "Build update....\n");
  std::string prefix = f.name() + ".s1.";

  //Compute the location to store
  std::vector<Expr> loc;
  Expr value = qualify_expr(prefix, f.reduction_value());
  DEBUG_MSG("Reg args size: " << f.reduction_args().size() << std::endl);
  for (size_t i = 0; i < f.reduction_args().size(); ++i)
    loc.push_back(qualify_expr(prefix, f.reduction_args()[i]));

  Stmt loop = build_store_loop_nest(f, prefix, loc, value, f.reduction_schedule());

  //Define the bounds of the reduction domain
  if (f.reduction_domain().defined()) {
    const std::vector<ReductionVariable>& dom = f.reduction_domain().domain();
    for (size_t i = 0; i < dom.size(); ++i) {
      std::string p = prefix + dom[i].name;
      Expr rbegin = Variable::make(Int(32), p + ".begin");
      Expr rend = Variable::make(Int(32), p + ".end");
      loop = LetStmt::make(p + ".loop_begin", rbegin, loop);
      loop = LetStmt::make(p + ".loop_end", rend, loop);
    }
  }
  return loop;
}

Stmt create_initial_loop(Function f) {
  std::vector<Stmt> stmts;
  stmts.push_back(build_produce(f));
  if (f.is_reduction())
    stmts.push_back(build_update(f));

  return BlockStmt::make(stmts);
}

Stmt lower(Function f) { // todo: input vector of functions
  // Compute an environment
  std::map<std::string, Function> env = find_transitive_calls(f);

  // Function is computed and stored at root
  Func(f).compute_root();
  Func(f).store_root();

  // Compute a realization order
  std::vector<std::string> order = realization_order(f.name(), env); // todo: makes sense only when there are multiple functions (dummy)

  DEBUG_MSG("Generating initial loop nest...\n");
  Stmt s = create_initial_loop(f);

  DEBUG_MSG("Scheduling function...\n");
  s = schedule_function(s, order, env);

  DEBUG_MSG( "Infering bounds...\n");
  s = infer_bounds(s, order, env);

  DEBUG_MSG("Priority pass..\n");
  s = OrderedArray_pass(s);

  DEBUG_MSG("Simplifying...\n");
  s = simplify(s);

  DEBUG_MSG("Flattening dimensions...\n");
  s = flatten_dimensions(s);

  DEBUG_MSG("Vectorization...\n");
  s = vectorize(s);

  DEBUG_MSG("Strength reduction...\n");
  s = strength_reduction(s);

  return s;
}


Stmt lower(Stmt s) {

  // const std::string& fname = "KNN.stmt";
  // std::ofstream stmt_output(fname.c_str());


  // cout << s ;
  // cout << "\n";
  // DEBUG_MSG("Simplifying...\n");
  // s = simplify(s);
  // cout << "\n";
  DEBUG_MSG("Priority pass..\n");
  s = OrderedArray_pass(s);

  // cout << s ;
  // cout << "\n";

  DEBUG_MSG( "Flattening dimensions...\n");
  s = flatten_dimensions(s);

  // cout << s ;
  // cout << "\n";

  DEBUG_MSG("Numerical Optimizations...\n");
  s = Numerical_Optimizations(s);

  // cout << s ;
  // cout << "\n";

  DEBUG_MSG("Strength reduction...\n");
  s = strength_reduction(s);

  // cout << s ;
  // cout << "\n";


  return s;
}


}
