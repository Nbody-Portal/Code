#include <fstream>
#include "Func.h"
#include "Lower.h"
#include "IRPrinter.h"
#include "StmtCompiler.h"

namespace Nbody {

/* Inject a base-case definition for a reduction */
void define_base_case(Function func, const std::vector<Expr>& a, Expr e) {
  std::cerr << "Inside define base case....\n";
  if (func.has_pure_definition())
    return;
  std::cerr << "Passed the if....\n";
  std::vector<Var> pure_args(a.size());
  for (size_t i = 0; i < a.size(); ++i) {
    if (const Variable* v = a[i].ir<Variable>())
      if (!v->param.defined())
        pure_args[i] = Var(v->name);
  }
  FuncArgVar(func, pure_args) = e;
}

void Transform::set_dim_type(VarOrRVar v, int stride, For::ForType ftype) {
  bool found = false;
  std::vector<Schedule::Dim>& dims = schedule.dim;
  for (size_t i = 0; i < dims.size(); ++i) {
    if (dims[i].var == v.name()) {
      found = true;
      dims[i].ftype = ftype;
      dims[i].stride = stride;
    }
  }
  assert(found && "Could not find loop to vectorize");
}

Transform& Transform::vectorize(VarOrRVar v, int stride) {
  set_dim_type(v, stride, For::Vectorized);
  return *this;
}

void Func::compile_to_bitcode(const std::string& filename, std::vector<Argument> args) {
  assert(defined() && "Cannot compiled undefined function");

  if (!lowered.defined())
    lowered = lower(func);

  Argument me(name(), true, value().type());
  args.push_back(me);

  StmtCompiler sc;
  sc.compile(lowered, name(), args);
  sc.compile_to_bitcode(filename);
}

void Func::compile_to_lowered_form (const std::string& fname) {
  if (!lowered.defined())
    lowered = lower(func);

  std::ofstream stmt_output(fname.c_str());
  stmt_output << lowered;
}

void* Func::jit_compile() {
  assert(defined() && "Cannot jit-compile undefined function");

  if(!lowered.defined())
    lowered = lower(func);

  std::cerr << "Inside jit_compile...\n";
  /* Infer arguments */
  InferArguments infer_args;
  lowered.accept(&infer_args);

  Argument me(name(), true, value().type());
  infer_args.arg_types.push_back(me);
  /* To update with address of output */
  arg_values = infer_args.arg_values;
  arg_values.push_back(NULL);

  StmtCompiler sc;
  sc.compile(lowered, name(), infer_args.arg_types);

  compiled_module = sc.compile_to_function_pointers();

  return compiled_module.function;
}

void Func::evaluate(Data result) {
  if(!compiled_module.wrapped_function)
    jit_compile();
  assert(compiled_module.wrapped_function && "No function pointer found");

  /* Check the type and dimensionality of the output */
  assert(result.dims() == dims() && "Result and Function have different dimensionalities");
  assert(result.type() == value().type() && "Result and Function have different types");

  /* Update the address of the output we're evaluation into */
  arg_values[arg_values.size() - 1] = result.raw_buffer();

  std::cerr << "Calling jitted function\n";
  compiled_module.wrapped_function(&(arg_values[0]));
}

Data Func::evaluate(int m, int n) {
  assert(defined() && "Cannot evaluate undefined function");
  Type t = value().type();
  Data d(t, m, n);
  evaluate(d);
  return d;
}

}
