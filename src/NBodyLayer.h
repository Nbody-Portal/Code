#ifndef N_BODY_LAYER_IMPORT
#define N_BODY_LAYER_IMPORT

#include "StorageObject.h"
#include "Storage.h"
#include "KeyValueStorage.h"
#include "PortalFunction.h"
#include "PortalOperator.h"
#include "Points.h"
#include <string>
#include <sstream>
#include <fstream>
#include <utility>
#include <assert.h>


#endif

#ifndef N_BODY_LAYER
#define N_BODY_LAYER

namespace Nbody {


class NBodyLayer{
	public:
		PortalOperator op; // operator coressponding to the layer
		Storage set; // // the dataset coressponding to the layer
		std::string setAccessor; // name of the accessor for this layer
		void * userFunct; // external function defined
		std::string userFunctName;
		std::string pointAccessor;
		int n; // size of the data set assigned to his layer

		int startIndex;  // starting index of the data set for this layer
		int endIndex;    // ending index of the data set for this layer
		PortalFunction func;

};

}



#endif
