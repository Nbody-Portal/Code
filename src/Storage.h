#ifndef _STORAGE_
#define _STORAGE_


#include "Nbody.h"
#include "StorageObject.h"
#include "Points.h"


#include <iostream>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <cfloat>
#include <assert.h>

#include "utils.h"
#include "vector_of_array.hpp"
#include "Binary_tree.h"
#include "Cover_tree.h"
// #include "general_tree.h"
#include "Hrect_bounds.h"
#include "Hsquare_bounds.h"
#include "Cover_bounds.h"
#include "Octree.h"



#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif


class Storage{
	public:
		/*
		Storage is the set data storage used within Portal.

		It can be used to load data from external sources into the program and to save data from the PortalExpr object.
		Arguments
			filepath - path to where file can be found
		*/
		Storage();
		Storage(std::string filepath);
		Storage(Nbody::Points<float> p);
		Storage(Nbody::Data d);
		Storage(vector_of_array<real_t> voa);
		Storage(std::vector<std::vector<float>> v);


		// float * operator [] (int index);
		const float * operator [] (int index) const;
		//const float * operator()(int index) const;
		bool operator==(const Storage &other) const;
		bool operator!=(const Storage &other) const;
		Storage operator=(Storage& other);
		friend std::ostream& operator << (std::ostream& outs, const Storage& store);



		int size() const;
		int pointSize() const;
		int stride(int i) const;
		// void set_tree_model(int a);
		Nbody::Points<float> points() const;
		void reassign(Nbody::Points<float> newSet);
		void reassign(vector_of_array<real_t> voa);
		vector_of_array<real_t> getVectorOfArray();
		void print();

		void toFile(std::string fname);

		bool modified;
		int treeModel = 0;

		typedef Tree<Hrect> BTreeType;
		// typedef GeneralTree<Hrect> TreeType;
		typedef OTree<Hsquare> OTreeType;
		typedef CTree<Cover> CTreeType;
		BTreeType* btree;
		OTreeType* otree;
		CTreeType* ctree;
	private:

		Nbody::Points<float> internalPoints;
		// Nbody::Points<float> internalPoints_perm;


};

class sso : public Storage{};
#endif
