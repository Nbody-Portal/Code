#include "Util.h"

namespace Nbody {

std::string int_to_string(int x) {
  static const std::string small_ints[] = {"0", "1", "2", "3", "4"};
  if (x < 5) return small_ints[x];
  std::ostringstream ss;
  ss << x;
  return ss.str();
}

std::string generate_name(char prefix) {
  static int instances[256];
  std::ostringstream str;
  str << prefix << instances[(unsigned char)prefix]++;
  return str.str();
}

bool begins_with(const std::string& str, const std::string& prefix) {
  return (str.compare(0, prefix.length(), prefix) == 0);
}

bool ends_with(const std::string& str, const std::string& suffix) {
  return (str.compare(str.length() - suffix.length(), suffix.length(), suffix) == 0);
}

}
