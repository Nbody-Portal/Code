#include "Storage.h"


Storage::Storage(){
	this->modified = false;
}

/**
Storage constructor

Reads an input file and converts it into a set storage object
File must of type CSV

Args
	filepath - file to read from
**/
Storage::Storage(std::string filepath){
	/* fist checks for the validity of the file */
	std::ifstream valFile(filepath);
  std::string line = "";
	int m = 0;
	int n = 0;
	while(getline(valFile , line)){
		std::string word = "";
		int tempn = 0;
		std::stringstream strstr(line);
    while (getline(strstr, word, ',')){
			++tempn;
		}
		if(m == 0){
			n = tempn;
		}
		if(n != tempn){
			std::cerr << "Storage constructor failed, row length not the same as previous rows @ row " << std::to_string(m+1) << "\n";
			assert(n == tempn);
		}
		++m;
	}
	valFile.close();

	this->internalPoints = Nbody::Points<float>(m , n);

  /* Reading from the file and assigned internal data values */
	std::ifstream writeFile(filepath);
	int mi = 0;
	while(getline(writeFile , line)){
		std::string word = "";
		int ni = 0;
		std::stringstream strstr(line);
        while (getline(strstr, word, ',')){
			this->internalPoints(mi , ni) = stof(word);
			++ni;
		}
		++mi;
	}
	writeFile.close();
	this->modified = false;
	vector_of_array<real_t> target, t_perm, t_center;
	target = this->getVectorOfArray();
	// t_perm  = vector_of_array<real_t>(target[0].size(), target.size());
	t_perm  = vector_of_array<real_t>(target.size(), target[0].size());
	t_center  = vector_of_array<real_t>(target.size(), target[0].size());

		// typedef GeneralTree<Hrect> GeneralTreeType;

		// ttree = new GeneralTree<Hrect>(target, t_perm);

	// if (treeModel == 0) {

		// typedef Tree<Hrect> BTreeType;
		// btree =  new BTreeType(target, t_perm);
		// btree->build_kdtree();
		// reassign(btree->data_perm);
		typedef OTree<Hsquare> OTreeType;
		otree =  new OTreeType(target, t_perm);
		typedef Tree<Hrect> BTreeType;
		btree =  new BTreeType(target, t_perm);
		typedef CTree<Cover> CTreeType;
		ctree =  new CTreeType(target, t_perm);

		if (target[0].size() == 3) {
			// typedef OTree<Hsquare> OTreeType;
			// otree =  new OTreeType(target, t_perm);
			otree->build_octree();
			cout << otree->node_data.size() << "\n";
			cout << otree->root().begin() << "\n";
			cout << otree->root().end() << "\n";
			cout << "build octree\n";
			reassign(otree->data_perm);
			treeModel = 1;
			// cout << "bade re-assign\n";
		}else if (target[0].size() < 100) {
			// typedef Tree<Hrect> BTreeType;
			// btree =  new BTreeType(target, t_perm);
			btree->build_kdtree();
			cout << "build kdtree\n";
			reassign(btree->data_perm);
			treeModel = 0;
		}else {
			ctree->build_covertree();
			cout << "build covertree\n";
			reassign(ctree->data_perm);
			treeModel = 2;

		}



 	/*
 		cout << "in stoarge 1\n";
		typedef Tree<Hrect> TreeType;
		cout << "in stoarge 2\n";
		TreeType b_tree(target, t_perm);
		cout << "in stoarge 3\n";
		ttree = &b_tree;
		cout << "in stoarge 4\n";
		// ttree->build_kdtree();
		ttree->build_tree();
		cout << "in stoarge 5\n";
		cout << "b_tree : " << b_tree.data_perm.size() << "\n";
		// cout << "ttree : " << ttree->data_perm.size() << "\n";
		cout << "b_tree : " << b_tree.data_perm.size() << "\n";
		// reassign(ttree->data_perm);
		reassign(b_tree.data_perm);
		cout << "in stoarge 6\n";
		*/

	// }
	// else if (treeModel == 1) {
	// 	typedef OTree<Hrect> TreeType;
	// 	TreeType ttree(target, t_perm);
	// 	ttree.build_tree();
	// }

	// if (target[0].size() < 4) {
	// 	t_perm  = vector_of_array<real_t>(target.size(), target[0].size());
	// else {
		// t_perm  = vector_of_array<real_t>(target[0].size(), target.size());
	// }



	// typedef Tree<Hrect> TreeType;
	// TreeType ttree(target, t_perm);

  // if (target[0].size() < 4)
	// 	ttree.build_kdtree_column_major();
	// else
		// ttree.build_kdtree();
  cout << "ghable kharej shodam az storage\n";
}


/**
Storage constructor.Uses input points object
Args
	p - Points object
**/
Storage::Storage(Nbody::Points<float> p){
	this->internalPoints = p;
	this->modified = false;
}

/**
Storage constructor. Uses input data object
Args
	d - Data object
**/
Storage::Storage(Nbody::Data d){
	this->modified = false;
	if(d.type().is_float()){
		this->internalPoints = Nbody::Points<float>(d);
		return;
	}
	int m , n;
	Nbody::Points<int> pi;
	Nbody::Points<bool> pb;
	if(d.type().is_bool()){
		pb = Nbody::Points<bool>(d);
	}
	else{
		pi = Nbody::Points<int>(d);
	}
	if(d.type().is_bool()){m = pb.m();}
	else{m = pi.m();}

	if(d.type().is_bool()){n = pb.n();}
	else{n = pi.n();}

	this->internalPoints = Nbody::Points<float>(m , n);

	for (int j = 0; j < m; ++j){
		for (int i = 0; i < n; ++i){
			float val;
			if(d.type().is_bool()){val = (float) pb(j,i);}
			else{val = (float) pi(j,i);}
			internalPoints(j , i) = val;
		}
	}
}

/**
Storage constructor. Uses input vector of array
Args
	voa - vector_of_array object
**/
Storage::Storage(vector_of_array<real_t> voa){
	this->internalPoints = Nbody::Points<float>(voa.size() , voa.array_size());
	for (int j = 0; j < voa.size(); ++j){
		for (int i = 0; i < voa.array_size(); ++i){
			internalPoints(j , i) = (float) voa[j][i];
		}
	}
	this->modified = false;
}

/**
Storage constructor. Uses vector of floats
Args
	voa - vector of floats
**/
Storage::Storage(std::vector<std::vector<float>> v){
	this->internalPoints = Nbody::Points<float>(v.size() , v[0].size());
	for (int j = 0; j < v.size(); ++j){
		for (int i = 0; i < v[0].size(); ++i){
			internalPoints(j , i) = v[j][i];
		}
	}
	this->modified = false;
}

/**
Returns the size of the Storage
**/
int Storage::size() const{
	return this->internalPoints.m();
}

/**
Returns the stride of the entire object or a point, depending on the dimension
Args
	i - dimension to check (0 or 1)
**/
int Storage::stride(int i) const{
	return this->internalPoints.stride(i);
}

/**
Returns the size of an individual point
**/
int Storage::pointSize() const{
	return this->internalPoints.n();
}



/**
Ostream operator for Storage
**/
std::ostream& operator << (std::ostream& outs, const Storage& store) {
	outs << "[";
	for (int j = 0; j < store.size(); ++j){
		if(j != 0){
			outs << "\n";
		}
		outs << "[";
		for (int i = 0; i < store.pointSize(); ++i){
			if(i != 0){
				outs << ", ";
			}
			outs << store[j][i];
		}
		outs << "]";
	}
	outs << "]";
	return outs;
}


/**
Prints the object
**/
void Storage::print(){
	std::cout << "[";
	for (int j = 0; j < this->size(); ++j){
		if(j != 0){
			std::cout << "\n";
		}
		std::cout << "[";
		for (int i = 0; i < this->pointSize(); ++i){
			if(i != 0){
				std::cout << ", ";
			}
			std::cout << this[0][j][i];
		}
		std::cout << "]";
	}
	std::cout << "]";
}

void Storage::toFile(std::string fname){
	std::ofstream fl;
	fl.open (fname);
	fl.precision(17);
	for (int j = 0; j < this->size(); ++j){
		if(j != 0){
			fl << "\n";
		}
		for (int i = 0; i < this->pointSize(); ++i){
			if(i != 0){
				fl << ",";
			}
			fl << std::fixed << this[0][j][i];
		}
	}
	fl.close();
}

/**
Reassigns the Storage with the newSet input

Args
	newSet - Points<float> object
**/
void Storage::reassign(Nbody::Points<float> newSet){
	cout << "reassign : " << newSet.n() << "\n";
	this->internalPoints = newSet;
	this->modified = true;
}

/**
Reassigns the Storage with the voa input

Args
	voa - vector_of_array<real_t> object
**/
void Storage::reassign(vector_of_array<real_t> voa){
	cout << "to re-assign\n";
	this->internalPoints = Nbody::Points<float>(voa.size() , voa.array_size());
	for (int j = 0; j < this->size(); ++j){
		for (int i = 0; i < this->pointSize(); ++i){
			internalPoints(j , i) = (float) voa[j][i];
			// ttree->data_perm[j][i] = (float) voa[j][i];
			cout << voa[j][i] << " , ";
		}
		cout << "\n";
	}
	this->modified = true;
}

/**
Returns the internal points object
**/
Nbody::Points<float> Storage::points() const{
	return this->internalPoints;
}

/**
Returns a vector_of_array representation of the Storage
**/
vector_of_array<real_t> Storage::getVectorOfArray(){
	vector_of_array<real_t> out(this->size() , this->pointSize());
	for (int j = 0; j < this->size(); ++j){
		for (int i = 0; i < this->pointSize(); ++i){
			out[j][i] = this[0][j][i];
		}
	}
	return out;
}

/**
Equality operator for Storage
**/
bool Storage::operator==(const Storage &other) const{
	if(this->size() != other.size()){
		return false;
	}
	if(this->pointSize() != other.pointSize()){
		return false;
	}
	for(int i = 0; i != this->size(); ++i){
		for(int j = 0; j != this->pointSize(); ++j){
			if(this[0][i][j] != other[i][j]){
				return false;
			}
		}
	}
	return true;
}

/**
NEQ operator for Storage
**/
bool Storage::operator!=(const Storage &other) const{
	if(this->size() != other.size()){
		return true;
	}
	if(this->pointSize() != other.pointSize()){
		return true;
	}
	for(int i = 0; i != this->size(); ++i){
		for(int j = 0; j != this->pointSize(); ++j){
			if(this[0][i][j] != other[i][j]){
				return true;
			}
		}
	}
	return false;
}

/**
Accessor for Storage
Returns a point, which then can also be accessed using []
**/
const float* Storage::operator [](const int index) const{
	return &this->internalPoints(index);
}

Storage Storage::operator=(Storage& other) {

	this->internalPoints = other.internalPoints;
	this->btree = other.btree;
	this->btree->data_perm = other.btree->data_perm;
	this->otree = other.otree;
	this->otree->data_perm = other.otree->data_perm;
	return *this;

}
