#ifndef PORTAL_FUNCTION_IMPORT
#define PORTAL_FUNCTION_IMPORT

#include <vector>
#include <utility>
#include <assert.h>

#include "IRNode.h"
#include "IROperator.h"

class PortalFunction{
    /*
    PortalFunction is the parent class of system defined functions/kernels.
    */
	public:
		// enum TYPE{EUDIST , EUCLIDEAN , SQRDIST , EXTERNAL , USER , NA , EXPR ,
		// 	        MANAHATTAN , CHEBYSHEV, DETERMIN};
		enum TYPE{EUDIST , EUCLIDEAN , SQRDIST , EXTERNAL , USER , NA , EXPR ,
			        MANAHATTAN , CHEBYSHEV, DETERMINANT, INVERSE, TRANSPOSE, SUM};
		PortalFunction();
		PortalFunction(TYPE t);
		PortalFunction(Nbody::Expr external);

		TYPE type;

		Nbody::Expr getExpr(std::vector<std::pair<Nbody::Expr , int>> setInfo);
		Nbody::Expr getExpr(Nbody::Expr e) ;

	private:
		int getSmallestDimensionality(std::vector<std::pair<Nbody::Expr , int>> setInfo) const;

		Nbody::Expr Determinant() ;
		Nbody::Expr Determinant(Nbody::Expr Mat , int size) ;
		Nbody::Expr Determinant(Nbody::Expr Mat, std::vector<int> flatMat , int size) ;
		std::vector<int> getCoFactor(std::vector<int>  FlatMatrix, int p, int q, int n) ;
		Nbody::Stmt Transpose(Nbody::Expr Mat, int size) ;
		Nbody::Stmt Inverse(Nbody::Expr Mat, int size);
		Nbody::Stmt MatrixOfMinor(Nbody::Expr Mat, std::vector<int> flatMat, int size);
		Nbody::Expr Sum(Nbody::Expr e, int size);
		Nbody::Expr externalFunction;
};
#endif
