#ifndef INC_IRPRINTER_H
#define INC_IRPRINTER_H

/* Defines operator overloads to print the intermediate
representation in human readable form. */

#include "IRNode.h"

namespace Nbody {

std::ostream& operator<<(std::ostream& stream, const Expr&);

std::ostream& operator<<(std::ostream& stream, const Type&);

std::ostream& operator<<(std::ostream& stream, const Stmt&);

std::ostream& operator<<(std::ostream& stream, const For::ForType&);

/* An IRVisitor that emits IR to the output stream in a readable form */
class IRPrinter : public IRVisitor {
public:
  IRPrinter(std::ostream& s) : stream(s), indent_level(0) { }

  /* Print an expression */
  void print(Expr ir) {
    ir.accept(this);
  }

  /* Print a statement */
  void print(Stmt ir) {
    ir.accept(this);
  }

  /* print the main steam of printer */
  void print() {
  }

  std::ostream& stream;

  /* Perform indentation */
  void indent();
  /* Current indentation level */
  size_t indent_level;
protected:



  void visit (const Integer*);
  void visit (const Flt*);
  void visit (const Dbl*);
  void visit (const String*);
  void visit (const Cast*);
  void visit (const rsqrt*);
  void visit (const Variable*);
  void visit (const Add*);
  void visit (const Sub*);
  void visit (const Mul*);
  void visit (const Div*);
  void visit (const Mod*);
  void visit (const Min*);
  void visit (const Max*);
  void visit (const ComparisonOp*);
  void visit (const Call*);
  void visit (const Vector*);
  void visit (const VectorSet*);
  void visit (const Assign* );
  void visit (const Let* );
  void visit (const LetStmt* );
  void visit (const Select* );
  void visit (const Load*);
  void visit (const Store*);
  void visit (const MultiStore*);
  void visit (const For*);
  void visit (const Realize*);
  void visit (const Evaluate*);
  void visit (const While*);
  void visit (const OrderedArray_Insert*);


};

}

#endif
