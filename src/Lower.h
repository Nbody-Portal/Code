#ifndef INC_LOWER_H
#define INC_LOWER_H

#include "IRNode.h"
#include "IRPass.h"

namespace Nbody {

/* Convert the given function into a loop that computes it. */
Stmt build_produce(Function f);

/* Create the loops that updates a function (handles reduction) */
Stmt build_update(Function f);

/* Lower a given halide function to a statement that evaluates it */
Stmt lower (Function f);

Stmt lower (Stmt s);
}

#endif
