#ifndef INC_UTIL_H
#define INC_UTIL_H

#include <vector>
#include <sstream>

namespace Nbody {

template<typename T>
std::vector<T> vec (T a) {
  std::vector<T> v(1);
  v[0] = a;
  return v;
}

template<typename T>
std::vector<T> vec (T a, T b) {
  std::vector<T> v(2);
  v[0] = a;
  v[1] = b;
  return v;
}

template<typename T>
std::vector<T> vec (T a, T b, T c) {
  std::vector<T> v(3);
  v[0] = a;
  v[1] = b;
  v[2] = c;
  return v;
}

/* Convert integer to string */
std::string int_to_string(int x);

/* Generate an unique name starting with the given character. */
std::string generate_name(char prefix);

/* Check if the first string begins with the second string. */
bool begins_with(const std::string& str, const std::string& prefix);

/* Check if the first string ends with the second string. */
bool ends_with(const std::string& str, const std::string& suffix);

class RefCount {
  int count;
public:
  RefCount() : count(0) {}
  void increment() {count++;}
  void decrement() {count--;}
  bool is_zero() const {
    return count == 0;
  }
};

template<typename T> void destroy(const T*);
template<typename T> RefCount& count(const T*);

/* Support class for shared pointers */
template<typename T>
struct Ptr {
  void inc (T* p) {
    if(p)
      count(p).increment();
  }
  void dec (T* p) {
    if (p) {
      count(p).decrement();
    //  if (count(p).is_zero())
     //   destroy(p);
    }
  }
public:
  T *ptr;

  ~Ptr() {
    dec(ptr);
  }

  Ptr() : ptr(NULL) {}
  Ptr (T *p) : ptr(p) {
    inc(ptr);
  }

  Ptr(const Ptr<T>& other) : ptr(other.ptr) {
    inc(ptr);
  }

  Ptr<T> &operator=(const Ptr<T> &other) {
    T* temp = other.ptr;
    inc(temp);
    dec(ptr);
    ptr = temp;
    return *this;
  }

  /* Checks if handle is null */
  bool defined() const {
    return ptr != NULL;
  }

  /* Checks if two handles point to the same ptr */
  bool same_as(const Ptr& other) const {
    return ptr == other.ptr;
  }
};

}

#endif
