#ifndef PORTAL_OPERATOR
#define PORTAL_OPERATOR

#include <assert.h>
#include <iostream>
#include <limits>

class PortalOperator{
    /*
    PortalOperator is the parent class of the system defined operators.
    */
	public:
		enum OP{FORALL , SUM , PRODUCT , LOGSUM , UNION , UNIONARG , ARGMIN , ARGMAX , MIN , MAX , KARGMIN , KARGMAX , KMIN , KMAX};
		enum IOType{ALL , LIST , VALUE};

		PortalOperator();
		PortalOperator(PortalOperator::OP o);
		PortalOperator(PortalOperator::OP o , int k);

		PortalOperator::OP getOperator() const;
		int getK() const;
		int getUncheckedK() const;
		void changeK(int newK);

		PortalOperator::IOType getOutputType() const;
		PortalOperator::IOType getInputType() const;

		bool isSingleVariableFilter() const;
		bool isMultiVariableFilter() const;
		bool isReductionFilter() const;
		bool isArgOp() const;

		friend std::ostream& operator << (std::ostream& outs, const PortalOperator& store);
		
		double getInitFilterVar() const;
		float initFilterVar;
    bool isComparative() const;
    double getTemp() const;
    bool reverse(double a, double b);


	private:
		OP o;
		//std::vector<float*> computedResult;
		int k;
};
#endif
