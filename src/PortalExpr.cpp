#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif
#define prune_type unsigned long long

#include "PortalExpr.h"
#include "Octree.h"
#include "prune_generator.h"


namespace Nbody {

PortalExpr::PortalExpr(){
	this->modifiedBody = true;
	this->modifiedArgs = true;
	this->loweredExists = false;
}

PortalExpr::~PortalExpr(){
	if(this->loweredExists){
		delete this->lwr;
		this->loweredExists = false;
	}

}

/**
Helper private function of add layer
All other addLayers call this function

Args
	op - PortalOperator
	inputAccessorString - names the set, used in the IR for all the storage, including the intermediate result storages
	Storage - 2d set storage input
	PortalFunction - Function to call on
	Extfunct - Pointer to a function to use
**/

void PortalExpr::addLayer(PortalOperator op , std::string inputAccessorString ,
	                             std::string pointAccessorString , Storage& set ,  PortalFunction funct , void * Extfunct){

	NBodyLayer l;
	l.op = op;

	/* In the case of a Union op, k needs to be adjusted to the size of the data structure */
	if(l.op.getOperator() == PortalOperator::OP::UNIONARG || l.op.getOperator() == PortalOperator::OP::UNION){
		l.op.changeK(set.size());
	}

	/* If its an external function call, link the void function */
	if(funct.type  == PortalFunction::TYPE::USER){
		l.userFunct = Extfunct;
		l.userFunctName = "externalFunction" + std::to_string(extFuncts.size());
		extFuncts.push_back(std::make_pair(l.userFunctName , Extfunct));
	}
	l.set = set;
	// l.set.ttree = set.ttree;
	l.func = funct;
	l.setAccessor = inputAccessorString;
	l.pointAccessor = pointAccessorString;
	l.n = set.size();
	l.startIndex = 0;
	l.endIndex = l.n;

	this->modifiedBody = true;
	layerStorage.push_back(l);
}


/**
addLayer allows the user to put together the problem.
The intent of this is to allow the formulation of the problem in a way that looked similar to the mathematical representation of the problem.
The nesting provides a first in, last executed approach to the problem.

Args
	op - PortalOperator
	inputAccessorString - names the set, used in the IR for all the storage, including the intermediate result storages
	Storage - 2d set storage input
	PortalFunction funct - Portal function to use for the layer
	void * funct - Pointer to a function to use. User reinterpret_cast<void*>(&functName) as the arg and flag the func as an extern "C"
**/

PortalExpr& PortalExpr::addLayer(PortalOperator op , Storage set){
	cout << "Add layer 1 \n";
	this->addLayer(op , "set"+std::to_string(layerStorage.size()) , "point"+std::to_string(layerStorage.size()) , set , PortalFunction(PortalFunction::TYPE::NA) , nullptr);
	return *this;
}

/*
	Adding a layer considering a Portal pre-defined function for the kernel
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , Storage set , PortalFunction funct){
	this->addLayer(op , "set"+std::to_string(layerStorage.size()) , "point"+std::to_string(layerStorage.size()) , set , funct , nullptr);
	return *this;

}

/*
	Adding a layer that doesn't have any kernel function --  could represent a middle layer
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , std::string inputAccessorString , Storage& set){
	this->addLayer(op , "set"+inputAccessorString ,  inputAccessorString , set ,  PortalFunction(PortalFunction::TYPE::NA) , nullptr);
	return *this;
}
/*
	Adding a layer considering a Portal pre-defined function for the kernel and a string variable for set access
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , std::string inputAccessorString , Storage set , PortalFunction funct){
	this->addLayer(op , "set"+inputAccessorString , inputAccessorString , set ,  funct , nullptr);
	return *this;
}

/*
	Adding a layer considering a external function for the kernel  and string variable for set access-- such as C++ functon
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , std::string inputAccessorString , Storage set , void * external_funct){
	this->addLayer(op , "set"+inputAccessorString , inputAccessorString , set ,  PortalFunction(PortalFunction::TYPE::USER) , external_funct);
	return *this;
}
/*
	Adding a layer considering a external function for the kernel-- such as C++ functon
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op  , Storage set , void * external_funct){
	this->addLayer(op , "set"+std::to_string(layerStorage.size()) , "point"+std::to_string(layerStorage.size()) , set ,  PortalFunction(PortalFunction::TYPE::USER) , external_funct);
	return *this;
}
/*
	Adding a layer considering an expression for the kernel
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , std::string inputAccessorString , Storage& set , Expr funct){
	this->addLayer(op , "set"+inputAccessorString , inputAccessorString , set , PortalFunction(funct) , nullptr);
	return *this;
}
/*
	Adding a layer considering an expression for the kernel and an expression accessor for the set
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , Expr setAccessor , Storage set , Expr funct){
	std::string inputAccessorString = "todo";//((Variable) setAccessor).name;
	// std::string inputAccessorString = setAccessor.name;
	this->addLayer(op , "set"+inputAccessorString , inputAccessorString , set , PortalFunction(funct) , nullptr);
	return *this;
}

/*
	Adding a layer considering an expression for the kernel and a variable accessor for the set
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , Var setAccessor , Storage set , Expr funct){
	// std::string inputAccessorString = "todo";//((Variable) setAccessor).name;
	std::string inputAccessorString = setAccessor.name();
	setAccessor.ChangeDim(set.pointSize());
	this->addLayer(op , "set"+inputAccessorString , inputAccessorString , set , PortalFunction(funct) , nullptr);
	return *this;
}

/*
	Adding a layer considering a variable accessor for the set
*/
PortalExpr& PortalExpr::addLayer(PortalOperator op , Var setAccessor , Storage set){
	std::string inputAccessorString = setAccessor.name();
		setAccessor.ChangeDim(set.pointSize());
	this->addLayer(op , "set"+inputAccessorString , inputAccessorString , set , PortalFunction(PortalFunction::TYPE::NA) , nullptr);
	return *this;
}
/*
Attaching additional storage for the problems that need extra storage such as EM
*/
void PortalExpr::attachStorage(std::string name , Storage obj){
	additionalStorage.push_back(std::make_pair(name , obj));
}

/**
Adjusts the iteration bounds for a particular layer
When executing, it will run the For loop on the specified layers from the specified start to end
Imposes an iteration bound on the generated For loop, so the problem can be run on specific parts of the input set

Args
	layer - layer to modify
	start - start index
	end - end index
**/
void PortalExpr::adjustSetIterationBounds(int layer , int start , int end){
	assert(layer < layerStorage.size() && "AdjustSetIterationBounds layer specified doesn't exist!");
	// assert(start < layerStorage[layer].n && "AdjustSetIterationBounds start index specified is out of bounds!");
	// assert(start >= 0 && "AdjustSetIterationBounds start index specified is negative!");
	assert(end <= layerStorage[layer].n && "AdjustSetIterationBounds end index specified is out of bounds!");
	assert(end >= 0 && "AdjustSetIterationBounds end index specified is negative!");

	layerStorage[layer].startIndex = start;
	layerStorage[layer].endIndex = end;
	this->modifiedArgs = true;
}

/**
Adjusts the counter IRS object, to allow values to be percolated out.
This is useful in the case of using adjustSetIterationBounds

For example, if we know the answer for a particular problem is in the second half of the set in layer 0, we can adjust the bounds to run through that
However, because the problems sees that it hasn't fully run through the inputed dataset, it won't allow the answer from that half of the problem to become the answer
Adjusting layer 0 with the start indices and n/2 for the counterIncrement will then ensure that getOutput will be the answer from the previous execution.

Note: the IRS size for layer 0 will be 1. The size for layer 1 will be the set size from layer 1. The size for layer 2 will be set size from layer 1 * set size from layer 2, and so on.
Adjust these bounds accordingly  (adjusting for layer 0 and startIndex 3 doesn't make sense)

Args
	layer - layer to modify
	startIndex - index of counter to modify on particular layer
	endIndex - end index of counter to modify on particular layer
	counterIncrement - how much to increase by
**/
void PortalExpr::adjustPartitionCounter(int layer , int startIndex , int endIndex , int counterIncrement){
	assert(startIndex < endIndex && "adjustCounter startIndex >= endIndex, which has no effect!");
	assert(layer < layerStorage.size() && "adjustCounter specified layer is larger than the amount of available layers.");

	this->compile();


	this->lwr->adjustCounter(this->layerStorage , layer , startIndex , endIndex , counterIncrement);
	this->modifiedArgs = true;

	std::vector<int> si;
	std::vector<int> ei;

	//Save the iteration bounds and over write them with new ones
	for(int i = 0; i != this->layerStorage.size(); ++i){
		si.push_back(layerStorage[i].startIndex);
		ei.push_back(layerStorage[i].endIndex);
		if(i < layer -1){
			layerStorage[i].startIndex = 0;
			layerStorage[i].endIndex = layerStorage[i].n;
		}
		else if(i > layer - 1){
			layerStorage[i].startIndex = 0;
			layerStorage[i].endIndex = 0;
		}
		else{
			layerStorage[i].startIndex = startIndex;
			layerStorage[i].endIndex = endIndex;
		}
	}
	this->execute(); //Adjusts counters and percolates old reults up

	//Re-insert previous indexes
	for(int i = 0; i != this->layerStorage.size(); ++i){
		layerStorage[i].startIndex = si[i];
		layerStorage[i].endIndex = ei[i];
	}
}

/**
Inserts an approximate point with Vec point for the specified bounds and layer

Args
	layer - layer to modify
	startIndex - start index of points you want to approximate
	endIndex - end index of points you want to approximate
	point - Vec point to use as the approximation
**/
void PortalExpr::insertApproximatePoint(int layer , int startIndex , int endIndex , Vec point){
	assert(point.size() == this->layerStorage[layer].set.pointSize() && "insertApproximatePoint point and layer point dimensions are not the same!");
	assert(startIndex < this->layerStorage[layer].set.size() && "insertApproximatePoint startIndex not within bounds of specified layer set!");
	assert(endIndex <= this->layerStorage[layer].set.size() && "insertApproximatePoint endIndex not within bounds of specified layer set!");

	float** saved = this->saveAndReplaceToPoints(layer , startIndex , endIndex , point); //Overwrites all the points in the layer with point

	std::vector<int> si;
	std::vector<int> ei;

	int other = 0;
	if(layer == 0) other = 1;

	//Adjust setIterationBounds
	for(int i = 0; i != this->layerStorage.size(); ++i){
		si.push_back(layerStorage[i].startIndex);
		ei.push_back(layerStorage[i].endIndex);
		if(i == layer){
			layerStorage[i].startIndex = startIndex;
			layerStorage[i].endIndex = endIndex;
		}
	}
	this->execute();

	//Replace injected data
	this->loadToPoints(layer , startIndex , endIndex , saved); //Writes old values back to this layer

	//Re-insert previous indexes
	for(int i = 0; i != this->layerStorage.size(); ++i){
		layerStorage[i].startIndex = si[i];
		layerStorage[i].endIndex = ei[i];
	}

	delete[] saved;

}

/**
Inserts an approximate point with Vec point for the specified boundss and layers
target is layer 0, source is layer 1

##############################################################################################################################
## NOTE: THIS IS MEANT FOR 2 LAYERED PROBLEMS. WHEN EXPANDING THE SUPPORTED LAYER AMOUNT, THIS FUNCTION MUST BE GENERALIZED ##
##############################################################################################################################
Args
	targetStartIndex - start index of points you want to approximate
	targetEndIndex - end index of points you want to approximate
	targetPoint - Vec point to use as the approximation
	sourceStartIndex - start index of points you want to approximate
	sourceEndIndex - end index of points you want to approximate
	sourcePoint - Vec point to use as the approximation
**/
void PortalExpr::insertApproximatePoint(int targetStartIndex , int targetEndIndex , Vec targetPoint , int sourceStartIndex , int sourceEndIndex , Vec sourcePoint){
	assert(targetPoint.size() == this->layerStorage[0].set.pointSize() && "insertApproximatePoint point and layer point dimensions are not the same!");
	assert(sourcePoint.size() == this->layerStorage[1].set.pointSize() && "insertApproximatePoint point and layer point dimensions are not the same!");
	Vec targetSave(targetPoint.size());
	Vec sourceSave(sourcePoint.size());

	float** targetSaved = this->saveAndReplaceToPoints(0 , targetStartIndex , targetEndIndex , targetPoint);
	float** sourceSaved = this->saveAndReplaceToPoints(1 , sourceStartIndex , sourceEndIndex , sourcePoint);

	std::vector<int> si;
	std::vector<int> ei;

	if(this->layerStorage[1].op.isReductionFilter()){
		//this->lwr->adjustCounter(this->layerStorage , 1 , targetStartIndex , targetEndIndex , sourceEndIndex - sourceStartIndex);
	}

	/* Adjust setIterationBounds */
	for(int i = 0; i != this->layerStorage.size(); ++i){
		si.push_back(layerStorage[i].startIndex);
		ei.push_back(layerStorage[i].endIndex);

		layerStorage[i].startIndex = (i == 0) ? targetStartIndex : sourceStartIndex;
		layerStorage[i].endIndex = (i == 0) ? targetEndIndex: sourceEndIndex;
	}

	this->lwr->adjustPointApproximateK(0 , targetEndIndex - targetStartIndex);
	this->lwr->adjustPointApproximateK(1 , sourceEndIndex - sourceStartIndex);

	this->execute();

	/* Replace injected data */
	this->loadToPoints(0 , targetStartIndex , targetEndIndex , targetSaved);
	this->loadToPoints(1 , sourceStartIndex , sourceEndIndex , sourceSaved);

	/* Re-insert previous indexes */
	for(int i = 0; i != this->layerStorage.size(); ++i){
		layerStorage[i].startIndex = si[i];
		layerStorage[i].endIndex = ei[i];
	}

	delete[] targetSaved;
	delete[] sourceSaved;

}


void PortalExpr::set_threshold(double t){
	threshold = t;
}
/**
Helper function for insertApproximatePoint
Loads pnt into the layer from start to end

Args
	layer - layer to modify
	startVal - start index
	endVal - end index
	pnt - point to write

Returns a float** to used to save the over written points
**/
float** PortalExpr::saveAndReplaceToPoints(int layer , int startVal , int endVal , Vec pnt){
	int n = this->layerStorage[layer].set.pointSize();
	float** out = new float*[endVal - startVal];
	for(int i = startVal; i < endVal; ++i){
		out[i - startVal] = new float[n];
		for(int j = 0; j < n; ++j){
			out[i - startVal][j] = this->layerStorage[layer].set[i][j];
			this->layerStorage[layer].set.points()(i , j) = pnt[j];
		}
	}
	return out;
}

/**
Helper function for insertApproximatePoint
Loads previously saved points back into the object

Args
	layer - layer to modify
	startVal - start index
	endVal -  end index
	saved - old saved points, returned by saveAndReplaceToPoints
**/
void PortalExpr::loadToPoints(int layer , int startVal , int endVal , float** saved){
	int n = this->layerStorage[layer].set.pointSize();
	for(int i = startVal; i < endVal; ++i){
		for(int j = 0; j < n; ++j){
			this->layerStorage[layer].set.points()(i , j) = saved[i - startVal][j];
		}
	}

}



/**
Currently a simple volume of boundIteration structure
**/
bool PortalExpr::verifyOutputValidity(){
	int volume = 0;
	int pointLength = this->layerStorage.size();
	for(int i = 0; i != this->boundStructure.size(); ++i){
		int cv = 1;
		for(int j = 0; j != pointLength; ++j){
			cv *=  this->boundStructure[i][j].second - this->boundStructure[i][j].first;
		}
		volume += cv;
	}

	int perfectVolume = 1;
	for(int i = 0; i != this->layerStorage.size(); ++i){
		perfectVolume *= this->layerStorage[i].n;
	}
	DEBUG_MSG( "CORRECT TRAVERSAL VOLUME " << perfectVolume <<  " CURRENT TRAVERSAL VOLUME " << volume << "\n");
	return volume == perfectVolume;
}

/**
Checks if the structure provided for the N-body computation is correct or not,
or the order of operators are valid
Definitions
	* Single Variable Filter/Reduction - an operator that reduced a set of values to a singular value
	* Multi Variable Filter/Reduction - an operator that reduces a set of values to a smaller set of values
Rules
	1. Excluding the FORALL operator, the input to a Single or Multi Variable Filter/Reduction must be a set of values
	2. The input to a FORALL operator can be any thing, from a single value to a set of a set of a set of a ..... of values
	3. Multi Var Filters should be on the inside of the FORALL operators
**/
void PortalExpr::verifyStructuralValidity(){
	// Track will be assigned as: Forall -> 1 , Multi Filter -> 2  , Single Filter -> 3
	int track = 1;
	for(int i = 0; i < layerStorage.size(); ++i){
		int type = 1;
		if(layerStorage[i].op.isMultiVariableFilter()){
			type = 2;
		}
		else if(layerStorage[i].op.isSingleVariableFilter()){
			type = 3;
		}
		if(type < track){
			if(type == 2){
				//Prev op was single var filter and the current op is a multi var filter
				std::cerr << "PortalExpr layer " << i << " not valid. " << layerStorage[i].op << " is a multi variable filter, which returns multiple values.";
				std::cerr << "The layer below this is a single variable filter and can only take in values as it's arguments.\n";
				assert(type >= track);
			}
			if(track == 3){
				//Prev op was single var filter and the current op is FORALL
				std::cerr << "PortalExpr layer " << i << " not valid.  " << layerStorage[i].op << " returns to a single variable filter, which takes in only values.\n";
				assert(type >= track);
			}
			//Prev op was multi var filter and the current op is FORALL
			std::cerr << "PortalExpr layer " << i << " not valid. " << layerStorage[i].op << " returns to a multi variable filter, which takes in only values.\n";
			assert(type >= track);
		}
		if(type == 2 and type == track){
			std::cerr << "PortalExpr layer " << i << " not valid. " << layerStorage[i].op << " is a multi variable filter which returns to another multi variable filter.\n";
			assert(type > track);
		}
		track = type;
	}
}

/**
Deletes the lowered function, throws out the data, and resets everything
**/
void PortalExpr::clearData(){
	if(this->loweredExists){
		delete this->lwr;
		this->loweredExists = false;
	}

	this->boundStructure.clear();
	this->modifiedBody = true;
	this->modifiedArgs = true;

	for(int i = 0; i != this->layerStorage.size(); ++i){
		this->layerStorage[i].startIndex = 0;
		this->layerStorage[i].endIndex = this->layerStorage[i].n;
	}
}

/**
Compiles, then evaluates the expression
this version doesn't use tree traversal
**/
void PortalExpr::execute(){
	this->compile();
	lwr->evaluate();
}

/*
Brute force validation of N-body computation,
it gets the result of computation and compares it with the brute force computation
for the defined N-body problem
*/
bool PortalExpr::Validate(Storage result){
	this->compile();
	lwr->evaluate();
	Storage output = this->getOutput();
  cout << "In the validation process\n";
	bool valid = true;
	for (int i = 0; i < result.size(); i++) {
		for(int j = 0; j < result.pointSize(); j++) {
			 // cout << i << " , " <<  j << " ==> " << result[i][j] <<  " , " <<  output[i][j] << "\n";
			 if (result[i][j] != output[i][j] )
			 {
					valid =  false;
			 }
		}
	}
	return valid;
}

/*
This set of validation chooses a random subset of data and validate the computation
for that random subset. The goal is to provide fast validation.
*/
// bool PortalExpr::FastValidate(int size, Storage result){
// 	this->compile();
// 	lwr->evaluate();
// 	vector<int> indexes(size, -1);
// 	Storage subS = SubStorage(layerStorage[0], size, indexes);
// 	Storage subT = SubStorage(layerStorage[1], size, indexes);
//   layerStorage[0] = subS;
// 	layerStorage[1] = subT;
// 	this->compile();
// 	lwr->evaluate();
// 	Storage output = this->getOutput();
//
//
// 	bool valid = true;
// 	for (int i = 0; i < subS.size(); i++) {
// 		for(int j = 0; j < subS.pointSize(); j++) {
// 			 if (result[indexes[i]][j] != output[i][j] )
// 			 {
// 					valid =  false;
// 			 }
// 		}
// 	}
// 	return valid;
// }

/*
provides a random subset of a Storage for doing the fast validation
*/
// Storage  PortalExpr::SubStorage(Storage source, int size, vector<int> &indexes){
// 	Storage result;
// 		for (size_t i = 0; i < size; i++){
// 			int k = rand() % source.size();
// 			result[i] = source[i];
// 			indexes.push_back(k);
// 		}
// 	 return
// }

/**
Compiles, and evaluates the tree traversal
**/
void  PortalExpr::executeTraverse(){
	this->compile();
  Compute();
}

void  PortalExpr::executeCoverTraverse(){
	this->compile();
  ComputeCover();
}

/**
Returns the output as a Storage format
**/
Storage PortalExpr::getOutput(){
	if(this->modifiedBody){
		return Storage();
	}
	return Storage(this->lwr->getOutput());
}

/**
Compiles the expression then writes the IR to the specified file

Args
	fname - file to write to
**/
void PortalExpr::compile_to_lowered_form (const std::string& fname) {
	cout << "Inside the compile to lowered form \n";
	this->compile();
	std::ofstream stmt_output(fname.c_str());
	this->loweredExists = true;
}

 /*
	Compiles the expression and writes the IRs of each level into the file
	Args
		fname - file to write to
 */
void PortalExpr::compile_to_level_printing (const std::string& fname) {
	this->lwr = new NBodyLower();
	std::ofstream stmt_output(fname.c_str());
	this->lowered = this->lwr->lower_printing(layerStorage , additionalStorage,  extFuncts);
	this->lwr->compile(extFuncts);
}

/*
Compiles the expression and checks if the storage is up to date
*/
void PortalExpr::compile(){
	if(this->modifiedBody || !this->loweredExists){
		this->verifyStructuralValidity();
		this->lwr = new NBodyLower();
		this->lowered = this->lwr->lower(layerStorage , additionalStorage);
		this->loweredExists = true;
		this->modifiedBody = false;
		this->lwr->compile(extFuncts);
	}
	else{
		//Checks if the sets have been reassigned
		for(int i = 0; i != this->layerStorage.size(); ++i){
			//If reassigned, it replaces the pointers in the lwred args
			if(this->layerStorage[i].set.modified){
				this->lwr->updateStorage(i , this->layerStorage);
				this->layerStorage[i].set.modified = false;
				//this->layerStorage[i].p = this->layerStorage[i].set.points(this->layerStorage[i].setAccessor);
			}
		}
	}
}


void  PortalExpr::ComputeCover() {


				// Providing the treeSet for passing to the multi tree traversal
				typedef CTree<Cover> TreeType;
        vector<TreeType*> treeSet;
        for (size_t i = 0; i< layerStorage.size(); i++) {
						treeSet.push_back(layerStorage[i].set.ctree);
						cout << treeSet[i]->root().begin() << "\n";
        }
        std::vector<PortalOperator> Op;
        for (size_t i = 0; i < layerStorage.size(); i++)
          Op.push_back(layerStorage[i].op);
        typedef prune_generator<TreeType, PortalExpr> pruneGenerator;
				pruneGenerator pg(Op, *this, treeSet, this->threshold);
				prune_type prune_size;
        MultiTreeTraversal<TreeType, PortalExpr, pruneGenerator> traverserM(treeSet, *this, pg, prune_size);
        traverserM.traverse();




}

// #if 0
/*
 	Provide the pre-requisites for the computation using tree traversal
  this function uses multi tree traversal for N-body computation
*/

void  PortalExpr::Compute() {


		if (layerStorage[0].set.pointSize() == 3) {
				typedef OTree<Hsquare> OTreeType;
				vector<OTreeType*> treeSet;
				for (size_t i = 0; i< layerStorage.size(); i++) {
						treeSet.push_back(layerStorage[i].set.otree);
							cout << layerStorage[i].set.otree->num_child() << "\n";
						cout << treeSet[i]->node_data.size() << "\n";

        }
        std::vector<PortalOperator> Op;
        for (size_t i = 0; i < layerStorage.size(); i++)
          Op.push_back(layerStorage[i].op);
        typedef prune_generator<OTreeType , PortalExpr> pruneGenerator;
				pruneGenerator pg2(Op, *this, treeSet, this->threshold);
				prune_type prune_size;
        MultiTreeTraversal<OTreeType, PortalExpr, pruneGenerator> traverserM(treeSet, *this, pg2, prune_size);
        traverserM.traverse();
		}
		else if (layerStorage[0].set.pointSize() < 100){
				// Providing the treeSet for passing to the multi tree traversal
				typedef Tree<Hrect> TreeType;
        vector<TreeType*> treeSet;
        for (size_t i = 0; i< layerStorage.size(); i++) {
						treeSet.push_back(layerStorage[i].set.btree);
						cout << treeSet[i]->root().begin() << "\n";
        }
        std::vector<PortalOperator> Op;
        for (size_t i = 0; i < layerStorage.size(); i++)
          Op.push_back(layerStorage[i].op);
        typedef prune_generator<TreeType, PortalExpr> pruneGenerator;
				pruneGenerator pg(Op, *this, treeSet, this->threshold);
				prune_type prune_size;
        MultiTreeTraversal<TreeType, PortalExpr, pruneGenerator> traverserM(treeSet, *this, pg, prune_size);
        traverserM.traverse();

		} else{
				typedef CTree<Cover> CTreeType;
				vector<CTreeType*> treeSet;
				for (size_t i = 0; i< layerStorage.size(); i++) {
						treeSet.push_back(layerStorage[i].set.ctree);
						cout << treeSet[i]->root().begin() << "\n";
				}
				std::vector<PortalOperator> Op;
				for (size_t i = 0; i < layerStorage.size(); i++)
					Op.push_back(layerStorage[i].op);
				typedef prune_generator<CTreeType, PortalExpr> pruneGenerator;
				pruneGenerator pg(Op, *this, treeSet, this->threshold);
				prune_type prune_size;
				MultiTreeTraversal<CTreeType, PortalExpr, pruneGenerator> traverserM(treeSet, *this, pg, prune_size);
				traverserM.traverse();

		}

}
 // #endif
// void  PortalExpr::Compute() {
//
//         int i = 0;
//         vector_of_array<real_t> source, s_perm;
//         vector_of_array<real_t> target, t_perm;
//
//         source = this->layerStorage[i].set.getVectorOfArray();
//         target = this->layerStorage[i+1].set.getVectorOfArray();
//         s_perm  = vector_of_array<real_t>(source.size(), source[0].size());
//         t_perm  = vector_of_array<real_t>(target.size(), target[0].size());
//
//
//
//         typedef Tree<Hrect> TreeType;
//         TreeType stree(source, s_perm);
//         TreeType ttree(target, t_perm);
//
//         stree.build_kdtree();
//         ttree.build_kdtree();
//
//         vector<TreeType*> trees;
//         for (size_t i = 0; i< this->layerStorage.size(); i++) {
//
//           vector_of_array<real_t> source, s_perm;
//           source = this->layerStorage[i].set.getVectorOfArray();
//           s_perm  = vector_of_array<real_t>(source.size(), source[0].size());
//           TreeType stree(source, s_perm);
//           stree.build_kdtree();
//           trees.push_back(&stree);
//         }
//
//         std::vector<PortalOperator> Op;
//
//         for (size_t i = 0; i < layerStorage.size(); i++)
//
//           Op.push_back(layerStorage[i].op);
//
//         typedef prune_generator<TreeType> pruneGenerator;
//
//         pruneGenerator pg(Op, stree, ttree, this->threshold);
//
//         MultiTreeTraversal<TreeType, PortalExpr, pruneGenerator> traverserM(trees, *this, pg);
//
//         traverserM.traverse();
//
// }
//

#if 0

/*
 	Provide the pre-requisites for the computation using tree traversal
  this function uses dual tree traversal for N-body computation
*/

void PortalExpr::Compute() {

        // vector_of_array<real_t> target, t_perm;
        // vector_of_array<real_t> source, s_perm;

        int i = 0;
        // target = this->layerStorage[i].set.getVectorOfArray();
        // source = this->layerStorage[i+1].set.getVectorOfArray();
        // t_perm  = vector_of_array<real_t>(target.size(), target[0].size());
        // s_perm  = vector_of_array<real_t>(source.size(), source[0].size());

				typedef Tree<Hrect> TreeType;
				// typedef Tree<Hrect> TreeType;
				//  if (treeModel == 0 )
        //    typedef Tree<Hrect> TreeType;
				//  else if (treeModel == 1)
				// 	 typedef OTree<Hrect> TreeType; // octree
        // TreeType ttree(target, t_perm);
        // TreeType stree(source, s_perm);
				//
        // ttree.build_kdtree();
        // stree.build_kdtree();

        std::vector<TreeType*> treeSet;

				cout << "Size of Layer storage: " << layerStorage.size() << "\n";

				// cout << layerStorage[0].set.ttree.data_perm.size() << "\n";

        // treeSet.push_back(&this->layerStorage[0].set.ttree);
        // treeSet.push_back(&this->layerStorage[1].set.ttree);
				treeSet.push_back(layerStorage[0].set.ttree);
        treeSet.push_back(layerStorage[1].set.ttree);


        std::vector<PortalOperator> Op;

        for (size_t i = 0; i < layerStorage.size(); i++)
        	Op.push_back(layerStorage[i].op);


        typedef prune_generator<TreeType, PortalExpr> pruneGenerator;

        // pruneGenerator pg(Op, treeSet, this->threshold);
        pruneGenerator pg2(Op, *this, treeSet, this->threshold);

				cout << layerStorage[0].set.ttree->root().index() << "\n";
				// cout << layerStorage[1].set.ttree.points() << "\n";

        layerStorage[0].set.reassign(layerStorage[0].set.ttree->data_perm);
        layerStorage[1].set.reassign(layerStorage[1].set.ttree->data_perm);

        prune_type prune_size;
        DualTreeTraversal<TreeType, PortalExpr, pruneGenerator> traverser(*layerStorage[0].set.ttree, *layerStorage[1].set.ttree, *this, pg2, prune_size);

        traverser.traverse(layerStorage[0].set.ttree->root(), layerStorage[1].set.ttree->root());



}
#endif

/*
 provides the computation for the basecase of the tree traversal by changing the boundries
 considering two boxes for the bases case computation
*/
template <typename Box>
void PortalExpr::base_case(Box& s, Box& t) {
	this->adjustSetIterationBounds(1 , s.begin() , s.end());
  this->adjustSetIterationBounds(0 , t.begin() , t.end());
  this->execute();
}
/*
 provides the computation for the basecase of the tree traversal by changing the boundries
 considering one boxe and a node for the bases case computation
*/

template <typename Box>
void PortalExpr::base_case(Box& s,int t) {
	this->adjustSetIterationBounds(1 , s.begin() , s.end());
  this->adjustSetIterationBounds(0 , t , t+1);
  this->execute();

}

/*
 provides the computation for the basecase of the tree traversal by changing the boundries
 considering one boxe and a node for the bases case computation
*/

template <typename Box>
void PortalExpr::base_case(std::vector<Box*> s) {
	for (size_t i = 0; i <  s.size(); i++) {
		  cout << "Layer: " << i << ": " << s[i]->begin() << " , " << s[i]->end() << "\n";
			this->adjustSetIterationBounds(i , s[i]->begin() , s[i]->end());
	}
  this->execute();
}

/*
	provides the computation between two data points based on the kernel function
	defined for the N-body computation
*/
double PortalExpr::point_calc(Vec& s, Vec& t) {

    int level = layerStorage.size() - 1;
    int dim = layerStorage[0].set.pointSize();
    PortalFunction op = layerStorage[level].func;
    // Expr f = op.getExpr();
    // Expr result = Nbody::Flt::make(1);
		Expr out;

		std::vector<std::string> setPointers;

		string   outputVarName = "outVar";
		Data* outputD = new Data(Float(32), 1 , dim , NULL , outputVarName);
		Points<float> outPoints(*outputD);
		// for(size_t i = 0; i < dim ; i++)
		// 	  outputD[0][i] = s[i];

		std::vector<std::pair<Expr , int>> setInfo;


		std::vector<Argument> args;
		std::vector<const void*> arg_values;


		for(int i = 0; i <= level; ++i){
			setPointers.push_back(layerStorage[i].pointAccessor);
			setInfo.push_back(std::make_pair(Variable::make(Float(32) , setPointers[i]) , 1));
			string setAccessor = layerStorage[i].setAccessor;
			// arg_values.push_back((Data)outPoints);
			args.push_back(Argument(setAccessor, true , Float(32)));
		}

 		std::vector<Stmt> calc;
		NBodyLower* ll = this->lwr;
		out  = op.getExpr(setInfo);



		string output= "out";
		calc.push_back(Assign::make(output , out));
		Stmt head = BlockStmt::make(calc);

		ll->lowered = head;
		ll->args = args;
		ll->arg_values = arg_values;

		ll->compile(this->extFuncts);
		Storage outputFinal = ll->evaluate();
		return (outputFinal[0][0]);


}

/*
	provides a temperorary value that has been conputate so far
	this is used in the tree traversal computation
*/
double PortalExpr::get_temp() {
	return this->getOutput()[getOutput().size()-1][getOutput().pointSize()-1];
}

/*
	provides a temperorary value that has been conputate so far  for a specific data point
	this is used in the tree traversal computation
*/
double PortalExpr::get_temp(int a) {
	return this->getOutput()[0][0];
}

/*
	provides a temperorary value that has been conputate so far for a set of data point
	presented in one bounding box. this is used in the tree traversal computation
*/
template <typename Box>
double PortalExpr::get_temp(Box& t) {

	real_t temp_val = getOutput()[0][0];
	for (int i =  t.begin(); i < t.end(); i++)
		if (temp_val > getOutput()[i][0])
			temp_val = getOutput()[i][0];
	return temp_val;
}
/*
	provides a temperorary value that has been conputate so far for two sets of data point
	presented in the bounding boxes. this is used in the tree traversal computation
*/

double PortalExpr::get_temp(int a, int b) {

		double	temp = DBL_MAX;
		for (size_t i = 0; i < getOutput().size(); i++ )
		{
			if (this->getOutput()[i][getOutput().pointSize()-1] < temp)
				temp = this->getOutput()[i][getOutput().pointSize()-1];
		}
		return temp;
 }

}
