#ifndef PORTAL_EXPRESSION_IMPORT
#define PORTAL_EXPRESSION_IMPORT

#include <functional>
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <utility>
#include <assert.h>

#include "StorageObject.h"
#include "Storage.h"
#include "ValueStorage.h"
#include "KeyValueStorage.h"
#include "PortalFunction.h"
#include "PortalOperator.h"
#include "Lower.h"
#include "IRNode.h"
#include "Data.h"
#include "Points.h"
// #include "IRPrinter.h"
#include "StmtCompiler.h"
#include "NBodyLayer.h"
#include "Binary_tree.h"
#include "Cover_tree.h"
// #include "Binary_tree2.h"
#include "Dual_tree_traversal_Portal.h"
#include "Multi_tree_traversal_Portal.h"
#include "NBodyLower.h"

#endif

#ifndef N_BODY_EXPRESSION
#define N_BODY_EXPRESSION

namespace Nbody {

/** Floating-point type for a real number. */
#if !defined (USE_FLOAT)
	typedef double real_t;
#else
	typedef float real_t;
#endif

typedef fixed_vector<real_t> Vec;

class PortalExpr{
	public:

		struct StoragePair{
			std::string key;
			StorageObject value;
		};

		std::vector<NBodyLayer> layerStorage;
		std::vector<std::pair<std::string , void *>> extFuncts;

		PortalExpr();
		~PortalExpr();

		/* Different functionalities for Adding a layer to the Portal expression */

		PortalExpr& addLayer(PortalOperator op , Storage set);
		PortalExpr& addLayer(PortalOperator op , Storage set , PortalFunction funct);
		PortalExpr& addLayer(PortalOperator op , Storage set , void * funct);
		PortalExpr& addLayer(PortalOperator op , std::string inputAccessorString , Storage& set);
		PortalExpr& addLayer(PortalOperator op , std::string inputAccessorString , Storage set , void * funct);
		PortalExpr& addLayer(PortalOperator op , std::string inputAccessorString , Storage set , PortalFunction funct);
		PortalExpr& addLayer(PortalOperator op , std::string inputAccessorString , Storage& set , Expr funct);
		PortalExpr& addLayer(PortalOperator op , Var setAccessor , Storage set);
		PortalExpr& addLayer(PortalOperator op , Var setAccessor , Storage set , Expr funct);
		PortalExpr& addLayer(PortalOperator op , Expr setAccessor , Storage set , Expr funct);

		void attachStorage(std::string name , Storage obj);

		/*
		adjustSetIterationBounds imposes an iteration bound on the generated For loop,
		so the problem can be run on specific parts of the input set
		layer - decides the layer, where layer 0 is the first layer added
		start - start set index
		end - end set index
		*/
		void adjustSetIterationBounds(int layer , int start , int end);
		/*
		adjustPartitionCounter updates the counters for each partition in the case of prune
		or approximation, this is useful when we need to check if we observerd the whole data
		*/
		void adjustPartitionCounter(int layer , int startIndex , int endIndex , int counterIncrement);
		void insertApproximatePoint(int layer , int startIndex , int endIndex , Vec point);
		void insertApproximatePoint(int targetStartIndex , int targetEndIndex , Vec targetPoint , int sourceStartIndex , int sourceEndIndex , Vec sourcePoint);

		void verifyStructuralValidity();
		bool verifyOutputValidity();

		void execute();
    void executeTraverse();
		void executeCoverTraverse();
		bool Validate(Storage result);
		// bool Validate();
		// bool ValidateResult();
		void compile_to_lowered_form (const std::string& fname);
		void compile_to_level_printing (const std::string& fname);
		void clearData();
		void compile();
		void Compute();
		void ComputeCover();
		Storage getOutput();


		void buildTree();
		template<typename Box> void base_case(Box& , int);
		template<typename Box> void base_case(Box& , Box&);
		template<typename Box> void base_case(std::vector<Box*>);
		double get_temp();
		double get_temp(int a, int b);
		double get_temp(int a);
		template<typename Box> double get_temp(Box&);
		double point_calc(Vec& s, Vec& t);
		void set_threshold(double t);

		NBodyLower * lwr;


	private:
		Stmt lowered;
		bool modifiedBody;
		bool modifiedArgs;
		bool loweredExists;
		int treeModel = 0;
		double threshold = 0.1;

		std::vector<std::pair<std::string , Storage>> additionalStorage;

		std::vector<std::vector<std::pair<int , int>>> boundStructure;

		// void addLayer(PortalOperator op , std::string inputAccessorString  , std::string pointAccessorString , Storage set ,  PortalFunction funct , void * Extfunct);
		void addLayer(PortalOperator op , std::string inputAccessorString  , std::string pointAccessorString , Storage &set ,  PortalFunction funct , void * Extfunct);

		float** saveAndReplaceToPoints(int layer , int startVal , int endVal , Vec pnt);
		void loadToPoints(int layer , int startVal , int endVal , float** saved);

};

}

#endif
