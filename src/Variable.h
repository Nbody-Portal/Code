#ifndef INC_VARIABLE_H
#define INC_VARIABLE_H

#include "IRNode.h"

namespace Nbody {

class Var {
  std::string _name;
  int _dim;

public:
  /* Construct a variable with the given name */
  Var(const std::string& n) : _name(n), _dim(2) {}

  /* Construct a variable with an automatically generated name */
  Var() : _name(generate_name('v')), _dim(2) {}

  Var(int d) : _name(generate_name('v')), _dim(d) {}

  /* Get the name of the variable */
  const std::string& name() const {
      return _name;
  }
  const int& dim() const {
      return _dim;
  }

  void ChangeDim(int d)  {
      _dim = d;
  }

  /* Check if two variable names are the same */
  bool same_as(const Var& other) const {
    return _name == other._name;
  }

  /* Variable can be treated as an expression of type Int(32) */
  operator Expr() {
    return Variable::make(Int(32), name());
  }
  // Expr Expr() {
  //   return Variable::make(Int(32), name());
  // }

  /* Adding a subscript operator for the Var struct*/
  inline Expr operator[](int index){
		// Expr mult = Mul::make(Integer::make(index) , Variable::make("set" + name() + ".stride.0"));
		// return Load::make(Float(32) , name() , mult , true , 1);
    return Load::make(Float(32) , name() , Variable::make(name()) , Integer::make(index));
	}
  // inline Expr Expr::operator[](int index){
  // 	assert(this->defined() && "operator '[]' of undefined Expr");
  // 	return Load::make(Float(32) , "OPOverload" , *this , Integer::make(index));
  //   }

};

}

#endif
