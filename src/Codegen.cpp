#ifdef _DEBUG
#define DEBUG_MSG(str) do { std::cout << str; } while( false )
#else
#define DEBUG_MSG(str) do { } while ( false )
#endif

#include <sstream>
#include "Codegen.h"
#include "buffer_t.h"

namespace Nbody {

  using namespace llvm;
  using namespace std;

#define IntializeTarget(target)         \
  LLVMInitialize##target##Target();     \
  LLVMInitialize##target##TargetInfo(); \
  LLVMInitialize##target##TargetMC();

#define InitializeAsmPrinter(target)    \
  LLVMInitialize##target##AsmPrinter(); \

#define InitializeAsmParser(target)     \
  LLVMInitialize##target##AsmParser();  \

#if X86
#define IntializeX86Target() InitalizeTarget(X86)
#define IntializeX86AsmPrinter() InitalizeAsmPrinter(X86)
#define IntializeX86AsmParser() InitalizeAsmParser(X86)
#endif

  CodeGenContext::CodeGenContext() :
    module(nullptr),
    function(nullptr),
    context (nullptr),
    builder (nullptr),
    value (nullptr),
    buffer_t_type (nullptr) {
      initializeLLVM();
    }

  void CodeGenContext::initializeLLVM() {
    InitializeNativeTarget();
    InitializeNativeTargetAsmPrinter();
    InitializeNativeTargetAsmParser();

#define LLVM_TARGET(X86) \
    LLVMInitializeX86Target();     \
    LLVMInitializeX86TargetInfo(); \
    LLVMInitializeX86TargetMC();
#include <llvm/Config/Targets.def>
#undef LLVM_TARGET

#define LLVM_ASM_PRINTER(X86) \
    LLVMInitializeX86AsmPrinter();
#include <llvm/Config/AsmPrinters.def>
#undef LLVM_ASM_PRINTER

#define LLVM_ASM_PARSER(X86) \
    LLVMInitializeX86AsmParser();
#include <llvm/Config/AsmParsers.def>
#undef LLVM_ASM_PARSER

    /*
#define LLVM_TARGET(target) \
Initalize##target##Target();
#include <llvm/Config/Targets.def>
#undef LLVM_TARGET

#define LLVM_ASM_PRINTER(target) \
Intialize##target##AsmPrinter();
#include <llvm/Config/AsmPrinters.def>
#undef LLVM_ASM_PRINTER

#define LLVM_ASM_PARSER(target) \
Initialize##target##AsmParser();
#include <llvm/Config/AsmParsers.def>
#undef LLVM_ASM_PARSER
*/
  }

  void CodeGenContext::init_module ()
  {
    if (builder) delete builder;

    context = new LLVMContext();
    /* Get the initial module for this target platform */
    module = get_target_module (context);
    builder = new IRBuilder<>(*context);

    /* Define types */
    void_ty = llvm::Type::getVoidTy(*context);
    i8 = llvm::Type::getInt8Ty(*context);
    i32 = llvm::Type::getInt32Ty(*context);
    f32 = llvm::Type::getFloatTy(*context);
    f64 = llvm::Type::getDoubleTy(*context);
  }


  CodeGenContext::~CodeGenContext()
  {
	DEBUG_MSG("FREEING MEMORY FROM CODEGEN \n");
    if (module) {
      delete context;
      context = NULL;
    }
    delete builder;
    builder = NULL;
  }

  /* Generate code from the AST */
  void CodeGenContext::compile (Stmt stmt, string name, const vector<Argument> &args)
  {
    DEBUG_MSG("Generating module...\n");
    /* Initialize module */
    init_module();

    /* Set the target triple */
    llvm::Triple triple = get_target_triple();
    module->setTargetTriple(triple.str());
    DEBUG_MSG("Target triple: " << module->getTargetTriple() << endl);

    /* Get the type of buffer_t */
    buffer_t_type = get_buffer_type();

    /* Analyze type of arguments to function */
    vector<llvm::Type*> arg_types(args.size());
    for (size_t i = 0; i < args.size(); i++) {
		DEBUG_MSG("ARG " << i << " " << args[i].name <<  "\n");
      if (args[i].is_ptr)
        arg_types[i] = buffer_t_type->getPointerTo();
      else
        arg_types[i] = llvm_get_type (args[i].type);
    }


    /* Create the top level interpreter function to call as entry */
    function_name = name;
    FunctionType* ftype = FunctionType::get (void_ty, arg_types, false);
    function = llvm::Function::Create (ftype, GlobalValue::ExternalLinkage, name, module.get());

    /* Create the initial basic block */
    BasicBlock *bblock = BasicBlock::Create (*context, "entry", function);
    builder->SetInsertPoint(bblock);

    /* Put arguments in the symbol table */
    size_t i = 0;
    for (llvm::Function::arg_iterator it = function->arg_begin(); it != function->arg_end(); it++) {
      if (args[i].is_ptr)
        unpack_buffer (args[i].name, iterator_to_pointer(it));
      else
        sym_push (args[i].name, iterator_to_pointer(it));
      i++;
    }

    /* Emit bitcode */
    stmt.accept (this);

    /* Return from function */
    builder->CreateRetVoid();

    module->setModuleIdentifier("nbody_" + name);

    /* Validate the generated code */
    verifyFunction(*function);

    /* Wrapper function (for calling from jit) */
    std::string wrapper_name = name + "_jit_wrapper";
    ftype = FunctionType::get (void_ty, vec<llvm::Type*>(i8->getPointerTo()->getPointerTo()), false);
    llvm::Function* wrapper = llvm::Function::Create (ftype, GlobalValue::ExternalLinkage, wrapper_name, module.get());
    bblock = BasicBlock::Create(*context, "entry", wrapper);
    builder->SetInsertPoint(bblock);

    Value* arg_array = iterator_to_pointer(wrapper->arg_begin());

    std::vector<Value*> wrapper_args(args.size());
    for(size_t i = 0; i < args.size(); ++i) {
      Value* ptr = builder->CreateConstGEP1_32(arg_array, (int)i);
      ptr = builder->CreateLoad(ptr);

      if(args[i].is_ptr)
        wrapper_args[i] = builder->CreatePointerCast(ptr, buffer_t_type->getPointerTo());
      else {
        ptr = builder->CreatePointerCast(ptr, arg_types[i]->getPointerTo());
        wrapper_args[i] = builder->CreateLoad(ptr);
      }

    }
    DEBUG_MSG( "Creating call from wrapper to function\n");
    builder->CreateCall(function, wrapper_args);
    builder->CreateRetVoid();
    verifyFunction(*wrapper);

    /* Verify the module */
    verifyModule(*module);

    /* Print the bitcode in a human-readable format
     * to see if the program compiled properly
     */

#ifdef _DEBUG
	DEBUG_MSG("Generated module\n");
  std::cout << "Generated module\n";
    // module->dump();
    // module->print(llvm::errs(), nullptr);
#endif
    /* Optimize the module */
    optimize_module();

    /* Print the optimized bitcode */

#ifdef _DEBUG
	DEBUG_MSG("Opitmized module\n");
  std::cout << "Opitmized module\n";
    // module->dump();
    // module->print(llvm::errs(), nullptr);
#endif
  }

  JITModule CodeGenContext::compile_to_function_pointers() {
    assert (module && "No module defined. Must call compile before calling compile_to_function_pointers");
    JITModule m;
    m.compile_module(this, std::move(module), function_name);
    return m;
  }


  JITModule CodeGenContext::compile_to_function_pointers(std::vector<std::pair<std::string , void *>> extFuncts) {
    assert (module && "No module defined. Must call compile before calling compile_to_function_pointers");
    JITModule m;
    m.compile_module(this, std::move(module), function_name , extFuncts , externFunctions);

    return m;
  }


  void CodeGenContext::optimize_module()
  {
    DEBUG_MSG( "Optimizing module...\n");
    /* Run optimization passes */
    class MyFunctionPassManager : public legacy::FunctionPassManager {
      public:
        MyFunctionPassManager(llvm::Module *m) : legacy::FunctionPassManager(m) {}
        virtual void add(Pass *p) override {
          DEBUG_MSG("Adding function pass: " << p->getPassName() << "\n");
          legacy::FunctionPassManager::add(p);
        }
    };
    class MyModulePassManager : public legacy::PassManager {
      public:
        virtual void add(Pass *p) override {
          DEBUG_MSG("Adding module pass: " << p->getPassName() << "\n");
          legacy::PassManager::add(p);
        }
    };
    MyFunctionPassManager fpm(module.get());
    MyModulePassManager mpm;;

    PassManagerBuilder b;
    b.OptLevel = 3;
#if LLVM_VERSION >= 50
    b.Inliner = createFunctionInliningPass(b.OptLevel, 0, false);
#else
    b.Inliner = createFunctionInliningPass(b.OptLevel, 0);
#endif
    b.LoopVectorize = true;
    b.SLPVectorize = true;
    b.populateFunctionPassManager(fpm);

    // Run optimization passes
    fpm.doInitialization();
    for (Module::iterator i = module->begin(); i != module->end(); i++) {
      fpm.run(*i);
    }
    fpm.doFinalization();
    mpm.run(*module);
  }

  std::unique_ptr<llvm::raw_fd_ostream> CodeGenContext::make_raw_fd_ostream(const std::string &filename) {
    std::string error_string;
    std::error_code err;
    std::unique_ptr<raw_fd_ostream> raw_out(new raw_fd_ostream(filename, err, sys::fs::F_None));
    if (err) error_string = err.message();
    cerr << "Error opening output " << filename << ": " << error_string << "\n";
    return raw_out;
  }

  void CodeGenContext::compile_to_bitcode(const std::string& filename) {
    assert (module && "No module defined. Must call compile before calling compile_to_bitcode");
    std::string error_string;
    auto output = make_raw_fd_ostream(filename.c_str());
    llvm::Module &mod = *module;
    LLVMOStream &out = *output;
    WriteBitcodeToFile(&mod, out);
  }

  /* Get the llvm type corresponding to a Nbody type */
  llvm::Type* CodeGenContext::llvm_get_type (LLVMContext *c, Nbody::Type t)
  {
    if (t.nelem == 1) {
      if (t.is_float()) {
        switch (t.bits) {
          case 32:
            return llvm::Type::getFloatTy(*c);
          case 64:
            return llvm::Type::getDoubleTy(*c);
          default:
            std::cerr << "There is no llvm type match." << endl;
            return NULL;
        }
      }
      else if (t.is_handle())
        return llvm::Type::getInt8PtrTy(*c);
      else {
        // assert (t.is_int());
        return llvm::Type::getIntNTy(*c, t.bits);
      }
    }
    else {
      llvm::Type* etype = llvm_get_type(c, t.element_type());
      return VectorType::get(etype, t.nelem);
    }
  }

  llvm::Type *CodeGenContext::llvm_get_type (Type t)
  {
    return llvm_get_type(context, t);
  }

  /* Generate code that evaluates an expression */
  Value *CodeGenContext::codegen (Expr e)
  {
    assert (e.defined());
    value = NULL;
    e.accept (this);
    assert (value);
    return value;
  }

  void CodeGenContext::codegen (Stmt s) {
    assert (s.defined());
    value = NULL;
    s.accept (this);
  }

  void CodeGenContext::sym_push (const std::string &name, llvm::Value *value) {
	  symKey.insert(name);
    symbol_table[name] = value;
  }

  void CodeGenContext::sym_pop (const std::string &name) {
    symbol_table.erase (name);
  }

  bool CodeGenContext::sym_exists (const std::string &name) {
    if (symbol_table.find(name) == symbol_table.end())
      return false;
    else
      return true;
  }

  llvm::Value *CodeGenContext::sym_get (const std::string &name) {
    if (!sym_exists(name)) {
      std::cerr << "Undeclared variable: " << name << endl;
	  DEBUG_MSG("Sym List\n");
		for (std::string const& key : symKey)
		{
			DEBUG_MSG(key << "\n");
			std::cout << key << "\n";
		}
		DEBUG_MSG("End Sym List \n");
      return NULL;
    }
    return symbol_table[name];
  }

  void CodeGenContext::unpack_buffer (string name, llvm::Value* buffer) {
    Value *data_ptr = buffer_data(buffer);
    // Populate the symbol table with data
    sym_push (name + ".buffer", buffer);
    sym_push (name + ".data", data_ptr);

    sym_push (name + ".begin.0", buffer_begin(buffer, 0));
    sym_push (name + ".begin.1", buffer_begin(buffer, 1));
    sym_push (name + ".end.0", buffer_end(buffer, 0));
    sym_push (name + ".end.1", buffer_end(buffer, 1));
    sym_push (name + ".stride.0", buffer_stride(buffer, 0));
    sym_push (name + ".stride.1", buffer_stride(buffer, 1));
    sym_push (name + ".elem_size", buffer_elem_size(buffer));
  }

  StructType* CodeGenContext::get_buffer_type() {
    StructType* buffer_type = module->getTypeByName("struct.buffer_t");
    assert (buffer_type && "Did not find buffer_t");
    return buffer_type;
  }

  Value* CodeGenContext::buffer_data (Value* buffer) {
    return builder->CreateLoad(buffer_data_ptr(buffer));
  }

  Value* CodeGenContext::buffer_begin (Value* buffer, int i) {
    return builder->CreateLoad(buffer_begin_ptr(buffer, i));
  }

  Value* CodeGenContext::buffer_end (Value* buffer, int i) {
    return builder->CreateLoad(buffer_end_ptr(buffer, i));
  }

  Value* CodeGenContext::buffer_stride (Value* buffer, int i) {
    return builder->CreateLoad(buffer_stride_ptr(buffer, i));
  }

  Value* CodeGenContext::buffer_elem_size (Value* buffer) {
    return builder->CreateLoad(buffer_elem_size_ptr(buffer));
  }

  Value* CodeGenContext::buffer_data_ptr(Value* buffer){
    return builder->CreateConstInBoundsGEP2_32(buffer_t_type, buffer, 0, 0, "buf_data");
  }

  Value* CodeGenContext::buffer_begin_ptr(Value* buffer, int i){
    Value* zero = ConstantInt::get(i32, 0);
    Value* field = ConstantInt::get(i32, 1);
    Value* index = ConstantInt::get(i32, i);
    std::vector<Value*> args = vec(zero, field, index);
    return builder->CreateInBoundsGEP(buffer, args, "buf_begin");
  }

  Value* CodeGenContext::buffer_end_ptr(Value* buffer, int i){
    Value* zero = ConstantInt::get(i32, 0);
    Value* field = ConstantInt::get(i32, 2);
    Value* index = ConstantInt::get(i32, i);
    std::vector<Value*> args = vec(zero, field, index);
    return builder->CreateInBoundsGEP(buffer, args, "buf_end");
  }

  Value* CodeGenContext::buffer_stride_ptr(Value* buffer, int i){
    Value* zero = ConstantInt::get(i32, 0);
    Value* field = ConstantInt::get(i32, 3);
    Value* index = ConstantInt::get(i32, i);
    std::vector<Value*> args = vec(zero, field, index);
    return builder->CreateInBoundsGEP(buffer, args, "buf_stride");
  }

  Value* CodeGenContext::buffer_elem_size_ptr(Value* buffer){
    return builder->CreateConstInBoundsGEP2_32(buffer_t_type, buffer, 0, 4, "data_size_elem");
  }

  Value* CodeGenContext::codegen_data_ptr(string buffer, Type type, Value* index)
  {
    // Get the base address from the symbol table
	Value* base_addr;
	if(sym_exists (buffer)){
		base_addr = sym_get (buffer);
	}
    else{
		base_addr = sym_get (buffer + ".data");
	}
    llvm::Type* base_addr_type = base_addr->getType();

    unsigned addr_space = base_addr_type->getPointerAddressSpace();
    llvm::Type* load_type = llvm_get_type(type)->getPointerTo(addr_space);

    if (load_type != base_addr_type)
      base_addr = builder->CreatePointerCast (base_addr, load_type);

    return builder->CreateInBoundsGEP(base_addr, index);
  }

  /* Code Generation */
  void CodeGenContext::visit (const Integer* op)
  {
    value = ConstantInt::get(i32, op->value, true);
    DEBUG_MSG("Creating integer: " << op->value << endl);
  }

  void CodeGenContext::visit (const Flt* op)
  {
    value = ConstantFP::get(f32, op->value);
    DEBUG_MSG("Creating floating point: " << op->value << endl);
  }

  void CodeGenContext::visit (const Dbl* op)
  {
    value = ConstantFP::get(f64, op->value);
    DEBUG_MSG("Creating floating point: " << op->value << endl);
  }

  void CodeGenContext::visit (const String* op)
  {
    value = ConstantFP::get(f64, op->value);
    DEBUG_MSG("Creating string constant: " << op->value << endl);
  }

  void CodeGenContext::visit (const Cast* op)
  {
    value = codegen (op->value);
    Type src_ty = op->value.type();
    Type dst_ty = op->type;
    llvm::Type *llvm_dst_ty = llvm_get_type (dst_ty);

    if (src_ty.is_int() && dst_ty.is_int())
      value = builder->CreateIntCast (value, llvm_dst_ty, src_ty.is_int());
    else if (src_ty.is_int() && dst_ty.is_float())
      value = builder->CreateSIToFP (value, llvm_dst_ty);
    else if (src_ty.is_float() && dst_ty.is_int())
      value = builder->CreateFPToSI (value, llvm_dst_ty);
    else {
      assert (src_ty.is_float() && dst_ty.is_float());
      value = builder->CreateFPCast (value, llvm_dst_ty);
    }
  }

  void CodeGenContext::visit (const Variable* op)
  {
    DEBUG_MSG("Look-up variable " << op->name << endl);
  	if(NamedValues.find(op->name) != NamedValues.end()){
  		DEBUG_MSG("Variable " << op->name << " found in assigned memory. Reading from there! \n");
  		value =  builder->CreateLoad(NamedValues[op->name] , op->name.c_str());
  	}
  	else{
  		value = sym_get (op->name);
  	}
  }

  void CodeGenContext::visit (const Add* op)
  {
    DEBUG_MSG("Creating Add \n");
    if (op->type.is_float())
      value = builder->CreateFAdd(codegen(op->a), codegen(op->b), "FAdd");
    else
      value = builder->CreateAdd(codegen(op->a), codegen(op->b), "Add");
  }

  void CodeGenContext::visit (const Sub* op)
  {
    DEBUG_MSG("Creating Sub \n");
    if (op->type.is_float())
      value = builder->CreateFSub(codegen(op->a), codegen(op->b), "FSub");
    else
      value = builder->CreateSub(codegen(op->a), codegen(op->b), "Sub");
  }

  void CodeGenContext::visit (const Mul* op)
  {
    DEBUG_MSG("Creating Mul \n");
    if (op->type.is_float())
      value = builder->CreateFMul(codegen(op->a), codegen(op->b), "FMul");
    else
      value = builder->CreateMul(codegen(op->a), codegen(op->b), "Mul");
  }

  void CodeGenContext::visit (const Div* op)
  {
    DEBUG_MSG("Creating Div \n");
    if (op->type.is_float())
      value = builder->CreateFDiv(codegen(op->a), codegen(op->b), "FDiv");
    else
      value = builder->CreateSDiv(codegen(op->a), codegen(op->b), "Div");
  }

  void CodeGenContext::visit (const Mod* op)
  {
    DEBUG_MSG("Creating Mod \n");
    if (op->type.is_float())
      value = builder->CreateFRem(codegen(op->a), codegen(op->b), "FRem");
    else
      value = builder->CreateSRem(codegen(op->a), codegen(op->b), "Rem");
  }

  void CodeGenContext::visit (const Max* op)
  {
    DEBUG_MSG("Creating Max \n");
    if (op->type.is_float())
		value = builder->CreateSelect(builder->CreateFCmpOGE(codegen(op->a) , codegen(op->b)) , codegen(op->a) , codegen(op->b));
    else
		value = builder->CreateSelect(builder->CreateICmpSGE(codegen(op->a) , codegen(op->b)) , codegen(op->a) , codegen(op->b));
  }

  void CodeGenContext::visit (const Min* op)
  {
    DEBUG_MSG("Creating Min \n");
    if (op->type.is_float())
		value = builder->CreateSelect(builder->CreateFCmpOLT(codegen(op->a) , codegen(op->b)) , codegen(op->a) , codegen(op->b));
    else
		value = builder->CreateSelect(builder->CreateICmpSLT(codegen(op->a) , codegen(op->b)) , codegen(op->a) , codegen(op->b));
  }

  void CodeGenContext::visit (const Select* op)
  {
    DEBUG_MSG("Creating Select \n");
	if (op->type.is_float())
		value = builder->CreateSelect(codegen(op->cond) , codegen(op->true_value) , codegen(op->false_value));
    else
		value = builder->CreateSelect(codegen(op->cond) , codegen(op->true_value) , codegen(op->false_value));
  }

  void CodeGenContext::visit (const ComparisonOp* op)
  {
    DEBUG_MSG("Creating comparison operation " << op->op << endl);
    mapcop["=="] = EQ;
    mapcop["!="] = NE;
    mapcop["<="] = LE;
    mapcop[">="] = GE;
    mapcop["<"]  = LT;
    mapcop[">"]  = GT;
	  mapcop["&&"]  = AND;
	  mapcop["||"]  = OR;
    if (op->a.type().is_float()) {
        switch (mapcop[op->op]) {
           case EQ: 	  value = builder->CreateFCmpOEQ(codegen(op->a), codegen(op->b), "FEQ"); break;
           case NE: 	  value = builder->CreateFCmpONE(codegen(op->a), codegen(op->b), "FNE"); break;
           case LT: 		value = builder->CreateFCmpOLT(codegen(op->a), codegen(op->b), "FLT"); break;
           case GT: 		value = builder->CreateFCmpOGT(codegen(op->a), codegen(op->b), "FGT"); break;
           case LE: 		value = builder->CreateFCmpOLE(codegen(op->a), codegen(op->b), "FLE"); break;
           case GE: 		value = builder->CreateFCmpOGE(codegen(op->a), codegen(op->b), "FGE"); break;
           case AND:		value = builder->CreateAnd(codegen(op->a)    , codegen(op->b) , "FAND"); break;
           case OR:	  	value = builder->CreateOr(codegen(op->a)     , codegen(op->b) , "FFOR"); break;
		    default: std::cout << op->op; assert(false && (" ComparisonOp operator not found!"));
      }
    }
    else {
      switch (mapcop[op->op]) {
        case EQ: 	  value = builder->CreateICmpEQ(codegen(op->a), codegen(op->b), "EQ"); break;
        case NE: 	  value = builder->CreateICmpNE(codegen(op->a), codegen(op->b), "NE"); break;
        case LT: 		value = builder->CreateICmpSLT(codegen(op->a), codegen(op->b), "LT"); break;
        case GT: 		value = builder->CreateICmpSGT(codegen(op->a), codegen(op->b), "GT"); break;
        case LE: 		value = builder->CreateICmpSLE(codegen(op->a), codegen(op->b), "LE"); break;
        case GE: 		value = builder->CreateICmpSGE(codegen(op->a), codegen(op->b), "GE"); break;
		    case AND:		value = builder->CreateAnd(codegen(op->a) , codegen(op->b) , "AND"); break;
		    case OR:		value = builder->CreateOr(codegen(op->a) , codegen(op->b) , "FOR"); break;
		    default: std::cout << op->op; assert(false && (" ComparisonOp operator not found!"));
      }
    }

  }

  void CodeGenContext::visit (const ComparisonOpI* op)
  {
    DEBUG_MSG("Creating comparison operation " << op->op << endl);
    mapcop["=="] = EQ;
    mapcop["!="] = NE;
    mapcop["<="] = LE;
    mapcop[">="] = GE;
    mapcop["<"]  = LT;
    mapcop[">"]  = GT;
    mapcop["&&"]  = AND;
    mapcop["||"]  = OR;

    if (op->a.type().is_float()) {
      switch (mapcop[op->op]) {
        case EQ: 	  value = builder->CreateSelect(builder->CreateFCmpOEQ(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        case NE: 	  value = builder->CreateSelect(builder->CreateFCmpONE(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        case LT:    value = builder->CreateSelect(builder->CreateFCmpOLT(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        case GT: 		value = builder->CreateSelect(builder->CreateFCmpOGT(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        case LE: 		value = builder->CreateSelect(builder->CreateFCmpOLE(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        case GE: 		value = builder->CreateSelect(builder->CreateFCmpOGE(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        case AND:		value = builder->CreateSelect(builder->CreateAnd(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        case OR:		value = builder->CreateSelect(builder->CreateOr(codegen(op->a) , codegen(op->b)) , ConstantFP::get(f32, 1) , ConstantFP::get(f32, 0)); break;
        default: std::cout << op->op; assert(false && (" ComparisonOpI operator not found!"));
      }
    }
    else {
      switch (mapcop[op->op]) {
        case EQ: 	  value = builder->CreateSelect(builder->CreateICmpEQ(codegen(op->a), codegen(op->b), "EQ"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        case NE: 	  value = builder->CreateSelect(builder->CreateICmpNE(codegen(op->a), codegen(op->b), "NE"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        case LT: 		value = builder->CreateSelect(builder->CreateICmpSLT(codegen(op->a), codegen(op->b), "LT"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        case GT: 		value = builder->CreateSelect(builder->CreateICmpSGT(codegen(op->a), codegen(op->b), "GT"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        case LE: 		value = builder->CreateSelect(builder->CreateICmpSLE(codegen(op->a), codegen(op->b), "LE"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        case GE: 		value = builder->CreateSelect(builder->CreateICmpSGE(codegen(op->a), codegen(op->b), "GE"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        case AND:		value = builder->CreateSelect(builder->CreateAnd(codegen(op->a) , codegen(op->b) , "AND"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        case OR:		value = builder->CreateSelect(builder->CreateOr(codegen(op->a) , codegen(op->b) , "OR"), ConstantInt::get(i32, 1) , ConstantInt::get(i32, 0)); break;
        default: std::cout << op->op; assert(false && (" ComparisonOpI operator not found!"));
      }
    }
  }


  void CodeGenContext::visit (const Call* op)
  {
    llvm::Function* func;
    vector<Value*> args(op->args.size());
    for (size_t i = 0; i < op->args.size(); i++) {
      args[i] = codegen(op->args[i]);
    }
    if (op->ctype == Call::Intrinsic)
      func = module->getFunction("llvm.x86." + op->name);
    else
      func = module->getFunction (op->name);

    llvm::Type *res_type = llvm_get_type (op->type);

    // If the function name doesn't exist, assume it's external
    if (!func) {
      if (op->ctype == Call::Intrinsic)
        DEBUG_MSG("Did not find function " << "llvm.x86." + op->name << " in module. Assuming it's an external function." << endl);
      else
        DEBUG_MSG("Did not find function " << op->name << " in module. Assuming it's an external function." << endl);
      vector<llvm::Type*> arg_type(args.size());
      for (size_t i = 0; i < args.size(); i++) {
        arg_type[i] = args[i]->getType();
      }

      FunctionType *func_ty = FunctionType::get (res_type, arg_type, false);

      if (op->ctype == Call::Intrinsic)
        func = llvm::Function::Create (func_ty, llvm::Function::ExternalLinkage, "llvm.x86." + op->name, module.get());
      else
        func = llvm::Function::Create (func_ty, llvm::Function::ExternalLinkage, op->name, module.get());
      func->setCallingConv (CallingConv::C);
	  externFunctions[op->name] = func;
    }
    else {
      FunctionType *func_ty = func->getFunctionType();
      for (size_t i = 0; i < args.size(); i++) {
        if (op->args[i].type().is_handle()) {
          llvm::Type *t = func_ty->getParamType(i);
          if (t != args[i]->getType())
            args[i] = builder->CreatePointerCast (args[i], t);
        }
      }
    }

    CallInst *call = builder->CreateCall (func, args);
    value = call;
  }

  void CodeGenContext::visit (const Vector* op)
  {
    DEBUG_MSG("Vector...\n");
    if(is_const(op->stride) && !is_const(op->base)) {
      DEBUG_MSG("Vector if...\n");
      Expr base = VectorSet::make(op->base, op->width);
      Expr stride = Vector::make(make_zero(op->base.type()), op->stride, op->width);
      value = codegen(base + stride);
    }
    else {
      DEBUG_MSG( "Vector else...\n");
      Value* base = codegen(op->base);
      Value* stride = codegen(op->stride);

      value = UndefValue::get(llvm_get_type(op->type));
      value = builder->CreateInsertElement(value, base, ConstantInt::get(i32, 0));
      for (int i = 1; i < op->type.nelem; i++) {
        if (op->type.is_float())
          base = builder->CreateFAdd(base, stride);
        else
          base = builder->CreateNSWAdd(base, stride);
        value = builder->CreateInsertElement(value, base, ConstantInt::get(i32, i));
      }
    }
  }

  void CodeGenContext::visit (const VectorSet* op)
  {
    DEBUG_MSG("Vector set...\n");
    Value* v = codegen(op->value);
    value = builder->CreateVectorSplat(op->width, v);
  }

  void CodeGenContext::visit (const Assign* op)
  {
	if(op->staticAssign){
		sym_push (op->name, codegen(op->value));
		return;
	}
	if(NamedValues.find(op->name) == NamedValues.end()){
		//Store for the first time
		DEBUG_MSG("Allocating memory for assigned memory item " << op->name << "\n");
		llvm::AllocaInst* alloca = builder->CreateAlloca(llvm_get_type(op->value.type()), 0, op->name.c_str());
		NamedValues[op->name] = alloca;
		builder->CreateStore(codegen(op->value) , alloca);

	}
	else{
		//Reassign
		DEBUG_MSG("Reassigning item in assigned memory " << op->name << "\n");
		builder->CreateStore(codegen(op->value) , NamedValues[op->name]);
	}

  }

  void CodeGenContext::visit (const Let* op)
  {
    sym_push (op->name, codegen(op->value));
    value = codegen(op->body);
    sym_pop (op->name);
  }

  void CodeGenContext::visit (const LetStmt* op)
  {
    sym_push (op->name, codegen(op->value));
    codegen(op->body);
    sym_pop (op->name);
  }

  void CodeGenContext::visit (const Load* op)
  {
    DEBUG_MSG("Loading identifier: " << op->name << endl);
	if(op->lt == Load::LoadType::REFERENCE){
		Value * realIndex =  builder->CreateMul(codegen(op->index), ConstantInt::get(i32, op->intStride, true), "Mul");
		value = codegen_data_ptr(op->name, op->type, realIndex);
		return;
	}
	if(op->lt == Load::LoadType::DEREFERENCE){
		Value* ptr = builder->CreateInBoundsGEP(codegen(op->pointer), codegen(op->index));
		value = builder->CreateLoad(ptr);
		return;
	}

  if(op->lt == Load::LoadType::VALUE) {
      // Scalar loads
      Value* ptr = codegen_data_ptr(op->name, op->type, codegen(op->index));
      LoadInst* load = builder->CreateLoad(ptr);
      value = load;
    }
    else if(op->type.is_scalar()) {
      // Scalar loads
      Value* ptr = codegen_data_ptr(op->name, op->type, codegen(op->index));
      LoadInst* load = builder->CreateLoad(ptr);
      value = load;
    }
    else {
      int alignment = op->type.bytes();
      const Integer* stride = op->stride.ir<Integer>();
      Value* lane = ConstantInt::get(i32, 0);
      Value* idx = codegen(op->index);
      idx = builder->CreateExtractElement(idx, lane);
      if (stride->value == 1) {
        // Sequential vector loads (stride=1)
        Value* ptr = codegen_data_ptr(op->name, op->type.element_type(), idx);
        ptr = builder->CreatePointerCast(ptr, llvm_get_type(op->type)->getPointerTo());
        LoadInst* load = builder->CreateAlignedLoad(ptr, alignment);
        value = load;
      }
      else {
        // Compute the index as scalars and gather into a vector
        Value* ptr = codegen_data_ptr(op->name, op->type.element_type(), idx);
        Value* stride = codegen(op->stride);
        value = UndefValue::get(llvm_get_type(op->type));
        for (int i = 0; i < op->type.nelem; i++) {
          Value* lane = ConstantInt::get(i32, i);
          LoadInst* load = builder->CreateLoad(ptr);
          value = builder->CreateInsertElement(value, load, lane);
          ptr = builder->CreateInBoundsGEP(ptr, stride);
        }
      }
    }
  }

  void CodeGenContext::visit (const Store* op)
  {
    Type vtype = op->value.type();
    DEBUG_MSG("Creating assignment for " << op->name << endl);
    if (vtype.is_scalar()) {
      // Scalar stores
      Value *ptr = codegen_data_ptr (op->name, vtype, codegen(op->index));
      if (ptr)
        StoreInst* store = builder->CreateStore (codegen(op->value), ptr, false);
    }
    else {
      int alignment = vtype.bytes();
      Value* lane = ConstantInt::get(i32, 0);
      Value* idx = codegen(op->index);
      idx = builder->CreateExtractElement(idx, lane);
      if (is_one(op->stride)) {
        // Sequential vector stores (stride=1)
        Value* ptr = codegen_data_ptr(op->name, vtype.element_type(), idx);
        ptr = builder->CreatePointerCast(ptr, llvm_get_type(vtype)->getPointerTo());
        StoreInst* store = builder->CreateAlignedStore(codegen(op->value), ptr, alignment);
      }
      else {
        // Compute the index as scalars and gather into a vector
        Value* ptr = codegen_data_ptr(op->name, vtype.element_type(), idx);
        Value* stride = codegen(op->stride);
        value = UndefValue::get(llvm_get_type(vtype));
        for (int i = 0; i < vtype.nelem; i++) {
          Value* lane = ConstantInt::get(i32, i);
          Value* val = codegen(op->value);
          Value* v = builder->CreateExtractElement(val, lane);
          StoreInst* store = builder->CreateStore (v, ptr);
          ptr = builder->CreateInBoundsGEP(ptr, stride);
        }
      }
    }
  }

  void CodeGenContext::visit (const For* op)
  {
    DEBUG_MSG("Creating for-loop: " << op->name << endl);

    if (op->ftype == For::Serial) {
      Value* min = codegen(op->begin);
      Value* max = codegen(op->end);
      Value* stride = codegen(op->stride);
      Value* next = builder->CreateAdd (min, stride);

      BasicBlock *preheader_bb = builder->GetInsertBlock();

      // Create a new basic block for the for loop, inserting after current block
      BasicBlock *loop_bb = BasicBlock::Create (*context, op->name + "_loop", function);

      // Create a basic block for the "after loop"
      BasicBlock *after_bb = BasicBlock::Create (*context, op->name + "_after_loop", function);

      // Create an explicit fall through from current block to loop_bb
      Value *enter_cond = builder->CreateICmpSLE (next, max);
      builder->CreateCondBr (enter_cond, loop_bb, after_bb);
      builder->SetInsertPoint (loop_bb);

      // Create phi node
      PHINode *phi = builder->CreatePHI (i32, 2);
      phi->addIncoming (min, preheader_bb);

      // Within the loop, the variable is equal to the phi node
      sym_push (op->name, phi);

      // Emit the body of the loop
      codegen (op->body);

      // Compute the next value of the iteration by adding stride
      Value *next_var = builder->CreateNSWAdd (phi, stride);

      // Add a new entry to the phi node for the back edge
      phi->addIncoming (next_var, builder->GetInsertBlock());

      // Compute the end condition
      Value *end = builder->CreateNSWAdd (next_var, stride);
      Value *end_cond = builder->CreateICmpSLE (end, max);

      // Insert the conditional branch into the end
      builder->CreateCondBr (end_cond, loop_bb, after_bb);

      // Any new code will be inserted after 'after_bb'
      builder->SetInsertPoint (after_bb);

      // Pop the loop variable from the scope
      sym_pop (op->name);
    }
    else
      assert (false && "Unknown for type");
  }

  void CodeGenContext::visit (const While * op)
  {

	    BasicBlock *preheader_bb = builder->GetInsertBlock();

      // Create a new basic block for the for loop, inserting after current block
      BasicBlock *loop_bb = BasicBlock::Create (*context, op->name + "_loop", function);

      // Create a basic block for the "after loop"
      BasicBlock *after_bb = BasicBlock::Create (*context, op->name + "_after_loop", function);

      // Create an explicit fall through from current block to loop_bb
    	Value *enter_cond = codegen(op->condition);
    	builder->CreateCondBr (enter_cond, loop_bb, after_bb);
    	builder->SetInsertPoint (loop_bb);

    	codegen (op->body);

	    // Insert the conditional branch into the end
    	builder->CreateCondBr (codegen(op->condition), loop_bb, after_bb);

    	// Any new code will be inserted after 'after_bb'
    	builder->SetInsertPoint (after_bb);

    	// Pop the loop variable from the scope
    	sym_pop (op->name);


  }

  void CodeGenContext::visit (const Evaluate* op)
  {
    codegen (op->value);
  }

  void CodeGenContext::visit (const Block* op)
  {
    for (size_t i = 0; i < op->stmts.size(); i++)
      codegen (op->stmts[i]);
  }

  void CodeGenContext::visit (const BlockStmt* op)
  {
    for (size_t i = 0; i < op->stmts.size(); i++)
      codegen (op->stmts[i]);
  }

  void CodeGenContext::visit (const OrderedArray_Insert * op)
  {
   codegen(op->value);
  }
   void CodeGenContext::visit (const rsqrt * op)
   {
    codegen(op->value);
   }



}
