#ifndef N_BODY_EXTERNAL_QUEUE_FUNCTION
#define N_BODY_EXTERNAL_QUEUE_FUNCTION

#include <string>
#include <vector>
#include <utility>
#include <math.h>
#include <map>
#include <tuple>
#include <assert.h>
#include <mkl.h>
#include "kNN_vector.h"

using namespace std;

/**
Each list operator must have these set of functions described here.

PushOnto
	Pushes the value and index onto the external structure
	Args
		val - float result we want to store
		index - index of the result

LoadPointsOnto
	Loads the values from the pointer to pointer + k onto the external structure
	Args
		val - pointer to the IRS value object
		index - pointer to the IRS index object
		k - shows how many values to load

StoreIntoArray
	Stores the external structure values onto the pointer to pointer + k
	This is used to write to the external output object
	Args
		valAddress - What to store to
		k - How many values to store

StoreIntoIRS
	Stores the external structure values onto the IRS value and index objects  from pointer to pointer + kbhit
	Args
		valAddress - pointer to the IRS value object
		indexAddress - pointer to the IRS index object
		k - How many values to store

Empty
	Empty the external structure
	It currently is not being used but it is good to have
**/

namespace Nbody {

	/**
	KMax Comparison
	**/

	struct KMaxOperatorComparison{
		bool operator()(const std::pair<float , int>  &lhs, const std::pair<float , int>  &rhs) const{
			return lhs.first > rhs.first;
		}
	};

	kNN_vector<std::pair<float , int> , KMaxOperatorComparison> * KMaxPQ = new kNN_vector<std::pair<float , int> , KMaxOperatorComparison>(3);

	extern "C"
	void KMaxPushOntoPriorityQueue(float val , int index , int k){
		KMaxPQ->push_back(std::make_pair(val , index));
	}

	extern "C"
	void KMaxLoadPointsOntoPQ(float * val , int * index , int k){
		KMaxPQ->clear();
		for(int i = 0; i != k; ++i){
			KMaxPQ->push_back(std::make_pair(val[i] , index[i]));
		}
	}

	extern "C"
	void KMaxStorePQIntoArray(float * valAddress , int k){
		for(int i = 0; i < k; ++i){
			valAddress[i] = KMaxPQ[0][i].first;
		}
	}

	extern "C"
	void KMaxStorePQIntoIRS(float * valAddress , int * indexAddress , int k){
		for(int i = 0; i < k; ++i){
			valAddress[i] = KMaxPQ[0][i].first;
			indexAddress[i] =  KMaxPQ[0][i].second;
		}
	}

	extern "C"
	void KMaxEmptyPriortyQueue(){
		KMaxPQ->clear();
	}


	/**
	KMin Comparison
	**/
	struct KMinOperatorComparison{
		bool operator()(const std::pair<float , int> &lhs, const std::pair<float , int> &rhs) const{
			return lhs.first < rhs.first;
		}
	};

	kNN_vector<std::pair<float , int> , KMinOperatorComparison> * KMinPQNew = new kNN_vector<std::pair<float , int> , KMinOperatorComparison>(3);

	extern "C"
	void KMinPushOntoPriorityQueue(float val , int index , int k){
		// KMinPQNew->push_back(std::make_pair(val , index));
	}

	extern "C"
	void KMinLoadPointsOntoPQ(float * val , int * index , int k){
		// KMinPQNew->clear();
		// for(int i = 0; i != k; ++i){
		// 	KMinPQNew->push_back(std::make_pair(val[i] , index[i]));
		// }
	}


	extern "C"
	void KMinStorePQIntoArray(float * valAddress , int k){
		// for(int i = 0; i < k; ++i){
		// 	valAddress[i] = KMinPQNew[0][i].first;
		// }
	}

	extern "C"
	void KMinStorePQIntoIRS(float * valAddress , int * indexAddress , int k){
		// for(int i = 0; i < k; ++i){
		// 	valAddress[i] = KMinPQNew[0][i].first;
		// 	indexAddress[i] =  KMinPQNew[0][i].second;
		// }
	}

	extern "C"
	void KMinEmptyPriortyQueue(){
		// KMinPQNew->clear();
	}


	/**
	KArgMin Comparison
	**/
	struct KArgMinOperatorComparison{
		// bool operator()(const std::pair<float , int>   &lhs, const  std::pair<float , int>  & rhs) const{
		// 	return lhs.first < rhs.first;
		// }
	};

	kNN_vector<std::pair<float , int> , KArgMinOperatorComparison> * KArgMinPQ = new kNN_vector<std::pair<float , int> , KArgMinOperatorComparison>(3);

	extern "C"
	void KArgMinPushOntoPriorityQueue(float val , int index , int k){
		// KArgMinPQ->push_back(std::make_pair(val , index));
	}

	extern "C"
	void KArgMinLoadPointsOntoPQ(float * val , int * index , int k){
		// KArgMinPQ->clear();
		// for(int i = 0; i != k; ++i){
		// 	KArgMinPQ->push_back(std::make_pair(val[i] , index[i]));
		// }
	}


	extern "C"
	void KArgMinStorePQIntoArray(float * valAddress , int k){
		// for(int i = 0; i < k; ++i){
		// 	valAddress[i] = KArgMinPQ[0][i].first;
		// }
	}

	extern "C"
	void KArgMinStorePQIntoIRS(float * valAddress , int * indexAddress , int k){

		// for(int i = 0; i < k; ++i){
		// 	valAddress[i] = KArgMinPQ[0][i].first;
		// 	indexAddress[i] =  KArgMinPQ[0][i].second;
		// }
	}

	extern "C"
	void KArgMinEmptyPriortyQueue(){
		// KArgMinPQ->clear();
	}

	/**
	KArgMax Comparison
	**/

	struct KArgMaxOperatorComparison{
		bool operator()(const std::pair<float , int>  &lhs, const  std::pair<float , int>  &rhs) const{
			return lhs.first > rhs.first;
		}
	};

	kNN_vector<std::pair<float , int> , KArgMaxOperatorComparison> * KArgMaxPQ = new kNN_vector<std::pair<float , int> , KArgMaxOperatorComparison>(3);

	extern "C"
	void KArgMaxPushOntoPriorityQueue(float val , int index , int k){
		KArgMaxPQ->push_back(std::make_pair(val , index));
	}

	extern "C"
	void KArgMaxLoadPointsOntoPQ(float * val , int * index , int k){
		KArgMaxPQ->clear();
		for(int i = 0; i != k; ++i){
			KArgMaxPQ->push_back(std::make_pair(val[i] , index[i]));
		}
	}

	extern "C"
	void KArgMaxStorePQIntoArray(float * valAddress , int k){
		for(int i = 0; i < k; ++i){
			valAddress[i] = KArgMaxPQ[0][i].first;
		}
	}

	extern "C"
	void KArgMaxStorePQIntoIRS(float * valAddress , int * indexAddress , int k){
		for(int i = 0; i < k; ++i){
			valAddress[i] = KArgMaxPQ[0][i].first;
			indexAddress[i] =  KArgMaxPQ[0][i].second;
		}
	}

	extern "C"
	void KArgMaxEmptyPriortyQueue(){
		KArgMaxPQ->clear();
	}

	/**
	UnionArg Comparison
	**/

	std::vector<std::pair<float , int>> UnionArgVec;

	extern "C"
	void UnionArgPushOntoPriorityQueue(float val , int index){
		if(val == 1){
			UnionArgVec.push_back(std::make_pair(val , index));
		}
	}

	extern "C"
	void UnionArgLoadPointsOntoPQ(float * val , int * index , int k){
		UnionArgVec.clear();
		for(int i = 0; i != k; ++i){
			if(val[i] == -1){
				break;
			}
			UnionArgVec.push_back(std::make_pair(val[i] , index[i]));
		}
	}

	extern "C"
	void UnionArgStorePQIntoArray(float * valAddress , int k){
		int s = UnionArgVec.size();
		for(int i = 0; i != s; ++i){
			valAddress[i] = UnionArgVec[i].second;
		}
	}

	extern "C"
	void UnionArgStorePQIntoIRS(float * valAddress , int * indexAddress , int k){
		int s = UnionArgVec.size();
		for(int i = 0; i != s; ++i){
			valAddress[i] = UnionArgVec[i].first;
			indexAddress[i] = UnionArgVec[i].second;
		}
	}

	extern "C"
	void UnionArgEmptyPriortyQueue(){
		UnionArgVec.clear();
	}

}

namespace Nbody {




	/**
 * This function will get the will measures L as --> sigma = L L^T
 * which L is the lower triangular matrix for sigma
 * also will return the determinant of that sigma
*/
extern "C"
real_t Cholesky(real_t** sigmai, real_t** L, int Dim) {
  /* Initialize L to zero */
  real_t determ = 1;
  for(size_t i = 0; i < Dim; ++i) {
    for(size_t j = 0; j < (i+1); ++j) {
      double sum = 0;
      for (size_t k = 0; k<j ; ++k) {
        sum += L[i][k] * L[j][k];
      }
			L[i][j] = (i == j)? std::sqrt(sigmai[i][i] - sum) : (1.0 /L[j][j]*(sigmai[i][j]- sum));
    }
    determ *= L[i][i];
  }
  determ *= determ;
  return determ;
}


/**
 * Calculating the Gaussian model (multivariate pdf)
 * using Eigen library  for transpose and inverse of matrix
 */
extern "C"
real_t Gaussian(real_t*  datai, real_t* mui, real_t** sigmai, int Dim, int p) {

	real_t res[Dim];
  double inner_product = 0;
	real_t** L = new real_t*[Dim];
  for(size_t i = 0 ; i < Dim; ++i)
    L[i] = new real_t[Dim];

	real_t determ = Cholesky(sigmai, L, Dim);
  res[0] = (datai[0]-mui[0])/L[0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = datai[i]-mui[0];
    for (size_t j = 0; j < i; ++j)
      temp -= L[i][j] * res[j];
    res[i] = temp / L[i][i];
    inner_product += res[i]*res[i];
  }
  // cerr << "f: " << f << ", det: " << determinant_all[f] << endl;
  real_t result = determ * std::exp(-0.5 * inner_product) * p;
  return result;

}




	extern "C"
	double Determinant(double* x, int a){

	    int info;
			int n = 2;
	    double Det = 0;
	    //take the Cholesky square root
			double *C = new double[4];

  		for(int i = 0; i < n*n; i++) C[i] = x[i];
	    dpotrf("L", &n, C, &n, &info);

	    //get the determinant
	    for(int i = 0; i < n; i++) Det += 2*(C[i*n+i]);
			return Det;


	}

	extern "C"
	void Inverse(double* x, double* y, int a){

	  int info;
		int n = 2;
	  //take the Cholesky square root
	  dpotrf("L", &n, x, &n, &info);

		//take the inverse
		dpotri("L", &n, x, &n, &info);
		y = x;


	}

	extern "C"
	void Transpose(double* x, double* y, int n){

		mkl_dimatcopy('R', 'T', n, n, 1 , x, n, n);
		y = x;
	}

	extern "C"
	void MatrixMul2(double* x, double* y, double* z, int m, int n, int k){

		double alpha = 1.0;
		double beta = 0.0;
		int a = 2;

		dgemm("N", "N", &m, &n, &k, &alpha , x, &n, y, &n, &beta, x, &n);
		z = x;

	}

	extern "C"
	void MatrixMul3(double* x, double* y, double* z, double* w, int m, int n, int k){

		double alpha = 1.0;
		double beta = 0.0;

		dgemm("N", "N", &m, &n, &k, &alpha , x, &n, y, &n, &beta, x, &n);
		dgemm("N", "N", &m, &n, &k, &alpha , x, &n, z, &n, &beta, x, &n);
		w = x;
	}


}
#endif
