#ifndef KEY_VALUE_STORAGE
#define KEY_VALUE_STORAGE

#include "StorageObject.h"


class KeyValueStorage : public StorageObject{
	public:
		/*
		KVPairStorageObject is useful for storing pairs like key value. 
		*/
		KeyValueStorage();
		KeyValueStorage(StorageObject key , StorageObject value);

		/*
		Get key
		*/
		StorageObject getKey();


		/*
		Get value
		*/
		StorageObject getValue();
	private:
		StorageObject k;
		StorageObject v;
};

#endif