#ifndef INC_REDUCTION_H
#define INC_REDUCTION_H

#include "IRNode.h"
#include "Util.h"

/* Internal representation of reduction */

namespace Nbody {

/* A single dimension in a reduction */
struct ReductionVariable {
  std::string name;
  Expr begin, end;
};

struct ReductionContents {
  mutable RefCount count;
  std::vector<ReductionVariable> domain;
};

/* Reduction domain is a vector of reduction variables */
class ReductionDomain {
  Ptr<ReductionContents> contents;

public:
  /* Construct a new reduction domain */
  ReductionDomain() : contents(NULL) {}

  /* Construct a reduction domain in the given reduction order,
  where start = innermost and end = outermost loop */
  ReductionDomain(const std::vector<ReductionVariable> &domain) : contents(new ReductionContents) {
    contents.ptr->domain = domain;
  }

  /* Check if two reduction domains are the same */
  bool same_as(const ReductionDomain& other) const {
    return contents.same_as(other.contents);
  }

  /* Immutable access to reduction variables */
  const std::vector<ReductionVariable>& domain() const {
    return contents.ptr->domain;
  }

  /* Test if the handle is not-NULL */
  bool defined() const {
    return contents.defined();
  }
};

template<>
inline RefCount& count<ReductionContents>(const ReductionContents* r) {
  return r->count;
}

/* Free the Reduction memory allocated */
template<>
inline void destroy<ReductionContents>(const ReductionContents* r) {
  delete r;
}

}
#endif
