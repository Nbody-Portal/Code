#ifndef _RANGE_SEARCH_H_
#define _RANGE_SEARCH_H_

#define prune_type unsigned long long

// #include "Binary_tree.h"
#include "range_search_rules.h"
#include "Single_tree_traversal_Pascal.h"
#include "Dual_tree_traversal_Pascal.h"
#include "Multi_tree_traversal_Pascal.h"
#include "Metric.h"
#include "Clock.hpp"

template <typename Tree>
class RangeSearch {
  public:
    int dim;

    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;

    MetricType* metric;
    
    /* level for parallelization of traversal */
    int level;    
    int multi = 0;

    RangeSearch(int d, Tree& st, Tree& tt) : dim(d), level(10),  src_tree(st), trg_tree(tt), metric(new SquaredEuclideanMetricType()) { }
    
    // RangeSearch(int d, int l, Tree& st, Tree& tt) : dim(d), level(l),  src_tree(st), trg_tree(tt), metric(new SquaredEuclideanMetricType()) { }
    
    RangeSearch(int d, int l, Tree& st, Tree& tt, int multi_tree = 0) : dim(d), level(l), src_tree(st), trg_tree(tt), multi(multi_tree),
                                                             metric(new SquaredEuclideanMetricType()) { }

    RangeSearch(int d, Tree& st, Tree& tt, MetricType* m) : dim(d), src_tree(st), trg_tree(tt), metric(m) { }

    /* Search all points in the given range. */
    void search (Range& range, int num_pts, resultNeighbors& neighbors, real_t epsilon) const;
};

// Template instantiation

/**
 * Find the K nearest neighbors of a given point
 *
 * \param p         Points whose range neighbors should be found
 * \param range     Range between which we are searching
 * \param neighbor  Array where the nearest neighbors are stored in sorted order
 * \param epsilon   Distance margin to ignore points for approximate RS calculation
 */

template <typename Tree>
void
RangeSearch<Tree>::search (Range& range, int num_pts, resultNeighbors& neighbors, real_t epsilon) const {
  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  prune_type num_prunes = 0;

  /* Create a vector to store the current range neighbors */
  resultNeighbors current(trg_tree.points());

  Clock timer;
  if (single_mode) {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef RangeSearchRules<Tree, Vec> Rule;
    Rule rules(dim, range, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, metric, neighbors);

    /* Create the traverser */
    SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);

    timer.start();
    for (size_t i = 0; i < trg_tree.points(); ++i) {
      /* Start traversal from the root of the tree */
      traverser.traverse (src_tree.root(), i);
    }
  }
  else {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef RangeSearchRules<Tree, Mat> Rule;
    Rule rules(dim, range, src_tree, trg_tree, src_tree.data_perm, trg_tree.data_perm, metric, current);

    if (multi == 0) {

      /* Create the traverser */
      DualTreeTraversal<Tree, Rule, Rule> traverser(src_tree, trg_tree, rules, rules, num_prunes, level);

      timer.start();
      /* Start traversal from the root of the tree */
      traverser.traverse (src_tree.root(), trg_tree.root());
    }    
    else {
      vector<Tree*> trees;
      trees.push_back(&src_tree);
      trees.push_back(&trg_tree);

      /* Create the traverser */
      MultiTreeTraversal<Tree, Rule, Rule> traverserM(trees, rules, rules, num_prunes, level);

      timer.start();
      /* Start traversal from the root of the trees */
      traverserM.traverse ();
    } 
  }
  double time_rs = timer.seconds();

  /* Push the nearest neighbors to the neighbor vector in the order of increasing distance */
  if (!single_mode) {
    size_t k;
    for (size_t i = 0; i < trg_tree.points(); ++i) {
      k = trg_tree.index[i];
      neighbors[k] = current[i];
    }
  }

  if (single_mode)
    cerr << "Single-tree range search time: " << time_rs << " seconds\n";
   else if (!multi)
    cerr << "Dual-tree range search time: " << time_rs << " seconds\n";
  else
     cerr << "Multi-tree range search time: " << time_rs << " seconds\n";
}

#endif
