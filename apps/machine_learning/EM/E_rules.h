
#ifndef _E_RULES_H_
#define _E_RULES_H_

#define DIM 3
#define GUASSIAN 2

#include <cfloat>
#include <Eigen/Dense>       // for using the determinant and inverse of matrix in Gaussian
#include <Eigen/Eigenvalues> // for eigen values and eigen vectors
#include <math.h>
#include <sys/time.h>
#include <ctime>
#include <vector>

// #include "Binary_tree.h"
// #include "Single_tree_traversal.h"
#include "Metric.h"
#include "Clock.hpp"

class stats {
  public:
    unsigned int dim;
    unsigned int  NumGuassian;
    
    real_t SW[GUASSIAN];
    real_t SWX[GUASSIAN][DIM];
    real_t SWXX[GUASSIAN][DIM][DIM];
    stats(): dim(DIM), NumGuassian(GUASSIAN){}
    stats(unsigned int d, unsigned int N) : dim(d), NumGuassian(N) {}
    
    void add(stats s1, stats s2) {
      size_t k;
      for( k = 0; k < NumGuassian; ++k) {
        SW[k] = s1.SW[k] + s2.SW[k] ;
        for (size_t i = 0 ; i < dim ; ++i) {
          SWX[k][i] = s1.SWX[k][i] + s2.SWX[k][i];
          for (size_t j = 0 ; j < dim ; ++j) {
            SWXX[k][i][j] = s1.SWXX[k][i][j] + s2.SWXX[k][i][j]; 
          }
        }
      }
    }
};

/* Helper structure for tree traversal  */
template <typename Tree>
class E_rules {
  private:
    /* dimension of  input data */
    int Dim; 
    /* Number of clusers for GMM*/
    unsigned int NumGuassian;
    int data_size;
    /* Pointer to the source tree */
    Tree& src_tree;
    /* Points to permutated source array */
    Points_t& src_data;
    /* For  measuring the guassian result */
    real_t Gaussian(real_t* datai, real_t* mui, int i);
    real_t Gaussian(Point datai, real_t* mui, int i);
    //real_t Gaussian(real_t* datai, real_t* mui, real_t** cholesky, real_t determiant);
    /*For measuring  the weight in Moore's paper (or responsibilities in wiki)*/
    real_t Weight(real_t* datai, int t);
    /* EM Information measured during tree traversal */
    real_t** mean;    // NumGuassian x Dim -> matrix of guassian means
    real_t*** sigma;  // NumGussian x Dim x Dim -> matrix of gussian covariance (dxd)
    real_t* pi;       // NumGuassian -> vector of probabilities for each guassian class
    real_t** resp;    // NumData x NumGaussian -> matrix of responsibilities
  
  public:
    /* Varibale for debuging purpose */
    real_t tolerance;
    real_t* weight_sofar;
    real_t num_of_prune = 0;
    real_t size_of_prune = 0;
    real_t power = 1; // measuring this power once and use it for all the computation of Guassian ** Optimization **
    real_t*** Cholesky_all;
    real_t* determinant_all;

    stats statistics;
    typedef typename Tree::NodeTree Box;
    /* Initilization */
    E_rules(int size, int d, unsigned int k, real_t tr, Tree& st, Points_t& s, real_t** mean1, 
       real_t*** sigma1, real_t* pi1, real_t** resp1, real_t*** Cholesky, real_t* determinant)
        :  Dim(d), NumGuassian(k), data_size(size), src_tree(st), src_data(s), mean(mean1), sigma(sigma1), pi(pi1), resp(resp1), tolerance(tr), Cholesky_all(Cholesky), determinant_all(determinant) {
         power = pow((2* M_PI),Dim);
          weight_sofar = new real_t[NumGuassian];
          for (unsigned int i = 0; i < NumGuassian; ++i) {
            weight_sofar[i] = 0;
          }
        }




    /* Base case calculation */
    void base_case (Box& s, int t);

    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);

    /* Calculate which branch to visit first */
    vector<int> visit (Box& s, int t);
    
    void ForwardSub(real_t** L, real_t* Y, real_t* result);
    
    /* computation for the approximation */
    void centroid_case (Box& s, int t);

    void set_temp(int temp) {};
    void adjustPartitionCounter(int , int , int , int){};
    double get_temp(){};


};

/* Template implementation */


/**
 * This function will measure the (L^-1)Y and return the result
*/

template <typename Tree>
void  
E_rules<Tree>::ForwardSub(real_t** L, real_t* Y, real_t* result) {
 result[0] = Y[0]/L[0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = Y[i];
    for (size_t j = 0; j < i; ++j)
      temp -= L[i][j] * result[j];
    result[i] = temp / L[i][i];
  }
}

template <typename Tree>
real_t 
E_rules<Tree>::Gaussian(real_t*  datai, real_t* mui, int f) {

  real_t res[Dim];
  double inner_product = 0;
  res[0] = (datai[0]-mui[0])/Cholesky_all[f][0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = datai[i]-mui[0];
    for (size_t j = 0; j < i; ++j)
      temp -= Cholesky_all[f][i][j] * res[j];
    res[i] = temp / Cholesky_all[f][i][i];
    inner_product += res[i]*res[i];
  }
  // cerr << "f: " << f << ", det: " << determinant_all[f] << endl;
  real_t result = determinant_all[f] * exp(-0.5 * inner_product );
  return result;
}


template <typename Tree>
real_t 
E_rules<Tree>::Gaussian(Point  datai, real_t* mui, int f) {

  real_t res[Dim];
  double inner_product = 0;
  res[0] = (datai[0]-mui[0])/Cholesky_all[f][0][0];
  real_t temp;
  for (size_t i = 1; i < Dim; ++i) {
    temp = datai[i]-mui[0];
    for (size_t j = 0; j < i; ++j)
      temp -= Cholesky_all[f][i][j] * res[j];
    res[i] = temp / Cholesky_all[f][i][i];
    inner_product += res[i]*res[i];
  }

 real_t result = determinant_all[f] * exp(-0.5 * inner_product );

 return result;
}


/*
* Base case calculation
*/
template<typename Tree>
void
E_rules<Tree>::base_case (Box& s, int t) {
  // cerr << "#" << s.index() << "" << endl; 
  stats result(Dim, NumGuassian);
  real_t Sigma[Dim][Dim];
  /* Measruing SW & SWX */
  real_t datai[Dim]; 
  real_t temp[NumGuassian];
  real_t  weight[NumGuassian];
   for(size_t f = 0; f < NumGuassian ; ++f)
        weight[f]=0;
  for (size_t i = s.begin(); i < s.end(); ++i) {
    real_t denominator = 0;
    for (size_t  aa = 0; aa < Dim ; ++aa) {
        datai[aa] = src_data[i][aa];
	for (size_t bb = 0 ; bb < Dim ; ++bb)
		Sigma[aa][bb] = datai[aa] * datai[bb];
    }
    for(size_t f = 0; f < NumGuassian ; ++f) {
      // cerr << "pi" << f << ": " << pi[f] << ", ";
      // cerr << "mean" << f << ": " << mean[f] << ", ";
      // cerr << "Gau" << f << ": " << Gaussian(datai, mean[f], f) << ", ";
      temp[f] = pi[f] * Gaussian(datai, mean[f], f) ;
      denominator += temp[f];
    }
    // cerr << endl;
 
    for(size_t f = 0; f < NumGuassian ; ++f) {
      temp[f] /= denominator; // temp is the same as weight
      result.SW[f] += temp[f];
      weight[f] += temp[f];
      for (int k = 0; k < Dim; ++k) {
        result.SWX[f][k] +=  datai[k] * temp[f];
        for (int j = 0; j < Dim; ++j) {
        result.SWXX[f][k][j] += Sigma[k][j] *  temp[f];
        }
      }
    
    }
  }
     for(size_t f = 0; f < NumGuassian ; ++f)
      {
       #pragma omp atomic
        weight_sofar[f] += weight[f];
      }
    statistics.add(statistics, result);
}

/*
* Centroid case calculation
*/
template<typename Tree>
void
E_rules<Tree>::centroid_case (Box& s, int t) {
  
  // cerr << "#" << s.index() << ": " << src_tree.centers[s.index()] << endl;
  stats result(Dim, NumGuassian);
  real_t denominator = 0;
  real_t temp[NumGuassian];
  real_t Sigma[Dim][Dim];
  for (size_t  aa = 0; aa < Dim ; ++aa){
        for (size_t bb = 0 ; bb < Dim ; ++bb)
                Sigma[aa][bb] = src_tree.centers[s.index()][aa] * src_tree.centers[s.index()][bb];
    }
  for(size_t f=0; f < NumGuassian; ++f) {
    temp[f] = pi[f] * Gaussian(src_tree.centers[s.index()], mean[f], f) ;
    denominator += temp[f];
  }

  for(size_t f = 0; f < NumGuassian ; ++f) {
    temp[f] /= denominator;
    result.SW[f] += (temp[f] * s.size());
    for (int k = 0; k < Dim; ++k) {
      result.SWX[f][k] +=  src_tree.centers[s.index()][k] * temp[f] * s.size();
      for (int j = 0; j < Dim; ++j) {
        result.SWXX[f][k][j] += Sigma[k][j] *  temp[f] * s.size();
      }
    }
    #pragma omp atomic
     weight_sofar[f] += (temp[f] * s.size());
  }
  statistics.add(statistics, result);
}



template<typename Tree>
inline
bool
E_rules<Tree>::prune_subtree (Box& s, int t) {

    real_t weight_diff;
    real_t min_weight;
    real_t max_weight;
    unsigned int counter = 0;
    real_t sum_lo = 0;
    real_t sum_hi = 0;
    real_t sum_center = 0;

    real_t lo_weights[NumGuassian];
    real_t hi_weights[NumGuassian];
    real_t tem_point[Dim];
    for (size_t i = 0; i < NumGuassian; ++i) {

       for (size_t k = 0; k < Dim; ++k) {
          tem_point[k] = s.bounds.lo[k];
	}
      lo_weights[i] = hi_weights[i] = pi[i] * Gaussian(tem_point, mean[i], i);
      for (size_t j = 1 ; j < pow(2, Dim) ; ++j) {
        int decimal_number = j ;
        for (size_t k = 0; k < Dim; ++k) {
          tem_point[k] = (decimal_number %2 ==0) ? s.bounds.lo[k]: s.bounds.hi[k];
          decimal_number /= 2;
        }
        real_t Guassian_temp = pi[i] * Gaussian(tem_point, mean[i], i);
        lo_weights[i] = (Guassian_temp < lo_weights[i]) ? Guassian_temp : lo_weights[i] ;
        hi_weights[i] = (Guassian_temp > hi_weights[i]) ? Guassian_temp : hi_weights[i] ;
      }

      sum_center += pi[i] * Gaussian(src_tree.centers[s.index()], mean[i], i);
      sum_lo += lo_weights[i];
      sum_hi += hi_weights[i];
    }
   
    for (unsigned int i = 0; i < NumGuassian; ++i) {
        min_weight = lo_weights[i]/sum_lo;
        max_weight = hi_weights[i]/sum_hi;

        weight_diff = abs(max_weight - min_weight);
        if ( weight_diff < (tolerance * (weight_sofar[i] + s.size() * min_weight))) {
          ++counter;
        }
    }
    if ((counter == NumGuassian) and (log(sum_hi/sum_lo) < 0.1 * abs(log(sum_center)))) {
      size_of_prune += s.size();
      num_of_prune++;
      return true; 
    }

  return false;
}



/**
 * Check which brach should be explored first.
 */
template<typename Tree>
inline
vector<int>
E_rules<Tree>::visit (Box& s, int t) {
  vector<int> visit_order;
  for (int i = 0; i < s.num_child(); i++)
    visit_order.push_back(i);
  return visit_order;
}

#endif
