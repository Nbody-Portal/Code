Distance function by Daniel Jooryabi

Here is a distance function I found from "A Novel Image Similarity Metric using SIFT-based Characteristics" 
by Tudor Barbu from the Institute of Computer science of the Romanian Academy. 

To run my code just run daniel_dist(imageFile1, imageFile2) and it will return a number indicating similarity. 

Alternatively you can run the batch test function which will give you detailed results. 