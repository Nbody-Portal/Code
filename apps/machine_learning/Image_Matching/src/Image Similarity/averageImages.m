
% This function averages multiple sift vectors into one vector
% simply pass in the images as a cell array
function y = averageImages(images)


n = size(images, 2); 
 
mat = []; 
for i = 1: n
    
    mat(:, :, i) = [images{i}]; 
end

y = mean(mat, 3); 


















end