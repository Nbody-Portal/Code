#ifndef _HD_H_
#define _HD_H_

#define counter_type unsigned long long

#include "HD_rules.h"
#include "Dual_tree_traversal_Pascal.h"
#include "Metric.h"
#include "Clock.hpp"

template <typename Tree>
class HD {
  public:
    int dim;

    Tree& src_tree;
    Tree& trg_tree;

    MetricType* metric;
    real_t MaxMin;

    /*  level for parallelism in tree traversal */
    int level;
    
    int multi;

    int pg_rule = 0 ;

    
    HD(int d, Tree& st, Tree& tt, int multi_tree = 0) : dim(d), src_tree(st), trg_tree(tt), multi(multi_tree), metric(new SquaredEuclideanMetricType()) { }
    
    HD(int d, int l, Tree& st, Tree& tt, int multi_tree = 0) : dim(d), level(l), src_tree(st), trg_tree(tt), multi(multi_tree), metric(new SquaredEuclideanMetricType()) { }
    

    HD(int d, Tree& st, Tree& tt, MetricType* m) : dim(d), src_tree(st), trg_tree(tt), metric(m) { }

    /* Get the Hausdorff distance which is max of min of data points */
    void max_min (unsigned int k, resultMaxMin& neighbors, real_t epsilon) ;
};

// Template instantiation

template<typename Tree>
void
HD<Tree>::max_min (unsigned int k, resultMaxMin& neighbors, real_t epsilon)  {

  /* Check if source-tree or dual-tree search should be used */
  /* for this example  WE ONLY  HAVE DUAL TREE */
  int single_mode = getenv__single_mode();
  counter_type num_prunes = 0;
  double time_HD;
  /* Create a vector to store the current max min, we keep it as a vector in case we wanted to make it for mare than one max min */
  vector<kvector> maxmin_holder(trg_tree.points(), kvector(k));
   
  
  if (single_mode) {
    
    typedef HD_rules<Tree, Vec> Rule;
    Rule rules(dim, k, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, metric, maxmin_holder, num_prunes);
    
    
    /* Create the traverser */
    // SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);
    for (size_t i = 0; i < trg_tree.points(); ++i) {
      num_prunes = 0;
      /* Start traversal from the root of the tree */
      // traverser.traverse (src_tree.root(), i);
    }
  }
  else {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef HD_rules<Tree, Mat> Rule;
    Rule rules(dim, k, src_tree, trg_tree, src_tree.data_perm, trg_tree.data_perm, metric, maxmin_holder, num_prunes);

    
    if (multi == 0) {
      /* Create the traverser */ 
      DualTreeTraversal<Tree, Rule, Rule> traverser(src_tree, trg_tree, rules, rules, num_prunes, level);
      /* Start traversal from the root of the tree */
  
     Clock timer;
     traverser.traverse (src_tree.root(), trg_tree.root());
     time_HD = timer.seconds();
     MaxMin = rules.MaxMin;
    }
    else {
      vector<Tree*> trees;
      trees.push_back(&src_tree);
      trees.push_back(&trg_tree);

      /* Create the traverser */
      // MultiTreeTraversal<Tree, Rule, Rule> traverserM(trees, rules, rules, num_prunes, level);

    }
  }

  if (single_mode)
    cerr << "Single-tree HD time: " << time_HD << " seconds\n";
  else if (!multi)
    cerr << "Dual-tree HD time: " << time_HD << " seconds\n";
  else
     cerr << "Multi-tree HD time: " << time_HD << " seconds\n";

  
}


#endif
