#ifndef _BH_RULES_H_
#define _BH_RULES_H_
#define counter_type unsigned long long

#include "kNN_vector.h"
#include "Metric.h"
#include "range.h"

typedef Metric<Point, Point> MetricType;
typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;


template <typename Tree>
class BarnesHutRules {
  public:
    typedef typename Tree::NodeTree Box;
    int dim;

    real_t threshold;
    Tree& src_tree;
    Tree& trg_tree;

    /* Points to permutated source array */
    Points_t& src_data;
    /* Reference input target array */
    Points_t& trg_data;
    MetricType* metric;


    int num_prunes;
    int num_base;

    std::vector<std::array<real_t,3>> result_forces;

    /* prune generation would be used */
    int pg_rule = 0;


    BarnesHutRules(int d, Tree& st, Tree& tt, Points_t& s, Points_t& t, MetricType* m, counter_type& num_p, real_t e)
        : dim(d), src_tree(st), trg_tree(tt), src_data(s), trg_data(t), metric(m), num_prunes(num_p) , threshold(e) {
          for (int i = 0; i < trg_tree.points(); i++) {
            std::array<real_t,3> a = {0,0,0};
            result_forces.push_back(a);
          }
          num_prunes = 0;
          num_base = 0;
    }


    /* Base case calculation */
    void base_case (Box& s, int t);
    void base_case (Box& s, Box& t);
    void base_case (vector<Box*> t);


    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);
    bool prune_subtree (Box& s, Box& t);
    bool prune_subtree (vector<Box*> t);

    /* Calculate which branch to visit first */
    vector<int> visit (Box& s, int t);
    vector<int> visit (Box& s, Box& t);
    vector<int> visit (vector<Box*> t, int div);

    /* Update distance */
    void update_state (Box& s, Box& t);
    void update_state (vector<Box*> t);

    /* Update the current value of the distance to the nearest point in the hyperrectangle */
    real_t distance_update (Box& s, int t);
    real_t distance_update (Box& s, Box& t);
    real_t distance_update (vector<Box*> t);


    /* centorid case computation for the subtree is will be prunes, in this case since it's prune it does nothing*/
    void centroid_case (Box& s, int t) {};
    void centroid_case (Box& s, Box& t);
    void centroid_case (vector<Box*> t) {};

    void set_temp(int temp) {};
    void adjustPartitionCounter(int , int , int , int){};
    double get_temp(){};
};

// Template implementation

template <typename Tree>
void
BarnesHutRules<Tree>::base_case (Box& s, int t) {
  SquaredEuclideanMetric<Point, Point> metric;
  real_t dist;
  for (size_t j = s.begin(); j < s.end(); ++j) {
      dist = metric.compute_distance (trg_data[t], src_data[j]);
      dist *= sqrt(dist);
      if (dist != 0)
      for(int i = 0; i < 3; i++) {
        result_forces[t][i] += trg_data[t][3] * src_data[j][3] * 6.674e-11 *
                               (trg_data[t][i] - src_data[j][i]) / dist;
      }
  }
}

template <typename Tree>
void
BarnesHutRules<Tree>::base_case (Box& s, Box& t) {
  SquaredEuclideanMetric<Point, Point> metric;
  real_t dist;
  for (size_t i = t.begin(); i < t.end(); ++i) {
    for (size_t j = s.begin(); j < s.end(); ++j) {
        dist = metric.compute_distance (trg_data[i], src_data[j]);
        dist *= sqrt(dist);
        if (i != j)
          for(int k = 0; k < 3; k++) {
            result_forces[i][k] += trg_data[i][3] * src_data[j][3] * 6.674e-11 *
                                   (trg_data[i][k] - src_data[j][k]) / dist;
          }
       }
  }
}

 /* k or _k is set to 1 for Barnes Hut with two dataset and has been used
  * for the multi-tree, in the case of an algorithm with more
  * than two dataset the kernel function and k should have
  * change accordingly
  */


template <typename Tree>
void
BarnesHutRules<Tree>::base_case (vector<Box*> s) {
  int k = 1;

  SquaredEuclideanMetric<Point, Point> metric;
  real_t dist;
  for (size_t i = s[k-1]->begin(); i < s[k-1].end(); ++i) {
    for (size_t j = s[k].begin(); j < s[k].end(); ++j) {
        dist = metric.compute_distance (trg_data[i], src_data[j]);
        dist *= sqrt(dist);
        if (i != j)
          for(int k = 0; k < 3; k++) {
            result_forces[i][k] += trg_data[i][3] * src_data[j][3] * 6.674e-11 *
                                   (trg_data[i][k] - src_data[j][k]) / dist;
          }
       }
  }
}

template <typename Tree>
inline
bool
BarnesHutRules<Tree>::prune_subtree (Box& s, int t) {

  Point3 t_point = trg_tree.center(t);
  Point3 s_center = src_tree.center(s.index());
  real_t distance  = sqrt(t_point.sq_euclidean_distance(s_center));
  real_t D = max(trg_tree.radius(t), src_tree.radius(s.index()));
  return ((D/distance) < threshold);
}

/**
 * Checks for intersection of two boxes for the prune opportunities
 */
template <typename Tree>
inline
bool
BarnesHutRules<Tree>::prune_subtree (Box& s, Box& t) {

  Point3 t_center = trg_tree.center(t.index());
  Point3 s_center = src_tree.center(s.index());
  real_t distance  = sqrt(t_center.sq_euclidean_distance(s_center));
  real_t D = max(trg_tree.radius(t.index()), src_tree.radius(s.index()));
  return ((D/distance) < threshold);

}



template <typename Tree>
inline
bool
BarnesHutRules<Tree>::prune_subtree (vector<Box*> s) {

  int _k = 1;
  int i = s[_k-1]->index();
  int j = s[_k]->index();
  int check = 0;
  Point3 t_center = trg_tree.center(i);
  Point3 s_center = src_tree.center(j);
  real_t distance  = sqrt(t_center.sq_euclidean_distance(s_center));
  real_t D = max(trg_tree.radius(i), src_tree.radius(j));
  return ((D/distance) < threshold);
}

/**
 * Check which brach should be explored first.
 */
template <typename Tree>
inline
vector<int>
BarnesHutRules<Tree>::visit (Box& s, int t) {
  vector<int> visit_order;
  for (int i = 0; i < s.num_child(); i++)
    visit_order.push_back(i);
  return visit_order;
}

template <typename Tree>
inline
vector<int>
BarnesHutRules<Tree>::visit (Box& s, Box& t) {

  vector<int> visit_order;
  for (int i = 0; i < s.num_child(); i++)
    visit_order.push_back(i);
  return visit_order;
}

template <typename Tree>
inline
vector<int>
BarnesHutRules<Tree>::visit (vector<Box*> s, int div) {

  vector<int> visit_order;
  for (int i = 0; i < s[div]->num_child(); i++)
    visit_order.push_back(i);
  return visit_order;
}

/**
 * Update distance information.
 */
template <typename Tree>
inline
void
BarnesHutRules<Tree>::update_state (Box& s, Box& t) {
  for (int i = 0; i < t.num_child(); i++)
    distance_update(s, trg_tree.node_data[t.child+i]);
}

template <typename Tree>
inline
void
BarnesHutRules<Tree>::update_state (vector<Box*> s) {
  int k = 1;
  distance_update(s[k-1], trg_tree.node_data[s[k]->child  ]);
  distance_update(s[k-1], trg_tree.node_data[s[k]->child+1]);
}

/**
 * Intersection data is updated.
 */
template <typename Tree>
real_t
BarnesHutRules<Tree>::distance_update (Box& s, int t) {
  int i = s.index();
  return  s.min_distance(trg_data[t]);
}

/**
 * Intersection data is computed.
 */
template <typename Tree>
real_t
BarnesHutRules<Tree>::distance_update (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  return  t.min_distance(s);
}
template <typename Tree>
real_t
BarnesHutRules<Tree>::distance_update (vector<Box*> s) {
  int k = 1;
  int i = s[k-1]->index();
  int j = s[k]->index();
  return  s[k]->min_distance(*s[k-1]);
}

template <typename Tree>
void
BarnesHutRules<Tree>::centroid_case (Box& s, Box& t) {

  Point3 t_center = trg_tree.center(t.index());
  Point3 s_center = src_tree.center(s.index());
  double distance  = sqrt(t_center.sq_euclidean_distance(s_center));
  Point3 force_total;
  for(int k = 0; k < 3; k++) {
    force_total[k] = t_center.get_mass() * t_center.get_mass() * 6.674e-11 *
                     (t_center[k] - s_center[k]) / distance;
  }
  for (size_t i = t.begin(); i < t.end(); ++i) {
      for(int k = 0; k < 3; k++) {
        result_forces[i][k] += force_total[k];

      }
  }
}


#endif
