#ifndef _BH_H_
#define _BH_H_

#define counter_type unsigned long long

#include "bh_rules.h"
#include "Single_tree_traversal_Pascal.h"
#include "Dual_tree_traversal_Pascal.h"
#include "Multi_tree_traversal_Pascal.h"
#include "Metric.h"
#include "Clock.hpp"

template <typename Tree>
class BarnesHut {
  public:
    int dim;

    Tree& src_tree;
    Tree& trg_tree;

    MetricType* metric;

    /*  level for parallelism in tree traversal */
    int level;
    std::vector<std::array<real_t,3>> result_forces;

    int multi;

    int pg_rule = 0 ;


    BarnesHut(Tree& st, Tree& tt, int multi_tree = 0) : dim(3), src_tree(st), trg_tree(tt), multi(multi_tree), metric(new SquaredEuclideanMetricType()) { }

    BarnesHut(int l, Tree& st, Tree& tt, int multi_tree = 0) : dim(3), level(l), src_tree(st), trg_tree(tt), multi(multi_tree), metric(new SquaredEuclideanMetricType()) { }


    BarnesHut(Tree& st, Tree& tt, MetricType* m) : dim(3), src_tree(st), trg_tree(tt), metric(m) { }

    /* calculates the gravitational force for all  data points */
    void force_calculate (real_t epsilon);
};

// Template instantiation

template<typename Tree>
void
BarnesHut<Tree>::force_calculate (real_t epsilon)  {

  /* Check if source-tree or dual-tree search should be used */
  int single_mode = getenv__single_mode();
  counter_type num_prunes = 0;
  double time_bh;

  if (single_mode) {

    typedef BarnesHutRules<Tree> Rule;
    Rule rules(dim, src_tree, trg_tree, src_tree.data_perm, trg_tree.data, metric, num_prunes, epsilon);


    /* Create the traverser */
    SingleTreeTraversal<Tree, Rule> traverser(src_tree, rules, num_prunes);
    for (size_t i = 0; i < trg_tree.points(); ++i) {
      num_prunes = 0;
      /* Start traversal from the root of the tree */
      traverser.traverse (src_tree.root(), i);
    }
  }
  else {
    /* Create a helper object for tree traversal and intersection calculation */
    typedef BarnesHutRules<Tree> Rule;
    Rule rules(dim,src_tree, trg_tree, src_tree.data_perm, trg_tree.data_perm, metric, num_prunes, epsilon);


    if (multi == 0) {
      /* Create the traverser */
      DualTreeTraversal<Tree, Rule, Rule> traverser(src_tree, trg_tree, rules, rules, num_prunes, level);
      /* Start traversal from the root of the tree */

     Clock timer;
     traverser.traverse (src_tree.root(), trg_tree.root());
     time_bh = timer.seconds();
     for (int i = 0; i < trg_tree.points(); i++) {
       result_forces.push_back(rules.result_forces[i]);
     }

    }
    else {
      vector<Tree*> trees;
      trees.push_back(&src_tree);
      trees.push_back(&trg_tree);

      /* Create the traverser */
      MultiTreeTraversal<Tree, Rule, Rule> traverserM(trees, rules, rules, num_prunes, level);

    }
  }

  if (single_mode)
    cerr << "Single-tree  Barnes Hut time: " << time_bh << " seconds\n";
  else if (!multi)
    cerr << "Dual-tree Barnes Hut time: " << time_bh << " seconds\n";
  else
     cerr << "Multi-tree Barnes Hut time: " << time_bh << " seconds\n";


}


#endif
