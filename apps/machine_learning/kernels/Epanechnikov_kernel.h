#ifndef _EPANECHNIKOV_KERNEL_H_
#define _EPANECHNIKOV_KERNEL_H_

#include <boost/math/special_functions/gamma.hpp>
#include "Metric.h"
#include "Kernel.h"

typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;
typedef Metric<Point, Point> MetricType;
/**
 * The Epanechnikov kernel is computed as
 * \f[
 * K(x, y) = \max{0, 1 - || x - y ||^2 / \mu^2}
 * \f]
 *
 * where \f$\mu\f$ is the kernel bandwidth (defaults to 1.0).
 */
class EpanechnikovKernel : public Kernel
{
  /* Kernel bandwidth */
  real_t bandwidth;

  /* Pre-computed helper value: \f$ \tau = \frac{1}{\mu^2} \f$ */
  real_t tau;

  MetricType* metric;

  public:
  /* Construct gaussian kernel */
  EpanechnikovKernel (real_t width = 1.0) : bandwidth(width), tau(pow(bandwidth, -2.0)), metric(new SquaredEuclideanMetricType()) { }

  /* Evaluates the kernel */
  real_t compute (const Point& a, const Point&b) const {
    return std::max(0.0, 1.0 - metric->compute_distance(a, b) * tau);
  }

  /* Evaluates the kernel given the squared distance between two points */
  real_t compute (const real_t dist) const {
    return std::max(0.0, 1.0 - dist * tau);
  }

  /* Evaluates the log of the kernel */
  real_t log_compute (const Point&a, const Point&b) const {
    real_t dist = metric->compute_distance(a, b);
    if (dist < bandwidth)
      return log(1.0 - dist * tau);
    else
      return -INFINITY;
  }

  /* Evaluates the log of the kernel given the squared distance between two points */
  real_t log_compute (const real_t dist) const {
    if (dist < bandwidth)
      return log(1.0 - dist * tau);
    else
      return -INFINITY;
  }

  /* Returns the normalization constant of the Epanechnikov kernel
   * given by (2+d)/2*Vd where Vd is the volume of a d-dimensiional
   * hypersphere.
   */
  real_t norm (size_t dim) {
    return (boost::math::tgamma(dim / 2.0 + 1.0) * (dim + 2.0)) /
      (2.0 * pow(bandwidth, dim) * std::pow(M_PI, dim / 2.0));
  }
};

#endif
