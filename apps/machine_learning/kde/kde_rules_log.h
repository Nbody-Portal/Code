#ifndef _KDE_RULES_LOG_H_
#define _KDE_RULES_LOG_H_

#include <cfloat>
#include <algorithm>
#include "Metric.h"

typedef Metric<Point, Point> MetricType;
typedef SquaredEuclideanMetric<Point, Point> SquaredEuclideanMetricType;

/* Helper structure for tree traversal and incremental hyperrectangle-hypersphere intersection data */
template <typename Tree, typename S, typename KernelType>
class KDERules {
  public:
    typedef typename Tree::NodeTree Box;
    int dim;

    real_t log_atol;
    real_t log_rtol;

    /* Kernel */
    KernelType& kernel;

    /* Pointer to the source tree */
    Tree& src_tree;
    /* Pointer to the target tree */
    Tree& trg_tree;

    /* Points to permutated source array */
    Points& src_data;
    /* Reference input target array */
    Points& trg_data;

    Vec& log_density;

    MetricType* metric;

    S min_bound;
    S max_bound;
    S spread;

    real_t global_bound;
    real_t global_spread;
    real_t kernel_norm;

    KDERules(int d, real_t atol, real_t rtol, KernelType& k, Tree& st, Tree& tt, Points& s, Points& t, Vec& density, MetricType* m) : dim(d), log_atol(atol), log_rtol(rtol), kernel(k), src_tree(st), trg_tree(tt), src_data(s), trg_data(t), log_density(density), metric(m), min_bound(), max_bound(), spread(), global_bound(0.0), global_spread(0.0), kernel_norm(kernel.log_norm(dim)) {}

    /* Base case calculation */
    void base_case (Box& s, int t);
    void base_case (Box& s, Box& t);

    /* Check if subtree should be pruned */
    bool prune_subtree (Box& s, int t);
    bool prune_subtree (Box& s, Box& t);

    /* Calculate which branch to visit first */
    bool visit (Box& s, int t);
    vector<int> visit (Box& s, Box& t);

    /* Update distance */
    void update_state (Box& s, Box& t);

    /* Update the current value of the distance to the nearest point in the hyperrectangle */
    void bound_update (Box& s, int t);
    void bound_update (Box& s, Box& t);

    /* centorid case computation for the subtree is will be prunes, in this case since it's prune it does nothing*/
    void centroid_case (Box& s, int t) {};

    /* Returns log of sum of exponent of two floating point numbers */
    real_t logsumexp(real_t a, real_t b) {
      real_t x = fmax(a, b);
      return (x == -INFINITY) ? x : x+log(exp(a-x)+exp(b-x));
    }

    /* Returns the log of difference of exponent of two numbers */
    real_t logdiffexp(real_t a, real_t b) {
      return (a <= b) ? -INFINITY : a+log(1-exp(b-a));
    }
};

// Template implementation
template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::base_case (Box& s, int t) {
  real_t kval;
  global_bound = logdiffexp(global_bound, min_bound[s.index()]);
  global_spread = logdiffexp(global_spread, spread[s.index()]);

  for (size_t j = s.begin(); j < s.end(); ++j) {
    kval = kernel.log_compute (trg_data[t], src_data[j]);
    global_bound = logsumexp(kval, global_bound);
  }
}

template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::base_case (Box& s, Box& t) {
  real_t kval;
  global_bound = logdiffexp(global_bound, min_bound[s.index()][t.index()]);
  global_spread = logdiffexp(global_spread, spread[s.index()][t.index()]);

  for (size_t i = t.begin(); i < t.end(); ++i) {
    real_t q = -INFINITY;
    for (size_t j = s.begin(); j < s.end(); ++j) {
      kval = kernel.log_compute (trg_data[i], src_data[j]);
      q = logsumexp(q, kval);
    }
    global_bound = logsumexp(global_bound, q);
    log_density[i] = logsumexp(log_density[i], q);
  }
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S, typename Kernel>
inline
bool
KDERules<Tree,S,Kernel>::prune_subtree (Box& s, int t) {
  int i = s.index();
  int num_pts = src_tree.points();
  /* If local bound criterion is met, prune */
  if ((kernel_norm + spread[i] + log(num_pts) - log(s.size())) <= logsumexp(log_atol, log_rtol + kernel_norm + min_bound[i]))
    return true;

  /* If global criterion is met, prune */
  if((kernel_norm + global_spread) <= logsumexp(log_atol, log_rtol + kernel_norm + global_bound))
    return true;
  return false;
}

/**
 * Check if the hypersphere from the current farthest neighbor intersects the hyperrectange.
 */
template<typename Tree, typename S, typename Kernel>
inline
bool
KDERules<Tree,S,Kernel>::prune_subtree (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  int num_pts = src_tree.points()*trg_tree.points();
  /* Compute local bound criterion */
  bool local_criterion = (kernel_norm + spread[i][j] + log(num_pts) - log(s.size()*t.size())) <= logsumexp(log_atol, log_rtol + kernel_norm + min_bound[i][j]);

  /* Compute global bound criterion */
  bool global_criterion = (kernel_norm + global_spread) <= logsumexp(log_atol, log_rtol + kernel_norm + global_bound);
  
  /* If local or global criterion is met, increase log density of all target/query points by K(mean + spread/2) */
  if (global_criterion || local_criterion) {
    real_t center_density = logsumexp(min_bound[i][j], spread[i][j]-log(2))-log(t.size());
    for (size_t k = t.begin(); k < t.end(); ++k)
      log_density[k] = logsumexp(log_density[k], center_density);
    return true;
  }
  return false;
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S, typename Kernel>
inline
bool
KDERules<Tree,S,Kernel>::visit (Box& s, int t) {
  vector<Box>& stree = src_tree.node_data;
  bound_update(stree[s.child], t);
  bound_update(stree[s.child+1], t);

  /* Update global bounds */
  global_bound = logdiffexp(global_bound, min_bound[s.index()]);
  global_bound = logsumexp(global_bound, min_bound[s.child]);
  global_bound = logsumexp(global_bound, min_bound[s.child+1]);

  global_spread = logdiffexp(global_spread, spread[s.index()]);
  global_spread = logsumexp(global_spread, spread[s.child]);
  global_spread = logsumexp(global_spread, spread[s.child+1]);

  /* Traversal order doesn't matter, return left */
  return 0;
}

/**
 * Check which brach should be explored first.
 */
template<typename Tree, typename S, typename Kernel>
vector<int>
KDERules<Tree,S,Kernel>::visit (Box& s, Box& t) {
  vector<int> visit_order;
  int j = t.index();
  vector<Box>& stree = src_tree.node_data;
  for (int i = 0; i < s.num_child(); i++) {
    visit_order.push_back(i);
    bound_update(stree[s.child+i], t);
    global_bound = logsumexp(global_bound, min_bound[s.child+i][j]);
    global_spread = logsumexp(global_spread, spread[s.child+i][j]);
  }

  /* Update global bounds */
  global_bound = logdiffexp(global_bound, min_bound[s.index()][j]);
  // global_bound = logsumexp(global_bound, min_bound[s.child][j]);
  // global_bound = logsumexp(global_bound, min_bound[s.child+1][j]);

  global_spread = logdiffexp(global_spread, spread[s.index()][j]);
  // global_spread = logsumexp(global_spread, spread[s.child][j]);
  // global_spread = logsumexp(global_spread, spread[s.child+1][j]);

  /* Traversal order doesn't matter, return left */
  return visit_order;
}

/**
 * Update distance information.
 */
template<typename Tree, typename S, typename Kernel>
inline
void
KDERules<Tree,S,Kernel>::update_state (Box& s, Box& t) {
  int i = s.index();
  vector<Box>& ttree = trg_tree.node_data;
  for (int j = 0; j < t.num_child(); i++) {
    bound_update(s, ttree[t.child+j]);
    global_bound = logsumexp(global_bound, min_bound[i][t.child+j]);
    global_spread = logsumexp(global_spread, spread[i][t.child+j]);
  }
  // bound_update(s, ttree[t.child+1]);

  /* Update global bounds */
  global_bound = logdiffexp(global_bound, min_bound[i][t.index()]);
  // global_bound = logsumexp(global_bound, min_bound[i][t.child]);
  // global_bound = logsumexp(global_bound, min_bound[i][t.child+1]);

  global_spread = logdiffexp(global_spread, spread[i][t.index()]);
  // global_spread = logsumexp(global_spread, spread[i][t.child]);
  // global_spread = logsumexp(global_spread, spread[i][t.child+1]);
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::bound_update (Box& s, int t) {
  int i = s.index();
  Range dist = s.range_distance(trg_data[t]);
  min_bound[i] = log(s.size()) + kernel.log_compute(dist.max());
  max_bound[i] = log(s.size()) + kernel.log_compute(dist.min());
  spread[i] = logdiffexp(max_bound[i], min_bound[i]);
}

/**
 * Intersection data is computed.
 */
template<typename Tree, typename S, typename Kernel>
void
KDERules<Tree,S,Kernel>::bound_update (Box& s, Box& t) {
  int i = s.index();
  int j = t.index();
  Range dist = t.range_distance(s);
  min_bound[i][j] = log(t.size()) + log(s.size()) + kernel.log_compute(dist.max());
  max_bound[i][j] = log(t.size()) + log(s.size()) + kernel.log_compute(dist.min());
  spread[i][j] = logdiffexp(max_bound[i][j], min_bound[i][j]);
}

#endif
