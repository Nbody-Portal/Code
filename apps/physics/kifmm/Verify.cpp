#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "Octree.h"
#include "Fmm.h"
#include "Utils.h"

template<typename Kernel, typename KMatrix>
int
FMM<Kernel,KMatrix>::validate (int num_pts, real_t &rerr)
{
  unsigned int dim = 3;
  int num_chk;
  num_chk = getenv__validate();
  int tdof = knl.trg_dof();

  Points& sorig = stree.data;
  Points& torig = ttree.data;

  Points chkpos(num_chk);
  Vec chkval(num_chk * tdof);
  Vec chkpot(num_chk * tdof);

  /* Randomly select points to check */
  int chk_id [num_chk];
  for (int k = 0; k < num_chk; k++) {
	  chk_id[k] = int( floor(drand48()*num_pts) );
    assert (chk_id[k]>=0 && chk_id[k]<num_pts);
  }

  for (int k = 0; k < num_chk; k++) {
    for (int d = 0; d < dim; ++d)
      chkpos[k][d] = torig[chk_id[k]][d];
	  for (int i = 0; i < tdof; i++)
      chkval[k*tdof+i] = result[chk_id[k]*tdof+i];
  }

  /* Compute direct N^2 on selected points */
  for (int i = 0; i < num_chk; i++) {
    auto chk_pos = boost::make_iterator_range(chkpos.begin()+i, chkpos.begin()+i+1);
    auto chk_pot = chkpot.begin()+i*tdof;
    points_iter spos = boost::make_iterator_range(sorig.begin(), sorig.end());
	  knl.kernel_evaluation(chk_pos, spos, charge.begin(), chk_pot);
  }

  /* Compute relative error */
  for (int k = 0; k < num_chk; k++)
	  for (int i = 0; i < tdof; i++) {
      chkpot[k*tdof+i] -= chkval[k*tdof+i];
    }

  real_t vn = 0;
  real_t en = 0;
  for (int k = 0; k < num_chk; k++)
	  for (int i = 0; i < tdof; i++) {
		  vn += chkval[k*tdof+i] * chkval[k*tdof+i];
		  en += chkpot[k*tdof+i] * chkpot[k*tdof+i];
	  }
  vn = sqrt(vn);
  en = sqrt(en);

  rerr = en/vn;

  return 0;
}

