#ifndef _VECTORMATRIXOP_H_
#define _VECTORMATRIXOP_H_

//#include "reals_aligned.h"

/* y = A x + y */
int daxpy(int n, real_t a, real_t* X, real_t* Y);
int daxpy(real_t a, Points& X, Points& Y);

/* y = alpha A x + beta y */
int dgemv(int m, int n, real_t alpha, real_t* A, real_t* X, real_t beta, real_t* Y);

/* R = tran(M) */
int tran(const real_t* M, real_t* R, int m, int n);

/* R = pinv(M, epsilon) */
int pinv(real_t* M, real_t epsilon, real_t* R, int m, int n);

#include "VectorMatrixOp.cpp"
#endif
