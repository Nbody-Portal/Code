#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <iostream>
#include <vector>

#include "Kernel.h"
#include "reals_aligned.h"

using namespace std;

/* ------------------------------------------------------------------------
*/
bool 
Kernel::homogeneous () 
{
  bool hom = false;
  switch(kernel_type) {
    case KERNEL_LAPLACE:
                        hom = true; break;
    case KERNEL_STOKES:
                        hom = true; break;
    case KERNEL_NAVIER: 
                        hom = false; break;
    default: assert(false && "Error: Unknown kernel");
  }
  return hom;
} 
  
void 
Kernel::homogeneous_degree (real_t* degvec) const
{
  switch(kernel_type) {
    case KERNEL_LAPLACE:
                        degvec[0] = 1; break;
    case KERNEL_STOKES:
                        degvec[0] = 1; degvec[1] = 1; degvec[2] = 1; degvec[3] = 2; break;
    case KERNEL_NAVIER: 
                        degvec[0] = 1; degvec[1] = 1; degvec[2] = 1; break; 
    default: assert(false && "Error: Unknown kernel");
  }
} 
  
int
Kernel::src_dof () 
{
  int dof = 0;
  switch(kernel_type) {
    case KERNEL_LAPLACE:
                        dof = 1; break;
    case KERNEL_STOKES:
                        dof = 3; break;
    case KERNEL_NAVIER: 
                        dof = 3; break;
    default: assert(false && "Error: Unknown kernel");
  }
  return dof;
} 

int
Kernel::trg_dof () 
{
  int dof = 0;
  switch(kernel_type) {
    case KERNEL_LAPLACE:
                        dof = 1; break;
    case KERNEL_STOKES:
                        dof = 3; break;
    case KERNEL_NAVIER: 
                        dof = 3; break;
    default: assert(false && "Error: Unknown kernel");
  }
  return dof;
} 

/** Naive kernel matrix functions */

  void 
Kernel::kernel_matrix (Pos trg, Pos src, real_t* mat) 
{
  switch(kernel_type) {
    case KERNEL_LAPLACE:
        kernel_matrix_laplace (trg, src, mat); break;
/*    case KERNEL_STOKES:
        kernel_matrix_stokes (trg, src, mat); break;
    case KERNEL_NAVIER: 
        kernel_matrix_navier (trg, src, mat); break;
*/    default: assert(false && "Error: Unknown kernel");
  }
} 

/** Naive Single-Layer Laplacian velocity kernel */

  int 
Kernel::kernel_matrix_laplace (Pos trg, Pos src, real_t* mat)
{
  int i, j;
  real_t x, y, z, r2, r;
  for (i = 0; i < src.n; i++) {
    real_t sx = src.x[i];
    real_t sy = src.y[i];
    real_t sz = src.z[i];
    for (j = 0; j < trg.n; j++) {
      x = sx - trg.x[j];
      y = sy - trg.y[j];
      z = sz - trg.z[j];
      r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      r = sqrt(r2);
#else 
      r = sqrtf(r2);
#endif
      if (r != 0) 
        mat[i*trg.n+j]  = OOFP_R / r;
    }
  }
  return 0;
}

/** Newton-Raphson fused kernel functions */

void 
Kernel::kernel_evaluation (Node<real_t> trg, Node<real_t> src) 
{
  switch(kernel_type) {
    case KERNEL_LAPLACE:
        kernel_evaluation_laplace (trg, src); break;
    case KERNEL_STOKES:
        kernel_evaluation_stokes (trg, src); break;
    case KERNEL_NAVIER: 
        kernel_evaluation_navier (trg, src); break;
    default: assert(false && "Error: Unknown kernel");
  }
} 
/* ------------------------------------------------------------------------
*/

/** NRSQ fused Single-Layer Laplacian velocity kernel */

int 
Kernel::kernel_evaluation_laplace (Node<real_t> trg, Node<real_t> src) 
{
  int i;
  SIMD_REG OOFP_R__ = SIMD_SET1 (OOFP_R);  
  for (i = 0; (i+SIMD_LEN) <= trg.num_elem(); i += SIMD_LEN) {
    SIMD_REG TX__ = SIMD_LOAD_U (trg.x + i);
    SIMD_REG TY__ = SIMD_LOAD_U (trg.y + i);
    SIMD_REG TZ__ = SIMD_LOAD_U (trg.z + i);
    SIMD_REG TD__ = SIMD_ZERO;
    int j;
    for (j = 0; j < src.num_elem(); j++) {
      SIMD_REG SD1__ = SIMD_LOAD1 (src.q + j);
      SIMD_REG DX1__ = SIMD_SUB (TX__, SIMD_LOAD1 (src.x + j));
      SIMD_REG DY1__ = SIMD_SUB (TY__, SIMD_LOAD1 (src.y + j));
      SIMD_REG DZ1__ = SIMD_SUB (TZ__, SIMD_LOAD1 (src.z + j));
      SIMD_REG R__ = SIMD_MUL (DX1__, DX1__);
      R__ = SIMD_ADD (R__, SIMD_MUL (DY1__, DY1__));
      R__ = SIMD_ADD (R__, SIMD_MUL (DZ1__, DZ1__));
      SIMD_REG TEMP__ = SIMD_CMPEQ (R__, SIMD_ZERO);
#if !defined (USE_FLOAT)
      SIMD_REG XHALF__ = SIMD_MUL (SIMD_SET1 (0.5), R__); 
      R__ = SIMD_INV_SQRT_S (R__);
      SIMD_REG RNR__ = SIMD_MUL (R__, R__);
      RNR__ = SIMD_MUL (RNR__, XHALF__);
      RNR__ = SIMD_SUB (SIMD_SET1 (1.5), RNR__);
      R__ = SIMD_MUL (RNR__, R__);
      //      R__ = SIMD_INV_SQRT (R__);
#else 
      R__ = SIMD_INV_SQRT (R__);
#endif
      //      R__ = SIMD_ANDNOT (TEMP__, R__);
      TD__ = SIMD_ADD (TD__, SIMD_MUL (SD1__, R__));
    }
    TD__ = SIMD_MUL (TD__, OOFP_R__);
    TD__ = SIMD_ADD (TD__, SIMD_LOAD_U (trg.q + i));
    SIMD_STORE_U (trg.q + i, TD__);
  }
  while (i < trg.num_elem()) {
    real_t tx = trg.x[i];
    real_t ty = trg.y[i];
    real_t tz = trg.z[i];
    real_t td = 0.0;
    int j;
    for (j = 0; j < src.num_elem(); j++) {
      real_t x = tx - src.x[j];
      real_t y = ty - src.y[j];
      real_t z = tz - src.z[j];
      real_t r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      real_t r = sqrt(r2);
#else 
      real_t r = sqrtf(r2);
#endif
      //      if (r != 0)
      td += src.q[j] / r;
    }
    trg.q[i]  += OOFP_R * td;
    ++i;
  }
  return 0;
}

/* ------------------------------------------------------------------------
*/

/** NRSQ fused Single-layer Stokes velocity kernel */

int 
Kernel::kernel_evaluation_stokes (Node<real_t> trg, Node<real_t> src) 
{
  int i;
  vector<double> coefs(2); 
  coefs[0] = 1.0;
  coefs[1] = 0.25; //coefs in the kernel, work for all examples
  real_t mu = coefs[0];
  real_t oomu = 1.0/mu;
  real_t cons = oomu * OOEP_R;
  real_t x, y, z, r2, r, invdr;
  real_t dot, denx, deny, denz;
  real_t temp1[SIMD_LEN], temp2[SIMD_LEN], temp3[SIMD_LEN];
  
  SIMD_REG OOEP_R__ = SIMD_SET1 (OOEP_R);  
  SIMD_REG OOMU__ = SIMD_SET1 (oomu);
  SIMD_REG CONS__ = SIMD_MUL (OOEP_R__, OOMU__);
  for (i = 0; (i+SIMD_LEN) <= trg.num_pts; i += SIMD_LEN) {
    SIMD_REG TX__ = SIMD_LOAD_U (trg.x + i);
    SIMD_REG TY__ = SIMD_LOAD_U (trg.y + i);
    SIMD_REG TZ__ = SIMD_LOAD_U (trg.z + i);
    SIMD_REG TD1__ = SIMD_ZERO;
    SIMD_REG TD2__ = SIMD_ZERO;
    SIMD_REG TD3__ = SIMD_ZERO;
    int j;
    for (j = 0; j < src.num_pts; j++) {
      SIMD_REG SD1__ = SIMD_LOAD1 (&src.q[j*3]);
      SIMD_REG SD2__ = SIMD_LOAD1 (&src.q[j*3+1]);
      SIMD_REG SD3__ = SIMD_LOAD1 (&src.q[j*3+2]);
      SIMD_REG DX1__ = SIMD_SUB (TX__, SIMD_LOAD1 (src.x + j));
      SIMD_REG DY1__ = SIMD_SUB (TY__, SIMD_LOAD1 (src.y + j));
      SIMD_REG DZ1__ = SIMD_SUB (TZ__, SIMD_LOAD1 (src.z + j));
      SIMD_REG R__ = SIMD_MUL (DX1__, DX1__);
      R__ = SIMD_ADD (R__, SIMD_MUL (DY1__, DY1__));
      R__ = SIMD_ADD (R__, SIMD_MUL (DZ1__, DZ1__));
      SIMD_REG TEMP__ = SIMD_CMPEQ (R__, SIMD_ZERO);
#if !defined (USE_FLOAT)
      SIMD_REG XHALF__ = SIMD_MUL (SIMD_SET1 (0.5), R__); 
      R__ = SIMD_INV_SQRT_S (R__);
      SIMD_REG RNR__ = SIMD_MUL (R__, R__);
      RNR__ = SIMD_MUL (RNR__, XHALF__);
      RNR__ = SIMD_SUB (SIMD_SET1 (1.5), RNR__);
      R__ = SIMD_MUL (RNR__, R__);
      //      R__ = SIMD_INV_SQRT (R__);
#else 
      R__ = SIMD_INV_SQRT (R__);
#endif
      //      R__ = SIMD_ANDNOT (TEMP__, R__);
      SIMD_REG DOTX = SIMD_MUL (DX1__, SD1__);
      SIMD_REG DOTY = SIMD_MUL (DY1__, SD2__);
      SIMD_REG DOTZ = SIMD_MUL (DZ1__, SD3__);
      SIMD_REG DSUM = SIMD_ADD (DOTX, DOTY);
      DSUM = SIMD_ADD (DSUM, DOTZ);
      DSUM = SIMD_MUL (DSUM, R__);
      DSUM = SIMD_MUL (DSUM, R__);

      DOTX = SIMD_MUL (DSUM, DX1__); 
      DOTY = SIMD_MUL (DSUM, DY1__); 
      DOTZ = SIMD_MUL (DSUM, DZ1__);

      SD1__ = SIMD_ADD (SD1__, DOTX); 
      SD2__ = SIMD_ADD (SD2__, DOTY); 
      SD3__ = SIMD_ADD (SD3__, DOTZ);

      TD1__ = SIMD_ADD (TD1__, SIMD_MUL (SD1__, R__));
      TD2__ = SIMD_ADD (TD2__, SIMD_MUL (SD2__, R__));
      TD3__ = SIMD_ADD (TD3__, SIMD_MUL (SD3__, R__));
    }
    TD1__ = SIMD_MUL (TD1__, CONS__);
    TD2__ = SIMD_MUL (TD2__, CONS__);
    TD3__ = SIMD_MUL (TD3__, CONS__);
    
    SIMD_STORE_U (temp1, TD1__);
    SIMD_STORE_U (temp2, TD2__);
    SIMD_STORE_U (temp3, TD3__);
    
    for (int k = 0; k < SIMD_LEN; k++) {
      trg.q[(i+k)*trg_dof()] += temp1[k];
      trg.q[(i+k)*trg_dof()+1] += temp2[k];
      trg.q[(i+k)*trg_dof()+2] += temp3[k];
    }
  } 
  while (i < trg.num_pts) {
    real_t tx = trg.x[i];
    real_t ty = trg.y[i];
    real_t tz = trg.z[i];
    real_t td = 0.0;
    int j;
    for (j = 0; j < src.num_pts; j++) {
      x = tx - src.x[j];
      y = ty - src.y[j];
      z = tz - src.z[j];
      r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      r = sqrt(r2);
#else
      r = sqrtf(r2);
#endif
      if (r == 0)
        invdr = 0;
      else
        invdr = 1.0 / r;
      dot = (x*src.q[j*3] + y*src.q[j*3+1] + z*src.q[j*3+2]) * invdr * invdr;
      denx = src.q[j*3] + dot*x;
      deny = src.q[j*3+1] + dot*y;
      denz = src.q[j*3+2] + dot*z;

      trg.q[i*3] += denx*invdr*cons;
      trg.q[i*3+1] += deny*invdr*cons;
      trg.q[i*3+2] += denz*invdr*cons;
    }
    ++i;
  }

  return 0;
}

/* ------------------------------------------------------------------------
*/

/** NRSQ fused Single-layer Navier velocity kernel */

int 
Kernel::kernel_evaluation_navier (Node<real_t> trg, Node<real_t> src) 
{
  int i;
  vector<double> coefs(2); 
  coefs[0] = 1.0;
  coefs[1] = 0.25; //coefs in the kernel, work for all examples
  real_t mu = coefs[0];
  real_t ve = coefs[1];
  real_t oomu = 1.0/mu;
  real_t sc1 = (3.0 - 4.0*ve)/(16.0*M_PI*(1.0-ve));
  real_t sc2 = 1.0/(16.0*M_PI*(1.0-ve));
  real_t x, y, z, r2, r, invdr;
  real_t dot, denx, deny, denz;
  real_t temp1[SIMD_LEN], temp2[SIMD_LEN], temp3[SIMD_LEN];
  
  SIMD_REG OOMU__ = SIMD_SET1 (oomu);
  SIMD_REG SC1__ = SIMD_SET1 (sc1);
  SIMD_REG SC2__ = SIMD_SET1 (sc2);
  for (i = 0; (i+SIMD_LEN) <= trg.num_pts; i += SIMD_LEN) {
    SIMD_REG TX__ = SIMD_LOAD_U (trg.x + i);
    SIMD_REG TY__ = SIMD_LOAD_U (trg.y + i);
    SIMD_REG TZ__ = SIMD_LOAD_U (trg.z + i);
    SIMD_REG TD1__ = SIMD_ZERO;
    SIMD_REG TD2__ = SIMD_ZERO;
    SIMD_REG TD3__ = SIMD_ZERO;
    int j;
    for (j = 0; j < src.num_pts; j++) {
      SIMD_REG SD1__ = SIMD_LOAD1 (&src.q[j*3]);
      SIMD_REG SD2__ = SIMD_LOAD1 (&src.q[j*3+1]);
      SIMD_REG SD3__ = SIMD_LOAD1 (&src.q[j*3+2]);
      SIMD_REG DX1__ = SIMD_SUB (TX__, SIMD_LOAD1 (src.x + j));
      SIMD_REG DY1__ = SIMD_SUB (TY__, SIMD_LOAD1 (src.y + j));
      SIMD_REG DZ1__ = SIMD_SUB (TZ__, SIMD_LOAD1 (src.z + j));
      SIMD_REG R__ = SIMD_MUL (DX1__, DX1__);
      R__ = SIMD_ADD (R__, SIMD_MUL (DY1__, DY1__));
      R__ = SIMD_ADD (R__, SIMD_MUL (DZ1__, DZ1__));
      SIMD_REG TEMP__ = SIMD_CMPEQ (R__, SIMD_ZERO);
#if !defined (USE_FLOAT)
      SIMD_REG XHALF__ = SIMD_MUL (SIMD_SET1 (0.5), R__); 
      R__ = SIMD_INV_SQRT_S (R__);
      SIMD_REG RNR__ = SIMD_MUL (R__, R__);
      RNR__ = SIMD_MUL (RNR__, XHALF__);
      RNR__ = SIMD_SUB (SIMD_SET1 (1.5), RNR__);
      R__ = SIMD_MUL (RNR__, R__);
      //      R__ = SIMD_INV_SQRT (R__);
#else 
      R__ = SIMD_INV_SQRT (R__);
#endif
      //      R__ = SIMD_ANDNOT (TEMP__, R__);
      SIMD_REG DOTX = SIMD_MUL (DX1__, SD1__);
      SIMD_REG DOTY = SIMD_MUL (DY1__, SD2__);
      SIMD_REG DOTZ = SIMD_MUL (DZ1__, SD3__);
      SIMD_REG DSUM = SIMD_ADD (DOTX, DOTY);
      DSUM = SIMD_ADD (DSUM, DOTZ);
      DSUM = SIMD_MUL (DSUM, R__);
      DSUM = SIMD_MUL (DSUM, R__);
      DSUM = SIMD_MUL (DSUM, SC2__);

      DOTX = SIMD_MUL (DSUM, DX1__); 
      DOTY = SIMD_MUL (DSUM, DY1__); 
      DOTZ = SIMD_MUL (DSUM, DZ1__);

      SD1__ = SIMD_MUL (SD1__, SC1__);
      SD2__ = SIMD_MUL (SD2__, SC1__);
      SD3__ = SIMD_MUL (SD3__, SC1__);

      SD1__ = SIMD_ADD (SD1__, DOTX); 
      SD2__ = SIMD_ADD (SD2__, DOTY); 
      SD3__ = SIMD_ADD (SD3__, DOTZ);

      TD1__ = SIMD_ADD (TD1__, SIMD_MUL (SD1__, R__));
      TD2__ = SIMD_ADD (TD2__, SIMD_MUL (SD2__, R__));
      TD3__ = SIMD_ADD (TD3__, SIMD_MUL (SD3__, R__));
    }
    TD1__ = SIMD_MUL (TD1__, OOMU__);
    TD2__ = SIMD_MUL (TD2__, OOMU__);
    TD3__ = SIMD_MUL (TD3__, OOMU__);
    
    SIMD_STORE_U (temp1, TD1__);
    SIMD_STORE_U (temp2, TD2__);
    SIMD_STORE_U (temp3, TD3__);
    
    for (int k = 0; k < SIMD_LEN; k++) {
      trg.q[(i+k)*trg.dof()] += temp1[k];
      trg.q[(i+k)*trg.dof()+1] += temp2[k];
      trg.q[(i+k)*trg.dof()+2] += temp3[k];
    }
  } 
  while (i < trg.num_pts) {
    real_t tx = trg.x[i];
    real_t ty = trg.y[i];
    real_t tz = trg.z[i];
    int j;
    for (j = 0; j < src.num_pts; j++) {
      x = tx - src.x[j];
      y = ty - src.y[j];
      z = tz - src.z[j];
      r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      r = sqrt(r2);
#else
      r = sqrtf(r2);
#endif
      if (r == 0)
        invdr = 0;
      else
        invdr = 1.0 / r;
      dot = (x*src.q[j*3] + y*src.q[j*3+1] + z*src.q[j*3+2]) * invdr * invdr * sc2;
      denx = sc1*src.q[j*3] + dot*x;
      deny = sc1*src.q[j*3+1] + dot*y;
      denz = sc1*src.q[j*3+2] + dot*z;

      trg.q[i*3] += denx*invdr*oomu;
      trg.q[i*3+1] += deny*invdr*oomu;
      trg.q[i*3+2] += denz*invdr*oomu;
    }
    ++i;
  }

  return 0;
}

/* ------------------------------------------------------------------------
*/

void 
Kernel::kernel_setup (Nbody::CodeGenContext& context) {
  /* Code generation using LLVM */
  Nbody::Param<int> sn("sn");
  Nbody::Param<int> tn("tn");

  Nbody::Node<real_t> s(sn, "src", 0);
  Nbody::Node<real_t> t(tn, "trg", 0);

  Nbody::Stmt kernel = dist_laplace(s, t);

  kernel = vectorize(kernel);

  kernel = numerical_opt(kernel);

  Nbody::Argument snode("src", true, Nbody::Int(0));
  Nbody::Argument tnode("trg", true, Nbody::Int(0));
  
  vector<Nbody::Argument> args(4);
  args[0] = snode;
  args[1] = tnode;
  args[2] = sn;
  args[3] = tn;
  context.generateCode(kernel, "test", args);
} 

void 
Kernel::kernel_evaluation_U (Node<real_t> trg, Node<real_t> src, Nbody::CodeGenContext& context)
{
  /* Run the generated code */
  std::vector<llvm::GenericValue> r_args(4);
  r_args[0].PointerVal = src.raw_data_ptr();
  r_args[1].PointerVal = trg.raw_data_ptr();
  r_args[2].IntVal = llvm::APInt(32, src.num_elem());
  r_args[3].IntVal = llvm::APInt(32, trg.num_elem());
  context.runCode (r_args);
}

