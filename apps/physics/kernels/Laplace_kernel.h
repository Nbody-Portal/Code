#ifndef _LAPLACE_KERNEL_H
#define _LAPLACE_KERNEL_H

#include "Simd_helper.h"

#define OOFP_R  (1.0/(4.0 * M_PI))

template <typename PointsIter, typename VecIter>
class LaplaceKernel
{
  bool hom = true;
  int sdof = 1;
  int tdof = 1;

public:
  LaplaceKernel() {}

  int src_dof () { return sdof; }
  int trg_dof () { return tdof; }
  bool homogeneous () { return hom; }

  void homogeneous_degree (real_t* degvec) const { degvec[0] = 1; }

  void kernel_matrix (Points& trg, Points& src, real_t* mat);

  void kernel_evaluation (PointsIter trg, PointsIter src, VecIter den, VecIter pot);
  void kernel_evaluation_optimized (PointsIter trg, PointsIter src, VecIter den, VecIter pot);
};

/** Naive Single-Layer Laplacian velocity kernel */
template <typename PointsIter, typename VecIter>
void
LaplaceKernel<PointsIter,VecIter>::kernel_matrix (Points& trg, Points& src, real_t* mat)
{
  real_t x, y, z, r2, r;
  for (size_t i = 0; i < src.size(); ++i) {
    real_t sx = src[i][0];
    real_t sy = src[i][1];
    real_t sz = src[i][2];
    for (size_t j = 0; j < trg.size(); ++j) {
      x = sx - trg[j][0];
      y = sy - trg[j][1];
      z = sz - trg[j][2];
      r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      r = sqrt(r2);
#else
      r = sqrtf(r2);
#endif
      if (r != 0)
        mat[i*trg.size()+j]  = OOFP_R / r;
    }
  }
}

/* ------------------------------------------------------------------------
*/
template <typename PointsIter, typename VecIter>
void
LaplaceKernel<PointsIter,VecIter>::kernel_evaluation(PointsIter trg, PointsIter src, VecIter den, VecIter pot)
{
  for (auto i = trg.begin(); i < trg.end(); ++i, ++pot) {
    real_t tx = (*i)[0];
    real_t ty = (*i)[1];
    real_t tz = (*i)[2];
    real_t td = 0.0;
    auto sden = den;
    for (auto j = src.begin(); j < src.end(); ++j, ++sden) {
      real_t x = tx - (*j)[0];
      real_t y = ty - (*j)[1];
      real_t z = tz - (*j)[2];
      real_t r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      real_t r = sqrt(r2);
#else
      real_t r = sqrtf(r2);
#endif
      if (r != 0)
        td += *sden / r;
    }
    *pot  += OOFP_R * td;
  }
}
/* ------------------------------------------------------------------------
*/

/** NRSQ fused Single-Layer Laplacian velocity kernel */
template <typename PointsIter, typename VecIter>
void
LaplaceKernel<PointsIter,VecIter>::kernel_evaluation_optimized(PointsIter trg, PointsIter src, VecIter den, VecIter pot)
{
  SIMD_REG OOFP_R__ = SIMD_SET1 (OOFP_R);
  auto i = trg.begin();
  for (; (i+SIMD_LEN) <= trg.end(); i += SIMD_LEN, pot += SIMD_LEN) {
    SIMD_REG TX__ = SIMD_LOAD_U (&(*i)[0]);
    SIMD_REG TY__ = SIMD_LOAD_U (&(*i)[1]);
    SIMD_REG TZ__ = SIMD_LOAD_U (&(*i)[2]);
    SIMD_REG TD__ = SIMD_ZERO;
    auto sden = den;
    for (auto j = src.begin(); j < src.end(); ++j, ++sden) {
      SIMD_REG SD1__ = SIMD_LOAD1 (&*sden);
      SIMD_REG DX1__ = SIMD_SUB (TX__, SIMD_LOAD1 (&(*j)[0]));
      SIMD_REG DY1__ = SIMD_SUB (TY__, SIMD_LOAD1 (&(*j)[1]));
      SIMD_REG DZ1__ = SIMD_SUB (TZ__, SIMD_LOAD1 (&(*j)[2]));
      SIMD_REG R__ = SIMD_MUL (DX1__, DX1__);
      R__ = SIMD_ADD (R__, SIMD_MUL (DY1__, DY1__));
      R__ = SIMD_ADD (R__, SIMD_MUL (DZ1__, DZ1__));
      //SIMD_REG TEMP__ = SIMD_CMPEQ (R__, SIMD_ZERO);
#if !defined (USE_FLOAT)
      SIMD_REG XHALF__ = SIMD_MUL (SIMD_SET1 (0.5), R__);
      R__ = SIMD_INV_SQRT_S (R__);
      SIMD_REG RNR__ = SIMD_MUL (R__, R__);
      RNR__ = SIMD_MUL (RNR__, XHALF__);
      RNR__ = SIMD_SUB (SIMD_SET1 (1.5), RNR__);
      R__ = SIMD_MUL (RNR__, R__);
#else
      R__ = SIMD_INV_SQRT (R__);
#endif
      //R__ = SIMD_ANDNOT (TEMP__, R__);
      TD__ = SIMD_ADD (TD__, SIMD_MUL (SD1__, R__));
    }
    TD__ = SIMD_MUL (TD__, OOFP_R__);
    TD__ = SIMD_ADD (TD__, SIMD_LOAD_U (&*pot));
    SIMD_STORE_U (&*pot, TD__);
  }
  while (i < trg.end()) {
    real_t tx = (*i)[0];
    real_t ty = (*i)[1];
    real_t tz = (*i)[2];
    real_t td = 0.0;
    auto sden = den;
    for (auto j = src.begin(); j < src.end(); ++j, ++sden) {
      real_t x = tx - (*j)[0];
      real_t y = ty - (*j)[1];
      real_t z = tz - (*j)[2];
      real_t r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      real_t r = sqrt(r2);
#else
      real_t r = sqrtf(r2);
#endif
      //      if (r != 0)
      td += *sden / r;
    }
    *pot  += OOFP_R * td;
    ++i;
  }
}
#endif
