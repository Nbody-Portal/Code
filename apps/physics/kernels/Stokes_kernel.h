#ifndef _STOKES_KERNEL_H
#define _STOKES_KERNEL_H

#include "Simd_helper.h"

#define OOFP_R  (1.0/(4.0 * M_PI))
#define OOEP_R  (1.0/(8.0 * M_PI))

template <typename PointsIter, typename VecIter>
class StokesKernel
{
  bool hom = true;
  int sdof = 3;
  int tdof = 3;

public:
  StokesKernel() {}

  int src_dof () { return sdof; }
  int trg_dof () { return tdof; }
  bool homogeneous () { return hom; }

  void homogeneous_degree (real_t* degvec) const {
    degvec[0] = 1; degvec[1] = 1; degvec[2] = 1; degvec[3] = 2;
  }

  void kernel_matrix (Points& trg, Points& src, real_t* mat);

  void kernel_evaluation (PointsIter trg, PointsIter src, VecIter den, VecIter pot);
  void kernel_evaluation_optimized (PointsIter trg, PointsIter src, VecIter den, VecIter pot);
};

/** Naive Stokes velocity kernel for translation matrix calculation */
template <typename PointsIter, typename VecIter>
void
StokesKernel<PointsIter,VecIter>::kernel_matrix (Points& trg, Points& src, real_t* mat)
{
  int m = trg.size() * trg_dof();
  vector<real_t> coefs(2);
  coefs[0] = 1.0;
  coefs[1] = 0.25; //coefs in the kernel, work for all examples
  real_t x, y, z, r2, r;
  real_t G, H;
  real_t mu = coefs[0];
  real_t oomu = 1.0/mu;
  real_t cons = oomu * OOEP_R;

  for (size_t i = 0; i < src.size(); ++i) {
    int c = i * src_dof();
    real_t sx = src[i][0];
    real_t sy = src[i][1];
    real_t sz = src[i][2];
    for (size_t j = 0; j < trg.size(); ++j) {
      int k = j * trg_dof();
      x = trg[j][0] - sx;
      y = trg[j][1] - sy;
      z = trg[j][2] - sz;
      r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      r = sqrt(r2);
#else
      r = sqrtf(r2);
#endif
      G = 1.0 / r;
      H = 1.0 / (r2 * r);
      if (r != 0) {
        mat[c*m+k]    = cons * (G + H*x*x); mat[(c+1)*m+k]    = cons * H*x*y;       mat[(c+2)*m+k]    = cons * H*x*z;
        mat[c*m+k+1]  = cons * H*y*x;       mat[(c+1)*m+k+1]  = cons * (G + H*y*y); mat[(c+2)*m+k+1]  = cons * H*y*z;
        mat[c*m+k+2]  = cons * H*z*x;       mat[(c+1)*m+k+2]  = cons * H*z*y;       mat[(c+2)*m+k+2]  = cons * (G + H*z*z);
      }
    }
  }
}

/* ------------------------------------------------------------------------
*/
template <typename PointsIter, typename VecIter>
void
StokesKernel<PointsIter,VecIter>::kernel_evaluation(PointsIter trg, PointsIter src, VecIter den, VecIter pot)
{
  vector<double> coefs(2);
  coefs[0] = 1.0;
  coefs[1] = 0.25; //coefs in the kernel, work for all examples
  real_t x, y, z, r2, r, invdr;
  real_t dot, denx, deny, denz;
  real_t mu = coefs[0];
  real_t oomu = 1.0/mu;
  real_t cons = oomu * OOEP_R;
  for (auto i = trg.begin(); i < trg.end(); ++i, pot+=3) {
    real_t tx = (*i)[0];
    real_t ty = (*i)[1];
    real_t tz = (*i)[2];
    auto sden = den;
    for (auto j = src.begin(); j < src.end(); ++j, sden+=3) {
      x = tx - (*j)[0];
      y = ty - (*j)[1];
      z = tz - (*j)[2];
      r2 = x*x + y*y + z*z;
#if !defined (USE_FLOAT)
      r = sqrt(r2);
#else
      r = sqrtf(r2);
#endif
      if (r == 0)
        invdr = 0;
      else
        invdr = 1.0 / r;
      dot = (x * *sden + y * *(sden+1) + z * *(sden+2)) * invdr * invdr;
      denx = *(sden) + dot*x;
      deny = *(sden+1) + dot*y;
      denz = *(sden+2) + dot*z;

      *(pot) += denx*invdr*cons;
      *(pot+1) += deny*invdr*cons;
      *(pot+2) += denz*invdr*cons;
    }
  }
}
/* ------------------------------------------------------------------------
*/

#endif
