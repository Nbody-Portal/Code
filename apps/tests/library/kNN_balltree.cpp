#include <iostream>
#include <cstdio>

#include "utils.h"
#include "Binary_tree.h"
#include "Hsphere_bounds.h"
#include "kNN.h"
#include "Clock.hpp"

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <filename> <# neighbors> <level>\n", use);
}

bool validate(int num_pts, int dim, int k, Tree<Hsphere>& stree, Points_t& targets, resultNeighbors& result) {
  bool error = false;
	/* Set tolerance value for validation */
	const real_t tolerance = 1e-2;

  /* Allocate memory for exhaustive calculation of near neighbors */
  kNN_distance* nearest = new kNN_distance[num_pts];

  /* Evaluate test/target point one by one */
  for (int i = 0; i < num_pts; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    for (int j = 0; j < num_pts; j++) {
      real_t dist = 0.0;
      for (int d = 0; d < dim; d++)
        dist += (stree.data[j][d]-targets[i][d]) * (stree.data[j][d]-targets[i][d]);
      nearest[j] = kNN_distance(j, dist);
    }

    /* Sort the source points by its distance to the target points */
    sort (nearest, nearest + num_pts);

    kNeighbors kneighbors = result[i];

    for (int j = 0; j < k; j++) {
      if (fabs(kneighbors[j].squared_distance - nearest[j].squared_distance) >= tolerance) {
        cerr << "Target point: " << i << endl;
        cerr << "Nearest neighbor " << j << " failed!\n";
        cerr << "Correct index: " << nearest[j].index << endl;
        cerr << "Correct distance: " << nearest[j].squared_distance << endl;
        cerr << "Computed index: " << kneighbors[j].index << endl;
        cerr << "Computed distance: " << kneighbors[j].squared_distance << endl;
        error = true;
        exit(0);
      }
    }
  }
  return error;
}

int main (int argc, char** argv)
{
  char* filename;
  int num_pts;
  int dim;
  int k;
  real_t epsilon = 0.0;  /* Set to non-zero value for approximate k-nn */
  bool error = false;
  Clock timer;
  int level;

  if (argc != 6) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  filename = argv[3];
  k = atoi (argv[4]);
  level = atoi(argv[5]);

  /* Allocate memory for original source and target dataset */
  Points_t sources(num_pts, dim);
  Points_t targets(num_pts, dim);

  from_file(sources, targets, filename, filename);

  /* Allocate memory for the permuted source and target points */
  Points_t s_perm(num_pts, dim);
  Points_t t_perm(num_pts, dim);

  /* Create and build source and target tree */
  typedef Tree<Hsphere> TreeType;
  TreeType stree(sources, s_perm);
  TreeType ttree(targets, t_perm);

  fprintf (stderr, "Building source tree...\n");
  timer.start();
  stree.build_balltree();
  double time_tree = timer.seconds();
  fprintf (stderr, "Source tree built.\n");

  int single_mode = getenv__single_mode();
  if (!single_mode) {
    fprintf (stderr, "Building target tree...\n");
    timer.start();
    ttree.build_balltree();
    time_tree += timer.seconds();
    fprintf (stderr, "Target tree built.\n");
  }

#ifdef _DEBUG
  std::cout << "Sources: " << std::endl;
  std::cout << sources << std::endl;

  cout << "In the main program...\n";
  std::cout << s_perm << std::endl;

  cout << "\n Tree structure...\n";
  std::cout << stree << std::endl;
#endif

  /* Get the k nearest neighbours */
  kNN<TreeType> knn(dim, level, stree, ttree);

  /* Calculate k-nearest neighbors using the pruning algorithm */
  resultNeighbors result (num_pts, vector<kNN_distance>(k));
	knn.nearest_neighbors(k, result, epsilon);

#ifdef _DEBUG
  /* Print the neighbors indices and distances */
  for (int i = 0; i < num_pts; i++) {
    kNeighbors kneighbors = result[i];
    for (int j = 0; j < k; j++)
      cout << sqrt(kneighbors[j].squared_distance) << "  ";
    cout << endl;
  }
  for (int i = 0; i < num_pts; i++) {
    kNeighbors kneighbors = result[i];
    for (int j = 0; j < k; j++)
      cout << kneighbors[j].index << "  ";
    cout << endl;
  }
#endif

  /* Validate the results */
//  error = validate(num_pts, dim, k, stree, targets, result);

  /* Print timing results */
  cerr << "Tree construction time: " << time_tree << " seconds\n";
  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}
