#include <iostream>
#include <cstdio>

#include "Octree.h"
#include "bh.h"
#include "Metric.h"




using namespace std;


static inline
  void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <distribution> <level> <tolerance>\n", use);
}

bool validate(std::vector<std::array<real_t,3>>& result_forces,
              Points_t& sources, Points_t& targets, real_t epsilon) {

  SquaredEuclideanMetric<Point, Point> metric;
  bool error = false;
  real_t tolerance = epsilon;
  real_t dist;
  std::vector<std::array<real_t, 3>> results;
  Clock timer;
  for (int i = 0 ; i < targets.size(); i++) {
    std::array<real_t, 3> temp = {0,0,0};
    for (int j = 0; j < sources.size(); j++) {
      if (i != j) {
        dist = metric.compute_distance (targets[i], sources[j]);
        dist = dist * sqrt(dist);
        for (int k = 0; k < 3; k++) {
          temp[k] +=  targets[i][3] * sources[j][3] * 6.674e-11 *
           (targets[i][k] - sources[j][k]) / dist;
        }

      }
    }
    results.push_back(temp);
  }
  // cout << "Naive C++ time:" << timer.seconds() << "\n";
  for (int i = 0 ; i < targets.size(); i++) {
      for (int k = 0; k < 3; k++) {
       if (abs(results[i][k] - result_forces[i][k]) > tolerance) {
         error = true;
         cout << "At ["<< i << "," << k << "]--> expected: " << results[i][k] << ", computed: " << result_forces[i][k] << "\n";
         break;
       }
      }
  }
  return error;
}

int main (int argc, char** argv)
{
  char* distribution;
  unsigned int num_pts, level, dim = 3;
  real_t epsilon;

  if (argc != 5) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  distribution = argv[2];
  level  = atoi (argv[3]);
  epsilon = atof (argv[4]);


  /* Initialize seed */
  util::default_generator.seed(1337);

  /* Allocate memory for original source and target points */
  Points_t sources(num_pts);
  Points_t targets(num_pts);
  Points_t centers(num_pts);

  /* Allocate memory for the permuted source and target points */
  Points_t s_perm(num_pts);
  Points_t t_perm(num_pts);

  fprintf(stderr, "Generating source dataset...\n");
  initialize_dataset(sources, distribution);
  initialize_dataset(targets, distribution);
  initialize_dataset(centers, distribution);


  Tree stree(sources, s_perm, centers);
  Tree ttree(targets, t_perm, centers);


  stree.build_octree();
  ttree.build_octree();

  BarnesHut<Tree> barnes_hut(level, stree, ttree);
  barnes_hut.force_calculate(epsilon);


   bool error = validate(barnes_hut.result_forces, s_perm, t_perm, epsilon);


  if (error)
    cerr << "Program failed, error!\n";
  else
    cerr << "Passed.\n";

  return 0;
}
