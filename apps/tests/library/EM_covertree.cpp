#include <iostream>

#include "utils.h"
#include "Cover_tree.h"
#include "EM_kdtree.h"
#include "Cover_bounds.h"
#include "Clock.hpp"

using namespace std;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> <Gaussian> <threshold> <filename> <numIter> <Epsilon>\n", use);
}


/* validate function for checking the results of EM */

bool validate(int num_pts, int dim, int k, int Iter, real_t tol, Points_t& Source, MeanSigma result) {

  bool error = false;
  EMBase emTest(num_pts, k, dim, Iter, tol, Source);
  emTest.IterationEM();
  int i,j,l;
 
  MeanSigma a;
  a = emTest.Result();
  for (i = 0; i < k; ++i) {
    for (j = 0; j < dim; ++j) {
      if (std::isnan(result.mean[i][j])) {
        cerr << i << "|" << j << ": " << result.mean[i][j] << ", ";
	      cout << "please re-run the test, there is NAN in the mean of a Gaussian" << endl;
	      error = true;
	      exit(0);
      }
      else if (a.mean[i][j] - result.mean[i][j] >= tol){
        cerr << "Gaussian  " << i << " doesn't match at dimension " << j << endl;
        cerr << "Correct version: " << a.mean[i][j] << endl;
        cerr << "Computed version: " << result.mean[i][j] << endl;
        error = true;
        exit(0);
      }
      for (l = 0; l < dim; ++l) {
        if (std::isnan(result.sigma[i][j][l])) {
          cout << i << "|" << j << "|" << l << ": " << result.sigma[i][j][l] << ", ";
          cout << "please re-run the test, there is NAN in the covariance of a Gaussian" << endl;
          error = true;
          exit(0);
        }
        else if (a.sigma[i][j][l] - result.sigma[i][j][l] >= tol){
          cerr << "Gaussian " << i  << " dosn't match at Cov on " << j  << ", "<< l<< endl;
          cerr << "Correct version: " << a.sigma[i][j][l] << endl;
          cerr << "Computed version: " << result.sigma[i][j][l] << endl;
          error = true;
          exit(0);
        }
      }
    }
  }
  return error;
}

int main(int argc, char** argv)
{
  char* filename;
  int num_pts; // number of data points
  int dim; // dimension of data
  int k; // number of clusters
  int Iter;
  real_t epsilon = 0.001;  // criteria to test convergence
  real_t tolerance;
  Clock timer;
  bool error;
  if (argc != 8) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  k = atoi (argv[3]);
  tolerance = atof (argv[4]);
  filename = argv[5];
  Iter = atoi (argv[6]);
  epsilon = atof(argv[7]);


  /* Allocate memory for the input dataset */
  Points_t sources(num_pts, dim);

  /* Read data from file */
  from_file(sources, filename);

  /* Allocate memory for the permuted source and target points */
  Points_t s_perm(num_pts, dim);

  /* Create and build source tree */
  typedef CTree<Cover> TreeType;
  TreeType stree(sources, s_perm);

  fprintf (stderr, "Building source tree ...\n");
  timer.start();
  stree.build_covertree();
  double stree_time = timer.seconds();
  fprintf (stderr, "Source tree built in %f seconds \n ", stree_time);

  /* EM calculation */
  EM<TreeType> emTest(num_pts, k, dim, epsilon, tolerance, stree.data_perm, stree,Iter);
  emTest.IterationEM();
  MeanSigma EM_result = emTest.Result();

 
  /* validate the results */
  error = validate(num_pts, dim, k, Iter, epsilon, sources, EM_result);
 
  /* print timing results */
  cerr << "Tree constuction time: " << stree_time << " seconds" << endl;
  if (error)
    cerr << "Program failed" << endl;
  else
    cerr << "Passed." << endl;
  return 0;
}
