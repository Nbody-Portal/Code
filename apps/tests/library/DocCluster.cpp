#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "math.h"
#include <cfloat>
#include <limits>
#include "Clock.hpp"

using namespace std;

int main (int argc, char** argv){

  if (argc != 3) {
    fprintf(stderr, "usge : % <dim><size>\n", argv[0]);
     return -1;
  }
  int dim = atoi(argv[1]);
  int size = atoi(argv[2]);
  vector<vector<int>> docs;
  for (int i = 0 ; i < size; i++) {
    vector<int> temp;
    for(int j = 0; j < dim; j++) {
        temp.push_back(lrand48());
    }
    docs.push_back(temp);
  }


  vector<int> sample;
  for (int i = 0; i < dim; i++)
    sample.push_back(lrand48());

  Clock timer;

  // Compute the min distance based on the Cosine similarity
  timer.start();
   double min = std::numeric_limits<double>::max();
   int min_index = -1;
   for (int i = 0; i < size; i++) {
         long mul = 0;
         long a = 0;
         long b = 0;
         for(int j = 0 ; j < dim; j++) {
           mul += sample[j] * docs[i][j];
           a += docs[i][j] * docs[i][j];
           b += sample[j] * sample[j];
         }

         double cosineSimilarity = mul / (sqrt(abs(a)) * sqrt(abs(b)));
         // cout << cosineSimilarity << " , " << (abs(a)) << " , " << (abs(b)) << "\n";
         if (cosineSimilarity < min) {
            min = cosineSimilarity;
            min_index = i;
         }

   }
   cout << "Total time: " << timer.seconds() << "\n";
   cout << "cosineSimilarity: " <<  min
        << " , " << min_index << "\n";
  // Compute the min distance based on normalizeing and
  // then caculating the Euclidean distance.
  min_index = -1;
  timer.start();
  long norm = 0;
  for(int j = 0 ; j < dim; j++){
    norm += sample[j]*sample[j];
  }
  norm = sqrt(norm);
  for(int j = 0 ; j < dim; j++){
    sample[j] /= norm;
  }
  min = std::numeric_limits<double>::max();
  for (int i = 0; i < size; i++) {
        norm = 0;
        for(int j = 0 ; j < dim; j++){
          norm += docs[i][j]*docs[i][j];
        }
        norm = sqrt(norm);
        for(int j = 0 ; j < dim; j++){
          docs[i][j] /= norm;
        }
        long temp = 0;
        for(int j = 0 ; j < dim; j++) {
          temp += (docs[i][j] - sample[j]) * (docs[i][j] - sample[j]);
        }
        temp = sqrt(temp);
        if (temp <  min) {
          min = temp;
          min_index = i;
        }
   }
   cout << "Total time: " << timer.seconds() << "\n";
   cout << "Euclidean_similarity: " <<  min
        << " , " << min_index << "\n";
  return 0;

}
