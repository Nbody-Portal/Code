
#include <iostream>

#include "utils.h"
#include "prune_generator.cpp"
#include "Func.h"
#include "Operators.h"
#include "Codegen.h"
#include "IRNode.h"
#include "IROperator.h"
#include "Argument.h"
#include "Param.h"
#include "Type.h"
#include "Variable.h"

using namespace std;
using namespace Nbody;

static inline
  void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> (<filename>) \n", use);
}


int main(int argc, char** argv)
{
  char* filename;
  int num_pts; // number of data points
  int dim; // dimension of data

  if (argc != 4 && argc != 3) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);
  filename = argv[3];
  
  /* Allocate memory for original source and target dataset */
  Points sources(num_pts, dim);
  Points targets(num_pts, dim);
  
  /* Allocate memory for the permuted source and target points */
  Points s_perm(num_pts, dim);
  Points t_perm(num_pts, dim);

  if (argc == 3) {

    /* Initialize seed */
    util::default_generator.seed(10);

    cout << "Generating input points...\n";
    //Points<real_t> input(num_pts, dim);
    for (int i = 0; i < num_pts; ++i)
      for (int j = 0; j < dim; ++j) {
        sources[i][j] = util::random<real_t>::get();
        targets[i][j] = util::random<real_t>::get();
      }
  } else {
    filename = argv[3];
    from_file(sources, targets, filename, filename);
  }
  /* Create and build source and target tree */
  typedef Tree<Hsphere> TreeType;
  TreeType stree(sources, s_perm);
  TreeType ttree(targets, t_perm);

  stree.build_kdtree();
  ttree.build_kdtree();

  /* predefined set of operators, here 2 of them */
  vector<Op*> op;
  Op*  o1 = new Op_Min();
  Op*  o2 = new Op_Max();
  op.push_back(o1);
  op.push_back(o2);

  Var x,y;
  Func kernel;
  kernel(x,y) = pow((x-y), 2);

   
  
  //Box* s = &stree.node_data[stree.root().child];
  Box* t = &stree.node_data[0];
 
  /*instantiating from prune generator and calling the prune for two box with two operators */
  
  //prune_generator<TreeType> pg_test(op, kernel, stree, ttree);
  prune_generator<TreeType> pg_test(op, stree, ttree );

  Point* s = &ttree.data[0];
  pg_test.prune(*t,*s);
  
  cout << "Passed1 Point & Box" << endl;
  cout << "Prune size -->" << pg_test.prune_size << endl;
  
  Box* ss = &ttree.node_data[0];
  pg_test.prune(*t,*ss);

  cout << "Passed2 Box and Box" << endl;
  cout << "Prune size -->" << pg_test.prune_size << endl;


  return 0;
}
