/*
 * computing the nearest neighbor from two file
 */
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

using namespace std;

double naiveCompute(int num_pts1,int  num_pts2, double** dataset1, double** dataset2, int Dim ) {
  double min = 0.0;
  
  for (int d = 0; d < Dim; d++) 
    min += (dataset1[1][d]- dataset2[1][d]) * (dataset1[1][d]- dataset2[1][d]) ;
  // two simple loop for iterating over the whole data
  for (int i = 0; i < num_pts1; i++) {
    for (int j = 0; j < num_pts2; j++) {
      double dist = 0.0;
      for (int d = 0; d < Dim; d++) 
        dist += (dataset1[i][d]- dataset2[j][d]) * (dataset1[i][d]- dataset2[j][d]) ;
      if (dist < min)
        min = dist;
      
    }
  }
  return min;
}


int main(int argc , char**  argv) {
  
  double** d1;
  double** d2;
  int num_pts1, num_pts2, Dim;
  if (argc != 4) {
    fprintf (stderr, "<num_of_pts1> <num_of_pts2> <Dim> ");
  }
 
  num_pts1 = atoi (argv[1]);
  num_pts2 = atoi (argv[2]);
  Dim = atoi (argv[3]);

  d1 = new double*[num_pts1];
  d2 = new double*[num_pts2];
  for (int i = 0; i < num_pts1; i++) { 
    d1[i] = new double[Dim];
    for (int d = 0; d < Dim; d++) 
       d1[i][d] = rand();
  }

  for (int i = 0; i < num_pts2; i++) { 
    d2[i] = new double[Dim];
    for (int d = 0; d < Dim; d++) 
       d2[i][d] = rand();
 }
 

  naiveCompute(num_pts1, num_pts2, d1, d2, Dim); 
  return 0;
}
