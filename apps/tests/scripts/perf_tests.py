#!/usr/bin/env python

import os
from os import path
import time
import argparse
import csv

# ----- main directories for executions ----- #
TOP_DIR = "/home/Laleh/Workspace/Nbody-ML/"
RUN_DIR = TOP_DIR + "apps/tests/library"
OUT_DIR = TOP_DIR +  "apps/tests/results/"
DATASET_DIR = "/home/real_datasets/"
RESULTS_DIR = TOP_DIR +  "apps/tests/"


# ----- setups for scaling experiments ----- #
TREES = ["kdtree"]
#TREES = ["kdtree", "balltree"]

TRAVERSALS = ["yes"]
#TRAVERSALS = ["yes", "no"]  # I need to add another level for Multi tree too and change it as S,D,M (Single, Dual, Multi)

#EXPR_NUMBERS = [1, 2, 3, 4, 5]
EXPR_NUMBERS = [1, 2, 3]

# ----- threads for scaling -----#
#THREADS = [1, 2, 4, 8, 16, 32]
THREADS = [32]

# ---- very small data set jut for checking ----- #
#DATASETS = ["G2-D2-5000"]
#DIM = [2]
#SIZE = [5000]

# ---- real data sets with smaller size ----- #
#DATASETS = ["IHEPC", "Census3", "KDD2", "yahoo2","HIGGS2"]
#DIM = [9, 68, 37, 11, 29]
#SIZE = [2075259, 100000, 1000000, 1000000, 1000000]

DATASETS = ["G2-D2-500000","IHEPC_CLEAN"]
DIM = [2, 9]
SIZE = [500000, 2049280]
# ---- real data sets with larger size ----- #
#DATASETS = ["IHEPC", "Census3", "KDD2", "yahoo2","HIGGS2"]
#DIM = [9, 68, 37, 11, 29]
#SIZE = [2075259, 2458285, 4898431, 41904293, 11000000]


# ----- scaling function  for kNN algorithm ----- #
def scaling_kNN():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for threads in THREADS:
          for expNum in EXPR_NUMBERS:
          
            outputName = OUT_DIR + "perf-kNN-%s-%s-th%d-expr%d.csv"%(dataset, tree, threads, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            command = "(env OMP_NUM_THREADS=%d S=no %s/test--kNN_%s %d %d %s 3 4 2>&1 | tee -a  %s)" \
                         % (threads, RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command) 

        level = level + 1



# ----- scaling function  for kde algorithm ----- #
def scaling_kde():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for threads in THREADS:
          for expNum in EXPR_NUMBERS:
        
            outputName = OUT_DIR + "perf-kde-%s-%s-th%d-expr%d.csv"%(dataset, tree, threads, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            # for the bandwidth of KDE I used 50 for smaller datasets and 30 for larger ones 
            command = "(env OMP_NUM_THREADS=%d S=no %s/test--kde_%s %d %d %s 30 4 2>&1 | tee -a  %s)" \
                         % (threads, RUN_DIR, tree, SIZE[level], DIM[level], datasetName, outputName)
            os.system(command)
            
        level = level + 1




# ----- scaling function  for HD algorithm ----- #
def scaling_HD():
  for traversal in TRAVERSALS:
    for tree in TREES:
      level = 0
      for dataset in DATASETS:
        for threads in THREADS:
          for expNum in EXPR_NUMBERS:

            outputName = OUT_DIR + "perf-HD-%s-%s-th%d-expr%d.csv"%(dataset, tree, threads, expNum)
            datasetName = DATASET_DIR + dataset + ".csv"
            command = "(env OMP_NUM_THREADS=%d S=no %s/test--HD_%s %d %d %s %s 4  2>&1 | tee -a  %s)" \
                         % (threads, RUN_DIR, tree, SIZE[level], DIM[level], datasetName, datasetName, outputName)
            os.system(command)
            
        level = level + 1




def main():

  # ----- making a directory for results ----- #
  output_dir = RESULTS_DIR + "results"
  os.system("mkdir " + output_dir )
  # ----- call to seperate functions for each algorithm ----- #
  scaling_kNN()
  scaling_kde()
  scaling_HD()

if __name__ == "__main__":
  main()


