#ifndef INC_RANGE_H
#define INC_RANGE_H

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif

/* Range class contains min and max values of a range. */
class Range {
  private:
    real_t min_;
    real_t max_;

  public:
    Range (real_t& r) : min_(r), max_(r) {}
    Range (real_t& min, real_t& max) : min_(min), max_(max) {}

    real_t min() const { return min_; }
    real_t max() const { return max_; }
    
    inline bool overlaps(const Range& r) const {
      return min_ <= r.max_ && max_ >= r.min_;
    }

    inline bool contains(const real_t d) const {
      return d >= min_ && d <= max_;
    }

    inline bool contains(const Range& r) const {
      return min_ <= r.min_ && max_ >= r.max_;
    }
};

#endif
