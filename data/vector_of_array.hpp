#pragma once

#include <iostream>
#include <vector>
#include <array>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstring>
#include <math.h>
#include <iterator>
#include <boost/iterator/iterator_adaptor.hpp>

#ifdef NDEBUG
#  include <assert.h>
#else               /* debugging not enabled */
#  define assert(x) /* empty macro to prevent errors */
#endif

using namespace std;

/** A class representing a (resizable) std::vector of fixed sized, but dynamic
 * arrays
 */
template <typename T>
class vector_of_array {
 private:
  // The run-time size of the inner arrays
  long n_;
  // The underlying data representing the vector of arrays
  // std::vector<T> data_;
  std::vector<std::vector<T> > data_; // use 2D vector instead

  typedef typename std::vector<T>::iterator data_iterator;

  class dynarray;
  class dynarray_iterator;

 public:
  // The type of the inner array
  typedef dynarray array_type;

  // std:: types
  typedef array_type          value_type;
  typedef array_type          reference;
  typedef array_type          const_reference;
  typedef dynarray_iterator   iterator;
  typedef dynarray_iterator   const_iterator;
  typedef long                size_type;
  typedef std::ptrdiff_t      difference_type;

  // Construct an invalid vector of arrays
  vector_of_array() : n_(0), data_() {}
  // Construct m arrays of size n
  // vector_of_array(long m, long n) : n_(n), data_(m*n) {}
  vector_of_array(long m, long n) : n_(n), data_(m, std::vector<T>(n)) {}

  // Expert only access to underlying data
  //       T* data()       { return data_.data(); }
  // const T* data() const { return data_.data(); }

        T* data()       { return data_[0].data(); }
  const T* data() const { return data_[0].data(); }

  // size_type size()       const { return data_.size() / n_; }
  size_type size()       const { return data_.size(); }

  size_type array_size() const { return n_; }

  /*
  array_type operator[](size_type i) {
    assert(i < size());
    return {n_, data_.data() + i*n_};
  }
  */

  array_type operator[](size_type i) {
    // assert(i < size());
    return {n_, data_[i].data()};
  }

  /*
  array_type operator[](size_type i) const {
    assert(i < size());
    return {n_, const_cast<T*>(data_.data()) + i*n_};
  }
  */

  array_type operator[](size_type i) const {
    // assert(i < size());
    return {n_, const_cast<T*>(data_[i].data() ) };
  }

  /*
  iterator begin() {
    return iterator(data_.begin(), n_);
  }
  iterator end() {
    return iterator(data_.end(), n_);
  }
  */

  iterator begin() {
    return iterator(data_[0].begin(), n_);
  }
  iterator end() {
    return iterator(data_[size()-1].end(), n_);
  }


  // TODO: Remove!
  void load_data (std::istream& str);


 private:

  class dynarray {
    long n_;
    T* ptr_;
    friend class vector_of_array;

    // Private default
    dynarray() = delete;


    // Private constructor
    dynarray(long n, T* ptr) : n_(n), ptr_(ptr) {}
   public:

    friend std::ostream& operator<< (std::ostream& os, const dynarray& arr) {
      for (long i = 0; i < arr.size(); ++i)
        os << arr[i] << "|";
      return os;
    }

    typedef T               value_type;
    typedef T&              reference;
    typedef const T&        const_reference;
    typedef T*              iterator;
    typedef const T*        const_iterator;
    typedef long            size_type;
    typedef std::ptrdiff_t  difference_type;

    //! Assignment operator
    dynarray& operator=(const dynarray& other) {
      // assert(n_ == other.n_);
      std::copy(other.begin(), other.end(), begin());
      return *this;
    }

    reference operator[](size_type i) {
      // assert(i < size());
      return ptr_[i];
    }

    const_reference operator[](size_type i) const {

      // assert(i < size());
      return ptr_[i];
    }

    T*       data()       { return ptr_[0]; }
    const T* data() const { return ptr_[0]; }

    reference       front()       { return ptr_[0]; }
    const_reference front() const { return ptr_[0]; }
    reference        back()       { return ptr_[n_-1]; }
    const_reference  back() const { return ptr_[n_-1]; }

    size_type     size() const { return n_; }
    size_type max_size() const { return n_; }
    bool         empty() const { return false; }

    // ITERATORS

    iterator        begin()       { return ptr_; }
    const_iterator  begin() const { return ptr_; }
    const_iterator cbegin() const { return ptr_; }

    iterator          end()       { return ptr_+n_; }
    const_iterator    end() const { return ptr_+n_; }
    const_iterator   cend() const { return ptr_+n_; }
  }; // end dynarray

  class dynarray_iterator
      : public boost::iterator_adaptor<dynarray_iterator,   // Derived Type
                                       data_iterator,       // Base Type
                                       dynarray> {          // Value Type
   public:
    using difference_type = typename data_iterator::size_type;

    // Construct an invalid iterator
    dynarray_iterator() {}
    // Construct a strided iterator
    dynarray_iterator(T* base, long stride)
        : dynarray_iterator::iterator_adaptor(base), stride_(stride) {}

    dynarray dereference() const {
      return dynarray(&(*(this->base_reference())), stride_);
    }
    void increment() {
      this->base_reference() += stride_;
    }
    void decrement() {
      this->base_reference() -= stride_;
    }
    void advance(difference_type n) {
      this->base_reference() += n*stride_;
    }
    difference_type distance_to(const dynarray_iterator& y) const {
      return (y.base_reference() - this->base_reference()) / stride_;
    }
   private:
    friend class iterator_core_access;
    const long stride_;
  };
};


// TODO: Optimize and robust?
template <typename T>
std::istream&
load_data(std::istream& s, vector_of_array<T>& v, char delim = ',') {
  unsigned i = 0, j = 0;
  std::string line, field;
  while (getline(s, line)) {
    std::stringstream lineStream(line);
    while (std::getline(lineStream, field, delim)) {
      std::istringstream is(field);
      is >> v[i][j];
      ++j;
    }
    ++i;
    j = 0;
  }
  return s;
}

template <typename T>
std::istream&
load_data_column_major(std::istream& s, vector_of_array<T>& v, char delim = ',') {
  unsigned i = 0, j = 0;
  std::string line, field;
  while (getline(s, line)) {
    std::stringstream lineStream(line);
    while (std::getline(lineStream, field, delim)) {
      std::istringstream is(field);
      is >> v[j][i];
      ++j;
    }
    ++i;
    j = 0;
  }
  return s;
}


/**
 * Generate and initialize a set of \a n points based on the
 * given input \a distribution
 * \param n             Number of elements/points in the input data
 * \param dim           Number of the dimensions in the input data
 * \param distribution  Distribution from which the points are generated
 */
template <typename T>
int
initialize_dataset(vector_of_array<T>& v, const std::string& distribution) {
  /* Initialize points based on an uniform random distibution */
  ofstream data_file;
  data_file.open("/home/real_datasets/D3-uniform.txt");
  if (distribution == std::string("uniform")) {
    fprintf (stderr, "Using uniform distribution for the dataset aaa\n");
    for (int i = 0; i < v.size(); ++i){
      for (int j = 0; j < v[i].size(); ++j)
        v[i][j] = util::random<T>::get();
      data_file << to_string(double(v[i][0])) << " , "<< to_string(double(v[i][1])) << " , "<< to_string(double(v[i][2]))  << "\n";
    }

  }
  data_file.close();
  return 0;
}

template <typename T>
int
initialize_dataset(std::vector<std::array<T,3>>& v, const std::string& distribution) {
  /* Initialize points based on an uniform random distibution */
  ofstream data_file;
  data_file.open("/home/real_datasets/D3-uniform.txt");
  if (distribution == std::string("uniform")) {
    fprintf (stderr, "Using uniform distribution for the dataset fff\n");
    for (int i = 0; i < v.size(); ++i) {
      for (int j = 0; j < v[i].size(); ++j)
        v[i][j] = (2.0 * drand48() - 1.0);
      data_file << to_string(double(v[i][0])) << " , "<< to_string(double(v[i][1])) << " , "<< to_string(double(v[i][2]))  << "\n";
    }
    data_file.close();
  }
  if (distribution == std::string("elliptical")) {
    fprintf (stderr, "Using elliptical distribution for the dataset\n");
    const double mu = 0.49; // redius
    const double center[3] = {0,0,0}; // center
    for (int i = 0; i < v.size(); ++i) {
      int j = 0;
      double phi = 2* M_PI * drand48();
      double theta = M_PI * drand48();
      v[i][0] = center[0] + 0.25 * mu * sin(theta) * cos(phi);
      v[i][1] = center[1] + 0.25 * mu * sin(theta) * sin(phi);
      v[i][2] = center[2] + mu * cos(theta);
    }
  }
  return 0;
}

template <typename T>
int
initialize_dataset(std::vector<std::array<T,4>>& v, const std::string& distribution) {
  /* Initialize points based on an uniform random distibution */
  if (distribution == std::string("uniform")) {
    fprintf (stderr, "Using uniform distribution for the dataset\n");
    for (int i = 0; i < v.size(); ++i) {
      int j = 0;
        for (j = 0; j < v[i].size(); ++j)
          v[i][j] = (2.0 * drand48() - 1.0);
      v[i][j] *= 1000;
    }
  }
  if (distribution == std::string("elliptical")) {
    fprintf (stderr, "Using elliptical distribution for the dataset\n");
    // ofstream data_file;
    // data_file.open("/home/real_datasets/D4-elliptical.txt");

    const double mu = 0.49; // redius
    const double center[3] = {0,0,0}; // center
    for (int i = 0; i < v.size(); ++i) {
      int j = 0;
      double phi = 2* M_PI * drand48();
      double theta = M_PI * drand48();
      v[i][0] = center[0] + 0.25 * mu * sin(theta) * cos(phi);
      v[i][1] = center[1] + 0.25 * mu * sin(theta) * sin(phi);
      v[i][2] = center[2] + mu * cos(theta);
      v[i][3] = 1000 * drand48();
      // data_file << to_string(double(v[i][0])) << " , "<< to_string(double(v[i][1])) << " , "<< to_string(double(v[i][2])) << " , "<< to_string(double(v[i][3])) << "\n";

    }
    // data_file.close();
  }
  return 0;
}

template <typename T>
std::ostream& operator<<(std::ostream& s, const vector_of_array<T>& v) {
  for (size_t i = 0; i < v.size(); ++i) {
    for (size_t j = 0; j < v[i].size(); ++j)
      s << v[i][j] << " ";
    s << std::endl;
  }
  return s;
}
