#ifndef _FIXED_VECTOR_H_
#define _FIXED_VECTOR_H_

#include <memory>
#include <algorithm>

template <typename T>
class fixed_vector {
 private:
  size_t length;
  std::unique_ptr<T[]> buffer;

 public:
  fixed_vector() : length(0), buffer() {}

  explicit fixed_vector(size_t N) : length(N), buffer(new T[length]) {}

  fixed_vector(const fixed_vector& o) : length(o.size()), buffer(new T[length]) {
    std::copy(o.begin(), o.end(), begin());
  }

  fixed_vector& operator=(fixed_vector&&) = default;

  fixed_vector(fixed_vector&&) = default;

  fixed_vector& operator=(const fixed_vector& o) {
    std::unique_ptr<T[]> tmp(new T[o.length]);
    copy(o.begin(), o.end(), tmp.get());
    length = o.length;
    buffer = move(tmp);
    return *this;
  }
  
  void clear(){
	  //delete buffer;
	  //std::unique_ptr<T[]> tmp(new T[length]);
	  //buffer = move(tmp);
	  buffer.reset();
	  buffer = std::unique_ptr<T[]>(new T[length]);
  }

  size_t size() const { return length; }
  bool empty()  const { return false; }

        T*  begin()         { return data(); }
  const T*  begin() const   { return data(); }
        T*    end()         { return data()+size(); }
  const T*    end() const   { return data()+size(); }

        T&  front()       { return *begin(); }
  const T&  front() const { return *begin(); }
        T&   back()       { return *(begin()+size()-1); }
  const T&   back() const { return *(begin()+size()-1); }

        T*  data()        { return buffer.get(); }
  const T*  data() const  { return buffer.get(); }

        T& operator[](size_t i)       { return data()[i]; }
  const T& operator[](size_t i) const { return data()[i]; }
};

#endif
