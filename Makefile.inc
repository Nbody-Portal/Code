
all-targets: announce all-recursive all-local

#=======================================================================
# Auto-detect an appropriate configuration.
#
# Override by setting environment variable, 'BUILD_ARCH'.
#=======================================================================

ifeq ($(host-name),)
  host-name := $(shell hostname)
endif

ifeq ($(kernel-name),)
  kernel-name := $(shell uname -s)
endif

ifeq ($(arch-name),)
  arch-name := $(shell uname -p)
endif

ifeq ($(proc-name),)

  # Try to detect processor

  ifeq ($(detect-cpuinfo),)
    ifeq ($(kernel-name),Darwin)
      detect-cpuinfo := $(shell sysctl machdep.cpu.brand_string | grep "^.*Xeon")
    endif
    ifeq ($(kernel-name),Linux)
      detect-cpuinfo := $(shell grep "^model name.*Xeon" /proc/cpuinfo)
    endif
    ifneq ($(detect-cpuinfo),)
      proc-name := Xeon
      proc-vendor := Intel
    endif
  endif

  ifeq ($(detect-cpuinfo),)
    ifeq ($(kernel-name),Darwin)
      detect-cpuinfo := $(shell sysctl machdep.cpu.brand_string | grep "^.*Core(TM)2 Duo")
    endif
    ifeq ($(kernel-name),Linux)
      detect-cpuinfo := $(shell grep "^model name.*Core(TM)2 Duo" /proc/cpuinfo)
    endif
    ifneq ($(detect-cpuinfo),)
      proc-name := Core2Duo
      proc-vendor := Intel
    endif
  endif

  ifeq ($(detect-cpuinfo),)
    detect-cpuinfo := $(shell grep "^model name.*Quad-Core .*Opteron" /proc/cpuinfo)
    ifneq ($(detect-cpuinfo),)
      proc-name := Barcelona
      proc-vendor := AMD
    endif
  endif

  ifeq ($(proc-name),)
    proc-name := unknown
    proc-vendor := unknown
  endif
endif

ifeq ($(cc-name),)
  cc-name := $(shell basename $(shell which clang))
  ifeq ($(cc-name),)
    cc-name := $(shell basename $(shell which icc))
  endif
  ifeq ($(cc-name),)
    cc-name := $(shell basename $(shell which gcc))
  endif
endif

ifeq ($(BUILD_ARCH),)
  BUILD_ARCH = $(arch-name)-$(proc-name)-$(kernel-name)-$(cc-name)
endif

include $(top_srcdir)/arch/$(BUILD_ARCH)

#=======================================================================
# Default build rules
#=======================================================================

CXXFLAGS_STD =
CXXFLAGS_STD += -I.
CXXFLAGS_STD += -I$(top_srcdir)/src
CXXFLAGS_STD += -I$(top_srcdir)/data
CXXFLAGS_STD += -I$(top_srcdir)/tree
CXXFLAGS_STD += -I$(top_srcdir)/utils
CXXFLAGS_STD += -I$(top_srcdir)/apps/machine_learning/metrics
CXXFLAGS_STD += -I$(top_srcdir)/apps/machine_learning/neighbor_search
CXXFLAGS_STD += -I$(top_srcdir)/apps/machine_learning/neighbor_search/kNN_vector.h


# Dependency directory and flags
DEPSDIR := $(shell mkdir -p .deps; echo .deps)
# MD: Dependency as side-effect of compilation
# MF: File for output
# MP: Include phony targets
DEPSFILE = $(DEPSDIR)/$(notdir $*.d)
DEPSFLAGS = -MD -MF $(DEPSFILE) #-MP

CLEANFILES += $(DEPSDIR)
MKL_sourcing :=source /home/opt/intel/parallel_studio_xe_2019/psxevars.sh > a.out

%.o: %.c
	$(CC) $(CFLAGS_STD) $(DEPSFLAGS) $(CFLAGS) $(COPTFLAGS) -o $@ -c $<

%.o: %.cpp
	$(MKL_sourcing) && $(CXX) $(CXXFLAGS_STD) $(DEPSFLAGS) $(CXXFLAGS) $(LLVM_CXX_FLAGS)  $(CXXOPTFLAGS)  -o $@ -c $<

announce:  hline-top announce-arch hline-bottom

hline-top:
	@echo "=================================================="

hline-bottom:
	@echo "=================================================="

announce-arch:
	@echo "Build architecture: $(BUILD_ARCH)"

all-local: all

all-recursive:
	@$(MAKE) recursive RECTGT=all

clean: clean-local clean-recursive

clean-recursive:
	@$(MAKE) recursive RECTGT=clean

clean-local:
	rm -rf $(TARGETS) $(DEPSDIR) $(CLEANFILES)

recursive:
	@test -n "$(RECTGT)"
	@for subdir in $(SUBDIRS) ; do \
	  echo "=== $(RECTGT): Entering subdirectory, `pwd`/$$subdir ... ===" ; \
	  cd $$subdir || exit 1 ; \
	  $(MAKE) $(RECTGT) || exit 1 ; \
	  cd - ; \
	done

# Include the dependency files
-include $(wildcard $(DEPSDIR)/*.d)

# eof
