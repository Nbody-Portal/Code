#ifndef _MULTI_TREE_TRAVERSAL_H_
#define _MULTI_TREE_TRAVERSAL_H_
#define counter_type unsigned long long

#ifdef _DEBUG_PAR
#define DEBUG_CMD(cmd) do { cmd} while( false )
#else
#define DEBUG_CMD(cmd) do { } while ( false )
#endif

#include <cfloat>
#include <omp.h>
#include <NBodyLower.h>
#include "IRNode.h"


/*
* This class is the general version for Dual-tree and
* designed to work for more than two tree as well as
* supporting non-binray tree.It is compatible with portal
*/


template <typename Tree, typename Base, typename pruneGenerator>
class MultiTreeTraversal {
 public:
  typedef typename Tree::NodeTree Box;
  std::vector<Tree*> tree_set;
  // std::vector<Tree*>& tree_set;
  Base& base;
  pruneGenerator prune;
  int task_level;
  real_t temp = -1;
  counter_type num_prunes = 0;
  /* coutner for debuging and analysing the size of prune and computations*/
  #ifdef _DEBUG_PAR
    counter_type num_basecases = 0;
    counter_type num_basecase[32];
    counter_type num_pruneEl[32];
  #endif

  /* Convenience constructor */
  // MultiTreeTraversal(std::vector<Tree*>& trees, Base&  N, pruneGenerator pg, counter_type& num, int level = 4)
    // : tree_set(trees), base(N), prune(pg), num_prunes(num), task_level(level) {
  MultiTreeTraversal(std::vector<Tree*> trees, Base&  N, pruneGenerator pg, counter_type& num, int level = 4)
      : tree_set(trees), base(N), prune(pg), num_prunes(num), task_level(level) {
        #ifdef _DEBUG_PAR
          for (size_t i= 0; i < 32; i++ ) {
            num_basecase[i] = 0;
            num_pruneEl[i] = 0;
          }
        #endif


  }
  /* Traverses trees checking hypersphere-hyperrectangle intersections to discard regions of the space */
  void traverse () ;

 private:
  /* Implementation helper function */
  void traverse_impl(std::vector<Box*> box_set, unsigned level = 0) ;
};

/**
 * Traverse the tree while discarding regions of space
 * with hypersphere-hyperrectange intersections.
 */
template <typename Tree, typename Base, typename pruneGenerator>
void
MultiTreeTraversal<Tree,  Base, pruneGenerator>::traverse()  {
  cout << "in multi tree traversal 1\n";
  vector<Box*> box_set;
  cout << "in multi tree traversal 2\n tree_set size: " << tree_set.size() << "\n" ;
  for (size_t i = 0 ; i < tree_set.size(); i++) {
    // box_set.push_back(&tree_set[i]->root());
    cout << "tree layer : " << i << ": " ;
    cout << tree_set[i]->root().begin() << " , " << tree_set[i]->root().end() << "\n";
    box_set.push_back(&tree_set[i]->root());
  }
  /* setting the nested parallelism */
  omp_set_nested(1);

  #pragma omp parallel
  {
    #pragma omp single nowait
    {
      traverse_impl(box_set, 0);
    }
  }
  #ifdef _DEBUG_PAR
    for (size_t i = 0; i < 32; i++) {
       num_basecases += num_basecase[i];
       cout  << "basecase " << i << " : " << (num_basecase[i]) << "\n" ;
    }
    for (size_t i = 0; i < 32; i++) {
        num_prunes += num_pruneEl[i];
        cout  << "pruneNodes " << i << " : " << (num_pruneEl[i]) << "\n" ;
    }
    cout << "Total base cases: " << num_basecases << "\n";
    cout << "Total prunes: " << num_prunes << "\n";
  #endif
}


/* implementation of traversal*/
template <typename Tree, typename Base, typename pruneGenerator>
void
MultiTreeTraversal<Tree, Base, pruneGenerator >::traverse_impl(std::vector<Box*> box_set,
                                             unsigned level)  {

  if (!prune.prune_subtree(box_set)) {
   // if (true){
    cout << "in multi tree traversal 5\n";
    /* If all the nodes in the node_set are leaves,
     then evaluate the base case */
    bool base_case = true;
    int  divide_node = -1;
    /* Checks if the nodes are leaves as well as finding the divide node */
    for (size_t i = 0; i < box_set.size(); i++) {
      cout << "in multi tree traversal 6\n";
      if (!box_set[0]->is_leaf())
        cout << "just checking the leaf\n";

      if (!box_set[i]->is_leaf()) {
        base_case = false;
        divide_node = i;
        break;
      }
      cout << "in multi tree traversal 7\n";
    }
    if (base_case) {
      cout << "in multi tree traversal 8\n";
      base.base_case (box_set);
      cout << "in multi tree traversal 8.1\n";
      temp = base.get_temp();
      cout << "in multi tree traversal 8.2\n";
      // prune.setTemp(divide_node,temp);
      #ifdef _DEBUG_PAR
        counter_type base_size = 1;
        for(size_t i = 0; i < box_set.size(); i++)
           base_size *= box_set[i]->size();
        num_basecase[omp_get_thread_num()] += base_size;
      #endif
    }
    else {
      cout << "in multi tree traversal 9\n";
     /* Recurse down the target node. Recursion order does not matter. */
        int child_id;
        std::vector<Box*> child_node_set = box_set;
        // std::vector<int> visit_order = prune.visit(box_set, divide_node);
        std::vector<int> visit_order;
        visit_order.push_back(0);
        visit_order.push_back(1);
        cout << "in multi tree traversal 10\n";
        cout << "visit order size: ";
        cout<< visit_order.size() << "\n";
        for (int i = 0; i < visit_order.size(); ++i) {
          cout << "in multi tree traversal 11\n";
          child_id = box_set[divide_node]->child + visit_order[i];
          cout << "in multi tree traversal 12\n";
          child_node_set[divide_node] = &tree_set[divide_node]->node_data[child_id];
          cout << "in multi tree traversal 13\n";
          if (level < task_level) {

            // #pragma omp task shared(box_set) untied
            {
              traverse_impl(child_node_set, level+1);
            }

          }else{
            traverse_impl(child_node_set, level+1);
          }
        }
        // #pragma omp taskwait
    }
  }
  else {
    /* handing the case when we prune the node sets and their coressponding subtrees*/
    #if 0
    prune.centroid_case(box_set);
    for (size_t i = 0; i < box_set.size()-1; i++)
      base.adjustPartitionCounter(i+1 , box_set[i]->begin() , box_set[i]->end() , box_set[i+1]->size());
    #ifdef _DEBUG_PAR
      real_t prune_size = 1;
      for(size_t i = 0; i < box_set.size(); i++)
         prune_size *= box_set[i]->size();
        num_pruneEl[omp_get_thread_num()] += prune_size;
    #endif
    #endif
  }
}

#endif
