/**
 *  Defines an Octree
 */

#ifndef INC_OCTREE_H
#define INC_OCTREE_H

#include <vector>
#include "vec3t.h"
#include "utils.h"
#include "vector_of_array.hpp"
#include "fixed_vector.h"

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif

//typedef vector_of_array<real_t> Points;
//typedef typename Points::array_type Point;

// typedef std::vector<std::array<real_t, 3> > Points;
// typedef std::array<real_t, 3> Point;

/*
typedef std::vector<std::array<real_t, 4> > Points_t;
typedef std::array<real_t, 4> Point;

typedef std::vector<real_t> Vec;
typedef std::vector<std::array<real_t, 4> > Mat;
*/

//typedef fixed_vector<real_t> Vec;
//typedef vector_of_array<real_t> Mat;


/*! A 3-D point with the x, y, and z-coordinates represented as reals */
typedef Vec3<real_t> Point3;
/*! A 3-D index with the three indices represented as integers */
typedef Vec3<int>    Index3;

#include <stddef.h>
using namespace std;

/*
enum {
  LET_SRCNODE = 1,
  LET_TRGNODE = 2
};
*/

/* Class for wrapper around tree structure */
template <typename Boundary>
class OTree {
public:
  /* Tree structure */
  class NodeTree {
    friend class OTree;

    /* Node index */
    unsigned int nodeid;
  public:
    int parent, child, depth;
	  Index3 path2node;

    /* Node index */
    Index3 idx;

	  /* Exact beginning index */
	  unsigned int beg;

    /* Number of points in the node */
	  unsigned int num;

	  /* Node's own vector of indices */
	  vector<unsigned int> vecid;

    /* Nodes in the U-List */
	  vector<int> Unodes;
	  /* Nodes in the V-List */
	  vector<int> Vnodes;
	  /* Nodes in the W-List */
	  vector<int> Wnodes;
	  /* Nodes in the X-List */
	  vector<int> Xnodes;

    /* Bounding object for this node */
    Boundary bounds;

    NodeTree() {}

    NodeTree (int p, int c, Index3 t, Index3 i, int d):
		  parent(p), child(c), depth(d), path2node(t), beg(0), num(0), vecid(0) {}
    NodeTree (int p, int c, Index3 t, Index3 i, int d, Boundary b):
  		 parent(p), child(c), depth(d), path2node(t), beg(0), num(0), vecid(0), bounds(b) {}

    /* Helper functions for information retrieval */
    bool is_root() const {
      return parent == -1;
    }
    bool is_leaf() const {
      return child == -1;
    }
    unsigned int begin() const {
      return beg;
    }
    unsigned int end() const {
      return beg + num;
    }
    unsigned int size() const {
      return num;
    }
    size_t num_child() const { return 8; }

    unsigned index() const {
      return nodeid;
    }

    /* Cache the furthest distance from centroid of the node
     * to any descentant points,
     * This field is useful in calculating a tighter bound
     */
    real_t maxdist;

    /* Return minimum distance to another point */
    real_t min_distance(const Point& point) {
      return bounds.min_distance(point);
    }
    real_t min_distance_column_major(const Point& point) {
      return bounds.min_distance_column_major(point);
    }

     real_t min_distance(Vec& point) {
      return bounds.min_distance(point);
    }
    /* Return minimum distance to another node */
    real_t min_distance(const NodeTree& box) {
      return bounds.min_distance(box);
    }

    real_t min_distance(const Points_t& box, int i) {
      return bounds.min_distance(box,i);
    }

    /* Returns the point with  minimum distance to another node */
    /*real_t min_distance(const NodeTree& box) {
      return bounds.min_point(box);
    }
    */
    
    /* Return the point with  minimum distance to another point */
    Vec min_point(const Point& point) {
      return bounds.min_point(point);
    }

    /* Return the point with  maximum distance to another point */
    Vec max_point(const Point& point) {
      return bounds.max_point(point);
    }

    /* Return the point with  minimum distance to another box */
    Vec min_point(NodeTree& box) {
      return bounds.min_point(box);
    }

    /* Return the point with  maximum distance to another box */
    Vec max_point(NodeTree& box) {
      return bounds.max_point(box);
    }
    /* Return the center vector of a boundary box */
    Vec center() {
      return bounds.center;
    }

    /* Return minimum incremental distance to another point */
    real_t min_distance(const Point& point, const NodeTree& box, const NodeTree& parent) {
      return bounds.min_distance(point, box, parent);
    }

    /* Return minimum and maximum distance to another point */
    Range range_distance(const Point& point) {
      return bounds.range_distance(point);
    }

    real_t max_distance(Vec& point) {
      return bounds.max_distance(point);
    }
    /* Return minimum and maximum distance to another node */
    Range range_distance(const NodeTree& box) {
      return bounds.range_distance(box);
   }

   real_t max_distance(const NodeTree& box) {
      return bounds.max_distance(box);
   }

  vector<double> boundary_distances(const NodeTree& box) {
    return bounds.boundary_distances(box);
  }

  vector<Vec> boundary_points(const NodeTree& box) {
    return bounds.boundary_points(box);
  }

  };

  /* Vector of all nodes */
  vector<NodeTree> node_data;

  /* Original dataset */
  Points_t& data;

  /* Permuted dataset */
  Points_t& data_perm;

  std::vector<unsigned> Index;

  /* Centers for each node */
  // Points_t& centers;

  /* Comptes centers for each node considering the mass */
  void centroids(NodeTree& s);


  vector<int> level;

  // OTree(Points_t& d, Points_t& o, Points_t& c) : node_data(0), data(d), data_perm(o), centers(c), level(0) {}
  // OTree(Points_t& d, Points_t& o) : node_data(0), data(d), data_perm(o), centers(o), level(0) {}
  // OTree(Points_t& d, Points_t& o) : node_data(0), data(d), data_perm(o), level(0) {}
  OTree(Points_t& d, Points_t& o) : data(d), data_perm(o), level(0) {}

  /* Build tree */
  int build_tree ();
  int build_octree ();
  /* Return the root node of this tree */
  NodeTree& root() {
    return node_data[0];
  }

  /* The number of points contained in this tree */
  inline unsigned points() const {
    return data.size();
  }

  /* The number of nodes contained in this tree */
  inline unsigned nodes() const {
    return node_data.size();
  }

  /* Return the number of nodes a parent node is split into.
   * For octree, this is 8
   */
   size_t num_child() const { return 8; }


  /* Helper functions */
  int pow2 (int l);
  int child (int nodeId, const Index3& idx);
  Point3  center (int nodeId);
  Point3  center_p (int nodeId);
  real_t radius (int nodeId);
  real_t radius();
  bool adjacent (int a, int b);
  int findgnt(int wntdepth, const Index3& wntpath);
  int order_topdown (vector<int>& boxes);
  int order_bottomup (vector<int>& boxes);
  int compute_lists (int nodeId);
};

/* Template instantiation */
#include "Octree.cpp"
#endif
