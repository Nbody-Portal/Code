/**
 *  Defines a binary tree
 */

#ifndef INC_BINARY_TREE_H
#define INC_BINARY_TREE_H

#include <vector>
#include "fixed_vector.h"
#include "range.h"

#include "vector_of_array.hpp"

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif

typedef vector_of_array<real_t> Points_t;
typedef typename Points_t::array_type Point;

typedef fixed_vector<real_t> Vec;
typedef vector_of_array<real_t> Mat;

#include <stddef.h>
using namespace std;

enum {
  LET_SRCNODE = 1,
  LET_TRGNODE = 2
};

enum {
  LO = 0,
  HI = 1
};

/* Class for wrapper around tree structure */
template <typename Boundary>
class Tree {
public:
  /* Tree structure */
  class NodeTree {
    friend class Tree;

    /* Node index */
    unsigned int nodeid;

  public:
    int parent, child, depth;

    /* Denotes whether it's the left or right child */
    int idx;

    /* Exact beginning index */
    unsigned int beg;

    /* Number of points in the node */
    unsigned int num;

    /* Level of the node in the tree */
    unsigned int level;

    /* Value used to split the hyperspace in two */
    real_t split_value;

    /* Index of the current dimension being split */
    unsigned int split_dim;

    /* Bounding object for this node */
    Boundary bounds;

    NodeTree() {}

    NodeTree (int p, int c, int d, int i, Boundary b) : nodeid(0), parent(p), child(c), depth(d), idx(i), beg(0), num(0), level(0), split_value(0.0), split_dim(0), bounds(b), maxdist(0) {};

    unsigned index() const {
      return nodeid;
    }

    /* Helper functions for information retrieval */
    bool is_root() const {
      return parent == -1;
    }
    bool is_leaf() const {
      return child == -1;
    }
    unsigned int begin() const {
      return beg;
    }
    unsigned int end() const {
      return beg + num;
    }
    unsigned int size() const {
      return num;
    }


    /* Cache the furthest distance from centroid of the node
     * to any descentant points,
     * This field is useful in calculating a tighter bound
     */
    real_t maxdist;

    /* Return minimum distance to another point */
    real_t min_distance(const Point& point) {
      return bounds.min_distance(point);
    }
    real_t min_distance_column_major(const Point& point) {
      return bounds.min_distance_column_major(point);
    }

     real_t min_distance(Vec& point) {
      return bounds.min_distance(point);
    }
    /* Return minimum distance to another node */
    real_t min_distance(const NodeTree& box) {
      return bounds.min_distance(box);
    }

    real_t min_distance(const Points_t& box, int i) {
      return bounds.min_distance(box,i);
    }

    /* Returns the point with  minimum distance to another node */
    /*real_t min_distance(const NodeTree& box) {
      return bounds.min_point(box);
    }
    */
    /* Return the point with  minimum distance to another point */
    Vec min_point(const Point& point) {
      return bounds.min_point(point);
    }

    /* Return the point with  maximum distance to another point */
    Vec max_point(const Point& point) {
      return bounds.max_point(point);
    }

    /* Return the point with  minimum distance to another box */
    Vec min_point(NodeTree& box) {
      return bounds.min_point(box);
    }

    /* Return the point with  maximum distance to another box */
    Vec max_point(NodeTree& box) {
      return bounds.max_point(box);
    }
    /* Return the center vector of a boundary box */
    Vec center() {
      return bounds.center;
    }

    /* Return minimum incremental distance to another point */
    real_t min_distance(const Point& point, const NodeTree& box, const NodeTree& parent) {
      return bounds.min_distance(point, box, parent);
    }

    /* Return minimum and maximum distance to another point */
    Range range_distance(const Point& point) {
      return bounds.range_distance(point);
    }

    real_t max_distance(Vec& point) {
      return bounds.max_distance(point);
    }
    /* Return minimum and maximum distance to another node */
    Range range_distance(const NodeTree& box) {
      return bounds.range_distance(box);
   }

   real_t max_distance(const NodeTree& box) {
      return bounds.max_distance(box);
   }

  vector<double> boundary_distances(const NodeTree& box) {
    return bounds.boundary_distances(box);
  }

  vector<Vec> boundary_points(const NodeTree& box) {
    return bounds.boundary_points(box);
  }


   size_t num_child() const { return 2; }
  };

  /* Vector of all tree nodes */
  vector<NodeTree> node_data;

  /* Original dataset */
  Points_t& data;

  /* Permuted dataset */
  Points_t& data_perm;

  /* Centers for each node */
  Points_t& centers;

  /* Comptes centers for each node*/
  void centroids(NodeTree& s);

  /* Compute maxdist for each node */
  void compute_maxdist(NodeTree& s);

  // Permutation: index[i] is the permuted idx of originally ith point
  // TODO: rename permute_ or such
  std::vector<unsigned> index;
  std::vector<int> numbers;
  int leaves = 0;
  Points_t* o =  new Points_t();

  // Tree(Points_t& o, Points_t& n) : data(o), data_perm(n), centers(n) {};
  Tree():data(*o), data_perm(*o), centers(*o){};

  Tree(Points_t& o, Points_t& n) : data(o), data_perm(n), centers(n) {};

  // Tree& operator=(Tree& other){
  //   this->data = other.data;
  //   this->data_perm  = other.data_perm;
  //   this->centers = other.centers;
  //   return *this;
  // }

  /* Build kd-tree */
  int build_kdtree();
  int build_kdtree_tight();

  int build_kdtree_midpoint();
  int build_kdtree_midpoint_tight();
  int build_kdtree_column_major ();

  /* Build ball-tree */
  int build_balltree ();

  /* Splits the current node into two */
  int split_node (int k);
  int mid_split_node (int k);
  int split_node_column_major (int k);
  int mid_split_node_column_major (int k);
  int mid_split_node_tight (int k);
  int median_split_node (int k);
  int mean_split_node (int k);

  /* Return the dimension to split on */
  int get_split_dim (int k);
  int get_split_dim_column_major (int k);

  /* The number of nodes contained in this tree */
  inline unsigned nodes() const {
    return node_data.size();
  }

  /* The number of points contained in this tree */
  inline unsigned points() const {
    return data.size();
  }

  inline unsigned points_column_major() const {
    return data[0].size();
  }

  /* Return the root node of this tree */
  NodeTree& root() {
    return node_data[0];
  }

  /* Return the number of nodes a parent node is split into.
   * For binary tree, this is 2
   */
   size_t num_child() const { return 2; }
};


// TODO: Use the output stream
template <typename T>
std::ostream& operator<<(std::ostream& s, const Tree<T>& tree) {
  auto& ntree = tree.nodes;
  for (size_t i = 0; i < ntree.size(); ++i) {
    fprintf (stderr, "node: %d  beg: %d  num: %d child: %d\n", i, ntree[i].beg, ntree[i].num, ntree[i].child);
    if (ntree[i].child == -1) {
      for (int j = ntree[i].beg; j < ntree[i].beg+ntree[i].num; ++j) {
        fprintf (stderr, "%d --> ", j);
        for (int k = 0; k < tree.data[j].size(); ++k)
          fprintf (stderr, "%lf \t", tree.data[j][k]);
        fprintf (stderr, "\n");
      }
    }
  }
  return s;
}


struct CompPoint {
  const int i;

  CompPoint(int idx) : i(idx) {}
  bool operator()(const Point& a, const Point& b) const {
    return a[i] < b[i];
  }
};

struct CompIdx {
  const int i;
  const Points_t& s;

 public:
  CompIdx(int idx, const Points_t& src) : i(idx), s(src) {}
  bool operator()(const int a, const int b) const {
    if ((a < s.size()) && (b < s.size()))
      return s[a][i] < s[b][i];
    return false;
  }
};

struct CompIdx_column_major {
  const int i;
  const Points_t& s;

 public:
  CompIdx_column_major(int idx, const Points_t& src) : i(idx), s(src) {}
  bool operator()(const int a, const int b) const {
    // if ((a < s[0].size()) && (b < s[0].size()))
      return s[i][a] < s[i][b];
    return false;
  }
};

/* Template instantiation */
#include "Binary_tree.cpp"

#endif
