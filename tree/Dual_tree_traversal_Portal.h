#ifndef _DUAL_TREE_TRAVERSAL_H_
#define _DUAL_TREE_TRAVERSAL_H_
#define counter_type unsigned long long

#ifdef _DEBUG_PAR
#define DEBUG_CMD(cmd) do { cmd} while( false )
#else
#define DEBUG_CMD(cmd) do { } while ( false )
#endif


#include <cfloat>
#include <omp.h>
#include <NBodyLower.h>
#include "IRNode.h"
#include "Binary_tree.h"


/*
* This class is the general version for Dual_tree_traversal_Cover.h and
* after performance tests we will eliminte that file and only use this
* instead. (This one is compatible with portal too)
*/


template <typename Tree, typename Base, typename pruneGenerator>
class DualTreeTraversal {
 public:
  typedef typename Tree::NodeTree Box;
  Tree& src_tree;
  Tree& trg_tree;

  Base& base;
  pruneGenerator prune;
  real_t temp = -1;
  int task_level;
  counter_type num_prunes = 0;
  counter_type num_bases = 0;

  #ifdef _DEBUG_PAR
    counter_type num_basecases = 0;
    counter_type num_basecase[32];
    counter_type num_pruneEl[32];
  #endif

  /* Convenience constructor */
  DualTreeTraversal(Tree& st, Tree& tt, Base&  N, pruneGenerator& pg, counter_type& num, int levelT = 10)
      : src_tree(st), trg_tree(tt), base(N), prune(pg), num_prunes(num), task_level(levelT){
        #ifdef _DEBUG_PAR
          for (size_t i= 0; i < 32; i++ ) {
            num_basecase[i] = 0;
            num_pruneEl[i] = 0;
          }
        #endif
  }

  /* Traverse the tree checking hypersphere-hyperrectangle intersections to discard regions of the space */
  void traverse (Box& s, Box& t) ;

 private:
  /* Implementation helper function */
  void traverse_impl(Box& s, Box& t, unsigned level) ;
};

/**
 * Traverse the tree while discarding regions of space
 * with hypersphere-hyperrectange (general bounds) intersections.
 */
template <typename Tree, typename Base, typename pruneGenerator>
void
DualTreeTraversal<Tree, Base, pruneGenerator>::traverse (Box& s, Box& t)  {

 /* to enable nested task parallelism in the tree travrsal computation*/

  #pragma omp parallel
  {
    #pragma omp single nowait
    {
       traverse_impl(s, t, 0);
    }
  }
  #ifdef _DEBUG_PAR
    for (size_t i = 0; i < 32; i++) {
       num_basecases += num_basecase[i];
       // cout  << "basecase " << i << " : " << (num_basecase[i]) << "\n" ;
    }
    for (size_t i = 0; i < 32; i++) {
        num_prunes += num_pruneEl[i];
        // cout  << "pruneNodes " << i << " : " << (num_pruneEl[i]) << "\n" ;
    }
    // cout << "Total base cases: " << num_basecases << "\n";
    cout << "Total base cases: " << num_bases << "\n";
    cout << "Total prunes: " << num_prunes << "\n";

  #endif


}

template <typename Tree, typename Base, typename pruneGenerator>
void
DualTreeTraversal<Tree, Base, pruneGenerator>::traverse_impl(Box& s, Box& t,
                                             unsigned level)  {

  int a;
  // if (!prune.prune_subtree(s, t)) {
  if(true){
  /* If both source and target nodes are leaves, evaluate the base case */
  cout << "traversal 1 \n";
  if (s.is_leaf() && t.is_leaf()) {
    cout << "traversal 2 \n";
      base.base_case(s,t);
      cout << "traversal 3 \n";
      a = t.child + t.num_child();
      temp = base.get_temp(t);
      prune.set_temp_node(t,temp);
      // num_bases +=s.size() * t.size();
      #ifdef _DEBUG_PAR
        // num_basecase[omp_get_thread_num()] += s.size() * t.size();
      #endif
  }
  #if 0
  if (s.is_leaf() && t.is_leaf()) {
    for (size_t j = t.begin(); j < t.end(); ++j) {
        if (!prune.prune_subtree(s,j)){
              base.base_case(s,j);
              temp = base.get_temp(j);
              prune.set_temp(temp);
              num_bases +=s.size();
              #ifdef _DEBUG_PAR
                num_basecase[omp_get_thread_num()] += s.size() * t.size();
              #endif
        }
      }
    }
   #endif
    /* Recurse down the target node. Recursion order does not matter. */
    else if (s.is_leaf() && !t.is_leaf()) {
      cout << "traversal 4 \n";
      int child_id = t.child;
      for (size_t i = 0; i < t.num_child(); i++) {
        child_id = t.child + i;
        traverse_impl(s, trg_tree.node_data[child_id], level+1);
      }
    }

    /* Recurse down the source node. Recursion order does matter. */
    else if (!s.is_leaf() && t.is_leaf()) {
      cout << "traversal 5 \n";
      /* Decide the visiting order of branches */
      std::vector<int> visit_order = prune.visit(s, t);
      int child_id = s.child;
      for (size_t i = 0; i < visit_order.size(); i++ ) {
        child_id =  s.child + visit_order[i];
        traverse_impl(src_tree.node_data[child_id], t, level+1);
      }
    }

        /* We have to recurse down both source and target nodes. */
    else {
      cout << "traversal 6 \n";
      for (int i = 0; i < t.num_child(); i++) {
        if (level < task_level ){
          #pragma omp task shared(s,t) untied
          {
            int trg_child_id = t.child + i;
            vector<int> visit_order = prune.visit(s,
                                          trg_tree.node_data[trg_child_id]);
            for (int j = 0; j < visit_order.size(); j++) {
              int src_child_id = s.child + visit_order[j];
              traverse_impl(src_tree.node_data[src_child_id],
                                      trg_tree.node_data[trg_child_id], level+1);
            }
          }
        }else {

          int trg_child_id = t.child + i;
          vector<int> visit_order = prune.visit(s,
                                        trg_tree.node_data[trg_child_id]);
          for (int j = 0; j < visit_order.size(); j++) {
            int src_child_id = s.child + visit_order[j];
            traverse_impl(src_tree.node_data[src_child_id],
                                    trg_tree.node_data[trg_child_id], level+1);
          }
        }
      }
  }
  } else {
    cout << "traversal 7 \n";
      prune.centroid_case(s, t);
      base.adjustPartitionCounter(1 , t.begin() , t.end() , s.size());
      // num_prunes += s.size() * t.size();
      #ifdef _DEBUG_PAR
        // num_pruneEl[omp_get_thread_num()] += s.size() * t.size();
      #endif
  }
  cout << "traversal 7 \n";
}

#endif
