#ifndef _MULTI_TREE_TRAVERSAL_H_
#define _MULTI_TREE_TRAVERSAL_H_
#define counter_type unsigned long long



#ifdef _DEBUG_PAR
#define DEBUG_CMD(cmd) do { cmd} while( false )
#else
#define DEBUG_CMD(cmd) do { } while ( false )
#endif

#include <cfloat>
#include <omp.h>


template <typename Tree, typename Rule, typename pruneGenerator>
class MultiTreeTraversal {
 public:
  typedef typename Tree::NodeTree Box;
  std::vector<Tree*>& tree_set;

  Rule& rules;
  pruneGenerator prune;
  int task_level;
  counter_type num_prunes = 0;

  #ifdef _DEBUG_PAR
    counter_type num_basecases = 0;
    counter_type num_basecase[32];
    counter_type num_pruneEl[32];
  #endif

  /* Convenience constructor */
  MultiTreeTraversal(std::vector<Tree*>& trees, Rule& r, pruneGenerator& pg, counter_type& num, int level = 4)
      : tree_set(trees), rules(r), num_prunes(num),  prune(pg), task_level(level) {

        #ifdef _DEBUG_PAR
          for (size_t i= 0; i < 32; i++ ) {
            num_basecase[i] = 0;
            num_pruneEl[i] = 0;
          }
        #endif
  }
  /* Traverse the tree checking hypersphere-hyperrectangle intersections to discard regions of the space */
  void traverse () ;

 private:
  /* Implementation helper function */
  void traverse_impl(std::vector<Box*> &box_set, unsigned level) ;
};

/**
 * Traverse the tree while discarding regions of space
 * with hypersphere-hyperrectange intersections.
 */
template <typename Tree, typename Rule, typename pruneGenerator>
void
MultiTreeTraversal<Tree, Rule, pruneGenerator>::traverse()  {

  vector<Box*> box_set;

  for (size_t i = 0 ; i < tree_set.size(); i++) {
    box_set.push_back(&tree_set[i]->root());
  }

  /* to enable nested task parallelism in the tree travrsal computation*/
  omp_set_nested(1);

  #pragma omp parallel
  {
    #pragma omp single nowait
    {
      traverse_impl(box_set, 0);
    }
  }
  #ifdef _DEBUG_PAR
    for (size_t i = 0; i < 32; i++) {
       num_basecases += num_basecase[i];
       cout  << "basecase " << i << " : " << (num_basecase[i]) << "\n" ;
    }
    for (size_t i = 0; i < 32; i++) {
        num_prunes += num_pruneEl[i];
        cout  << "pruneNodes " << i << " : " << (num_pruneEl[i]) << "\n" ;
    }
    cout << "Total base cases: " << num_basecases << "\n";
    cout << "Total prunes: " << num_prunes << "\n";
  #endif
}


/* implementation of traversal*/
template <typename Tree, typename Rule, typename pruneGenerator>
void
MultiTreeTraversal<Tree, Rule, pruneGenerator>::traverse_impl(std::vector<Box*> &box_set,
                                             unsigned level)  {

  counter_type prune = 0;
  if (!rules.prune_subtree(box_set)) {
    /* If all the nodes in the node_set are leaves, then, evaluate the base case */
    bool base_case = true;
    int  divide_node = -1;
    for (size_t i = 0; i < box_set.size(); i++)
      if (!box_set[i]->is_leaf()) {
        base_case = false;
        divide_node = i;
        // break;
      }
    if (base_case) {
      rules.base_case (box_set);
      #ifdef _DEBUG_PAR
        counter_type base_size = 1;
        for(size_t i = 0; i < box_set.size(); i++)
           base_size *= box_set[i]->size();

        num_basecase[omp_get_thread_num()] += base_size;
      #endif
    }
    else {

     /* Recurse down the target node. Recursion order does not matter. */
        int child_id;
        std::vector<Box*> child_node_set = box_set;
        std::vector<int> visit_order = rules.visit(box_set, divide_node);
        for (int i = 0; i < visit_order.size(); ++i) {
          child_id = box_set[divide_node]->child + visit_order[i];
          child_node_set[divide_node] = &tree_set[divide_node]->node_data[child_id];
          if ((level < task_level) && (divide_node == box_set.size()-1)) {
            #pragma omp task shared(box_set) untied
            {
              traverse_impl(child_node_set, level+1);
            }
          }else{
            traverse_impl(child_node_set, level+1);
          }
        }
        #pragma omp taskwait
    }
  }
  else {
    rules.centroid_case(box_set);
    #ifdef _DEBUG_PAR
      counter_type prune_size = 1;
      for(size_t i = 0; i < box_set.size(); i++)
         prune_size *= box_set[i]->size();

      num_pruneEl[omp_get_thread_num()] += prune_size;
    #endif

  }

}

#endif
