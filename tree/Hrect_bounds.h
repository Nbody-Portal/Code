/**
 *  Defines a hyperrectangle bounding box
 */

#ifndef INC_HRECT_BOUNDS_H
#define INC_HRECT_BOUNDS_H

#include "fixed_vector.h"
#include "range.h"

#include "Metric.h"

#include "vector_of_array.hpp"

#if !defined (USE_FLOAT)
/** \brief Floating-point type for a real number. */
typedef double real_t;
#else
/** \brief Floating-point type for a real number. */
typedef float real_t;
#endif

typedef vector_of_array<real_t> Points;
typedef typename Points::array_type Point;

typedef fixed_vector<real_t> Vec;
typedef vector_of_array<real_t> Mat;



class Hrect;

typedef Tree<Hrect>::NodeTree Box;

/* Class for hyperrectange represented by two points,
 * one for the lower bound and other for the upper
 * bound along each dimension.
 */
class Hrect {
  public:
    unsigned int dim;

    Vec lo;
    Vec hi;
    Vec lo_i;
    Vec hi_i;
    Vec center;
    /* Default constructor */
    Hrect () {}

    /* Initalizes to the specified dimension */
    //Hrect (unsigned int d) : dim(d), lo(d), hi(d) { }
    Hrect (unsigned int d) : dim(d), lo(d), hi(d), lo_i(d), hi_i(d), center(d) { }

    /* Computes the bounds of the hyperrectangle enclosing the dataset */
    int bounding_box (const Points_t& data);
    int bounding_box_column_major (const Points_t& data);

    /* Compute minimum box-to-point squared distance */
    real_t min_distance (const Point& point);
    real_t min_distance (const Points_t& point, int i);
    real_t min_distance (Vec& point);

    /* Finds the point that have min and max distance in the hyper rectangle from the point*/
    Vec min_point (const Point& point);
    Vec max_point (const Point& point);

    /* Finds the point that have min and max distance in the hyper rectangle from the box*/
    Vec min_point (Box& box);
    Vec max_point (Box& box);

    /* Compute minimum box-to-box squared distance */
    real_t min_distance (const Box& box);

    /* Compute minimum and maximum box-to-point squared distance */
    Range range_distance (const Point& point);
    real_t max_distance (Vec& point);

    /* Compute minimum and maximum box-to-box squared distance */
    Range range_distance (const Box& box);
    real_t max_distance (const Box& box);

    /* Compute minimum incremental box-to-point squared distance */
    real_t min_distance (const Point& point, const Box& box, const Box& parent);
    /* Returns the width of the hyperrectange for a particular dimension */
    real_t width (unsigned int d) { return hi[d] - lo[d]; }

    /* Calculate the distance between two points */
    real_t distance_between_points (const Point& point1, const Point& point2);

    /* Vector of distances */
    vector<double> boundary_distances(const Box& box);
    vector<Vec> boundary_points(const Box& box);

};

/**
 * Compute the bounds of the rectangle enclosing the dataset
 */
inline int
Hrect::bounding_box (const Points_t& data) {
  for (size_t d = 0; d < dim; d++) {
    real_t bound_lo = data[0][d];
    real_t bound_hi = data[0][d];
    center[d] = 0;
    for (size_t i = 1; i < data.size(); ++i) {
      if (data[i][d] < bound_lo) {
        bound_lo = data[i][d];
	      lo_i[d] = i;
      }
      if (data[i][d] > bound_hi) {
        bound_hi = data[i][d];
	      hi_i[d] = i;
      }
     center[d] += data[i][d];
    }
    lo[d] = bound_lo;
    hi[d] = bound_hi;
   center[d] /= data.size();
  }
  return 0;
}

inline int
Hrect::bounding_box_column_major (const Points_t& data) {
  for (size_t d = 0; d < data.size(); d++) {
    real_t bound_lo = data[d][0];
    real_t bound_hi = data[d][0];
    center[d] = 0;
    for (size_t i = 1; i < data[0].size(); ++i) {
      if (data[d][i] < bound_lo) {
        bound_lo = data[d][i];
	      lo_i[d] = i;
      }
      if (data[d][i] > bound_hi) {
        bound_hi = data[d][i];
	      hi_i[d] = i;
      }
     center[d] += data[d][i];
    }
    lo[d] = bound_lo;
    hi[d] = bound_hi;
   center[d] /= data[0].size();
  }
  return 0;
}

/**
 * Compute minimum box-to-point squared distance
 */
inline real_t
Hrect::min_distance (const Point& point) {
  real_t lower, higher, box_diff;
  real_t sum = 0;
  for (size_t d = 0; d < dim; d++) {
    lower = lo[d] - point[d];
    higher = point[d] - hi[d];
    box_diff = pow(((lower + fabs(lower)) + (higher + fabs(higher)))/2, 2);
    sum += box_diff;
  }
  return sum;
}
inline real_t
Hrect::min_distance (const Points_t& points, int i) {
  real_t lower, higher, box_diff;
  real_t sum = 0;
  for (size_t d = 0; d < dim; d++) {
    lower = lo[d] - points[d][i];
    higher = points[d][i] - hi[d];
    box_diff = pow(((lower + fabs(lower)) + (higher + fabs(higher)))/2, 2);
    sum += box_diff;
  }
  return sum;
}

inline real_t
Hrect::min_distance (Vec& point) {
  real_t lower, higher, box_diff;
  real_t sum = 0;
  for (size_t d = 0; d < dim; d++) {
    lower = lo[d] - point[d];
    higher = point[d] - hi[d];
    box_diff = pow(((lower + fabs(lower)) + (higher + fabs(higher)))/2, 2);
    sum += box_diff;
  }
  return sum;
}

inline vector<double>
Hrect::boundary_distances(const Box& box) {
  vector<double> bounds_temp;
  SquaredEuclideanMetric<Vec, Vec> metric;
  int boundary = pow(2,dim);
  for (size_t i = 0 ; i < boundary; i++) {
    Vec temp_point(dim);
    int decimal_number = i;
    for (size_t k = 0; k < dim; ++k) {
      temp_point[k] = (decimal_number %2 ==0) ? lo[k]: hi[k];
      decimal_number /= 2;
    }
       for ( int j = 0; j < boundary ; j++ ) {
         Vec temp_point_inner(dim);
         int decimal_number_inner = j ;
         double distance = 0;
         for (size_t kk = 0; kk < dim; ++kk) {
           temp_point_inner[kk] = (decimal_number_inner %2 ==0) ? box.bounds.lo[kk]: box.bounds.hi[kk];
           decimal_number_inner /= 2;
      //     distance +=  (temp_point[kk]- temp_point_inner[kk]) * (temp_point[kk]- temp_point_inner[kk]);
         }
         distance =  metric.compute_distance(temp_point, temp_point_inner);
         bounds_temp.push_back(distance);

      }
     }
     return bounds_temp;
}

inline vector<Vec>
Hrect::boundary_points(const Box& box) {
  vector<Vec> bounds_point;
  int boundary = pow(2,dim);
  for (size_t i = 0 ; i < boundary; i++) {
    Vec temp_point(dim);
    int decimal_number = i;
    for (size_t k = 0; k < dim; ++k) {
      temp_point[k] = (decimal_number %2 ==0) ? lo[k]: hi[k];
      decimal_number /= 2;
    }
    bounds_point.push_back(temp_point);
  }
  return bounds_point;
}
inline Vec
Hrect::min_point (const Point& point) {
  Vec p(dim);
  real_t a, b;
  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - point[d];
    b = point[d] - hi[d];
    if (a >= 0) { // point is lesser than the lower bownd so we chooce the lower bound as the closest point
      p[d] = lo[d];
    }
    else {
     if (b >= 0) { // point is greater than the higher bound so we choose the higher point as the closer point
       p[d] = hi[d];
     }
     else {
       p[d] = point[d];
     }
    }
  }
  return p;
}

inline Vec
Hrect::min_point (Box& box) {
  Vec p(dim);
  real_t a, b;
  Vec center = box.center();
  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - center[d];
    b = center[d] - hi[d];
    if (a >= 0) { // point is lesser than the lower bownd so we chooce the lower bound as the closest point
      p[d] = lo[d];
    }
    else {
     if (b >= 0) { // point is greater than the higher bound so we choose the higher point as the closer point
       p[d] = hi[d];
     }
     else {
       p[d] = center[d];
     }
    }
  }
  return p;
}

inline Vec
Hrect::max_point (const Point& point) {
  Vec p(dim);
  real_t a, b;
  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - point[d];
    b = point[d] - hi[d];
    if (a >= 0) { // point is lesser than the lower bownd so we chooce the highrt bound as the farthest point
      p[d] = hi[d];
    }
    else {
     if (b >= 0) { // point is greater than the higher bound so we choose the lower point as the farthestr point
       p[d] = lo[d];
     }
     else {
       p[d] = fabs(a) > fabs(b) ?  lo[d] : hi[d];
     }
    }
  }
  return p;
}

inline Vec
Hrect::max_point (Box& box) {
  Vec p(dim);
  real_t a, b;
  Vec center = box.center();
  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - center[d];
    b = center[d] - hi[d];
    if (a >= 0) { // point is lesser than the lower bownd so we chooce the highrt bound as the farthest point
      p[d] = hi[d];
    }
    else {
     if (b >= 0) { // point is greater than the higher bound so we choose the lower point as the farthestr point
       p[d] = lo[d];
     }
     else {
       p[d] = fabs(a) > fabs(b) ?  lo[d] : hi[d];
     }
    }
  }
  return p;
}
/**
 * Compute minimum box-to-box squared distance
 */
inline real_t
Hrect::min_distance (const Box& box) {
  real_t lower, higher, box_diff;
  real_t sum = 0;

  // cout << " box: " << box.index() << " , \n";
  for (size_t d = 0; d < dim; d++) {
    // cout << d << " , " << box.bounds.lo[d] << " , " << hi[d] << "\n";
    lower = box.bounds.lo[d] - hi[d];
    // lower = box.bounds.lo[d];
    higher = lo[d] - box.bounds.hi[d];
    // higher = box.bounds.hi[d];
    box_diff = pow(((lower + fabs(lower)) + (higher + fabs(higher)))/2, 2);
    sum += box_diff;
  }
  // cout << "end: " << box.bounds.lo[0] << " \n";
  // cout << hi[0] << "\n";
  return sum;
}

/**
 * Compute minimum and maximum box-to-point squared distance
 */
inline Range
Hrect::range_distance (const Point& point) {
  real_t a, b; //, lower, higher;
  real_t lo_sum = 0;
  real_t hi_sum = 0;

  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - point[d];
    b = point[d] - hi[d];
    lo_sum += pow(((a + fabs(a)) + (b + fabs(b)))/2, 2);
    hi_sum += pow(fmax(fabs(a), fabs(b)), 2);
/*
    if (a >= 0) { // point is lesser than lower bound
      lower = a;
      higher = -b;
    }
    else {
      if (b >= 0) { // point is greater than higher bound
        lower = b;
        higher = -a;
      }
      else { // point is between the lower and higher bound
        lower = 0;
        higher = -std::min(a, b);
      }
    }
    lo_sum += lower * lower;
    hi_sum += higher * higher;
*/
  }
  Range sum(lo_sum, hi_sum);

  return sum;
}

inline real_t
Hrect::max_distance (Vec& point) {
  real_t a, b; //, lower, higher;
  real_t hi_sum = 0;
  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - point[d];
    b = point[d] - hi[d];
    hi_sum += pow(fmax(fabs(a), fabs(b)), 2);
    }

  return hi_sum;
}



/**
 * Compute minimum and maximum box-to-box squared distance
 */
inline Range
Hrect::range_distance (const Box& box) {
  real_t a, b, lower, higher;
  real_t lo_sum = 0;
  real_t hi_sum = 0;

  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - box.bounds.hi[d];
    b = box.bounds.lo[d] - hi[d];
    if (a >= b) {
      lower = (a > 0) ? a : 0;
      higher = -b;
    }
    else {
      lower = (b > 0) ? b : 0;
      higher = -a;
    }
    lo_sum += lower * lower;
    hi_sum += higher * higher;
  }
  Range sum(lo_sum, hi_sum);

  return sum;
}


inline real_t
Hrect::max_distance (const Box& box) {
  real_t a, b, lower, higher;
  real_t hi_sum = 0;

  for (size_t d = 0; d < dim; d++) {
    a = lo[d] - box.bounds.hi[d];
    b = box.bounds.lo[d] - hi[d];
    if (a >= b) {
      higher = -b;
    }
    else {
      higher = -a;
    }
    hi_sum += higher * higher;
  }

  return hi_sum;
}


inline real_t
Hrect::min_distance (const Point& point, const Box& box, const Box& parent) {
  /* Get the splitting dimension */
  int parent_dim = parent.split_dim;

  /* Target point at the splitting dimension */
  real_t target_point = point[parent_dim];

  /* Check if the current branch modifies the bounding hyperrectangle */
  real_t offset = target_point - parent.split_value;
  if ((box.idx == 0 && !(offset > 0)) || (box.idx == 1 && !(offset < 0))) {
    return 0;
  }

  /* Perform incremental update */
  real_t sum = 0;

/*  real_t lower, higher, box_diff;

  lower = lo[parent_dim] - target_point;
  higher = target_point - hi[parent_dim];
  box_diff = (lower + fabs(lower)) + (higher + fabs(higher));
*/
  real_t box_diff;
  if (offset < 0) {
    box_diff = lo[parent_dim] - target_point;
    if (box_diff < 0)
      box_diff = 0;
  }
  else {
    box_diff = target_point - hi[parent_dim];
    if (box_diff < 0)
      box_diff = 0;
  }
  sum = (offset * offset) - (box_diff * box_diff);
  return sum;
}

/* Calculate the distance between two points */
inline real_t
Hrect::distance_between_points (const Point& point1, const Point& point2) {
  EuclideanMetric<Point, Point> metric;
  real_t dist = metric.compute_distance(point1, point2);
  return dist;
}
#endif
