#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <set>
#include <queue>
#include <math.h>
#include <vector>

using namespace std;

template <typename Boundary>
  int
OTree<Boundary>::build_tree ()
{
  int l = 0;
  int arr_beg = 0;
  int arr_end = 1;
  int arr_count = 0;
  unsigned int dim = 3;
  vector< vector<unsigned int> > vid;
  vector<unsigned int> num;
  vector<NodeTree>& ntree = this->node_data;
  vector<int>& nlevel = this->level;

  /* Get the max number of elements that should be in grouped in any leaf node */
  unsigned int pts_max = getenv__leaf_size();

  /* Push root ntree */
  ntree.push_back (NodeTree (-1, -1, Index3(0,0,0), Index3(0,0,0), 0));
  vid.push_back (vector<unsigned int>());
  vector<unsigned int>& curr_id = vid[0];

  for (size_t k = 0; k < data.size(); k++) {
    // TODO: Add boundary checking
    curr_id.push_back (k);
  }
  num.push_back (curr_id.size());

  nlevel.push_back (arr_beg);
  while (arr_beg < arr_end) {
    arr_count = arr_end;
    for (int k = arr_beg; k < arr_end; k++) {
      ntree[k].nodeid = k;
      ntree[k].num = num[k];
      /* Check if "max" points per box condition is satisfied */
      if (num[k] > pts_max) {
        ntree[k].child = arr_count;
        arr_count = arr_count + pow2 (dim);
        /* Divide the parent ntree/box into 8 child node/boxes */
        for (int a = 0; a < 2; a++)
          for (int b = 0; b < 2; b++)
            for (int c = 0; c < 2; c++) {
              ntree.push_back (NodeTree (k, -1, 2 * ntree[k].path2node + Index3(a,b,c), Index3(a,b,c), ntree[k].depth + 1) );
              vid.push_back (vector<unsigned int>());
              num.push_back (0);
            }
        /** Get the center of the current ntree/box */
        Point3 center_node (center(k));
        /** Determine which child each source point in the parent box belongs to */
        for (auto it = vid[k].begin(); it != vid[k].end(); it++) {
          Index3 idx;
          for (size_t d = 0; d < dim; ++d)
            idx(d) = (data[*it][d] >= center_node(d));
          int child_nodeid = child (k, idx);
          vid[child_nodeid].push_back (*it);
        }
        vid[k].clear();
        /* Get the total number of source points in each ntree/box */
        for (int a = 0; a < 2; a++)
          for (int b = 0; b < 2; b++)
            for (int c = 0; c < 2; c++) {
              int child_nodeid = child (k, Index3(a,b,c));
              num[child_nodeid] = vid[child_nodeid].size();
              ntree[child_nodeid].num = num[child_nodeid];
            }
      }
    }
    l++;
    arr_beg = arr_end;
    arr_end = arr_count;
    nlevel.push_back (arr_beg);
  }

  /** Ordering of the node/boxes, in top-down fashion */
  vector<int> order_boxes;
  order_topdown (order_boxes);


  /* Tag source node/boxes */
  int sum = 0;
  int data_perm_counter = 0;
  for (size_t i = 0; i < order_boxes.size(); i++) {
    int nodeid = order_boxes[i];
    if (num[nodeid] > 0) {
      if(ntree[nodeid].child==-1) {
        ntree[nodeid].beg = sum;
        ntree[nodeid].num = num[nodeid];
        sum += num[nodeid];
        ntree[nodeid].vecid = vid[nodeid];
        for (int j = 0; j < vid[nodeid].size(); j++ ){
          Index.push_back(vid[nodeid][j]);
          data_perm[data_perm_counter++] = data[vid[nodeid][j]];
        }
      }
    }
  }
  /* Calculates the center for each box, considering the objects mass */
  // centroids(this->node_data[0]);
  return 0;
}

template <typename Boundary>
  int
OTree<Boundary>::build_octree ()
{
  int l = 0;
  int arr_beg = 0;
  int arr_end = 1;
  int child_id;
  int arr_count = 0;
  unsigned int dim = 3;
  vector< vector<unsigned int> > vid;
  vector<unsigned int> num;
  vector<NodeTree>& ntree = this->node_data;
  vector<int>& nlevel = this->level;

  /* Get the max number of elements that should be in grouped in any leaf node */
  unsigned int pts_max = getenv__leaf_size();

  /* Compute the bounding box */
  Boundary bounds(dim);
  bounds.bounding_box (data);

  /* Push root ntree */
  ntree.push_back (NodeTree (-1, -1, Index3(0,0,0), Index3(0,0,0), 0, bounds));
  vid.push_back (vector<unsigned int>());
  vector<unsigned int>& curr_id = vid[0];

  for (size_t k = 0; k < data.size(); k++) {
    // TODO: Add boundary checking
    curr_id.push_back (k);
  }
  num.push_back (curr_id.size());

  nlevel.push_back (arr_beg);
  while (arr_beg < arr_end) {
    arr_count = arr_end;
    for (int k = arr_beg; k < arr_end; k++) {
      ntree[k].nodeid = k;
      ntree[k].num = num[k];
      /* Check if "max" points per box condition is satisfied */
      if (num[k] > pts_max) {
        ntree[k].child = arr_count;
        arr_count = arr_count + pow2 (dim);
        /* Divide the parent ntree/box into 8 child node/boxes */
        for (int a = 0; a < 2; a++)
          for (int b = 0; b < 2; b++)
            for (int c = 0; c < 2; c++) {
              ntree.push_back (NodeTree (k, -1, 2 * ntree[k].path2node + Index3(a,b,c), Index3(a,b,c), ntree[k].depth + 1, ntree[k].bounds) );
              child_id = ntree[k].child + a *4 + b*2+c;
              ntree[child_id].bounds.Diameter = ntree[k].bounds.Diameter/2;
              Index3 ind(a,b,c);
              for (int d = 0; d < dim; d++) {
                real_t hi = ntree[k].bounds.hi[d];
                real_t lo = ntree[k].bounds.lo[d];
                real_t diff = hi - lo;
                ntree[child_id].bounds.hi[d] = ind[d] < 1 ? (lo+diff/2): hi;
                ntree[child_id].bounds.lo[d] = ind[d] > 0 ? (lo+diff/2): lo;
              }
              vid.push_back (vector<unsigned int>());
              num.push_back (0);
            }
        /** Get the center of the current ntree/box */
        Point3 center_node (center(k));
        /** Determine which child each source point in the parent box belongs to */
        for (auto it = vid[k].begin(); it != vid[k].end(); it++) {
          Index3 idx;
          for (size_t d = 0; d < dim; ++d)
            idx(d) = (data[*it][d] >= center_node(d));
          int child_nodeid = child (k, idx);
          vid[child_nodeid].push_back (*it);
        }
        vid[k].clear();
        /* Get the total number of source points in each ntree/box */
        for (int a = 0; a < 2; a++)
          for (int b = 0; b < 2; b++)
            for (int c = 0; c < 2; c++) {
              int child_nodeid = child (k, Index3(a,b,c));
              num[child_nodeid] = vid[child_nodeid].size();
              ntree[child_nodeid].num = num[child_nodeid];
            }
      }
    }
    l++;
    arr_beg = arr_end;
    arr_end = arr_count;
    nlevel.push_back (arr_beg);
  }

  /** Ordering of the node/boxes, in top-down fashion */
  vector<int> order_boxes;
  order_topdown (order_boxes);


  /* Tag source node/boxes */
  int sum = 0;
  int data_perm_counter = 0;
  for (size_t i = 0; i < order_boxes.size(); i++) {
    int nodeid = order_boxes[i];
    if (num[nodeid] > 0) {
      if(ntree[nodeid].child==-1) {
        ntree[nodeid].beg = sum;
        ntree[nodeid].num = num[nodeid];
        sum += num[nodeid];
        ntree[nodeid].vecid = vid[nodeid];
        for (int j = 0; j < vid[nodeid].size(); j++ ){
          Index.push_back(vid[nodeid][j]);
          data_perm[data_perm_counter++] = data[vid[nodeid][j]];
        }
      }
    }
  }
  /* Calculates the center for each box, considering the objects mass */
  // centroids(this->node_data[0]);
  return 0;
}

template <typename Boundary>
  int
OTree<Boundary>::compute_lists (int nodeid)
{
  set<int> Uset, Vset, Wset, Xset;
  int curr_node = nodeid;
  vector<NodeTree>& ntree = this->node_data;

  if (ntree[curr_node].parent != -1) {
    int parent_node = ntree[curr_node].parent;
    Index3 min_idx(0);
    Index3 max_idx (pow2 (ntree[curr_node].depth));

    for (int i = -2; i < 4; i++)
      for (int j = -2; j < 4; j++)
        for (int k = -2; k < 4; k++) {
          Index3 try_path (2 * ntree[parent_node].path2node + Index3(i,j,k) );
          if (try_path >= min_idx && try_path <  max_idx && try_path != ntree[curr_node].path2node) {
            int res_node = findgnt (ntree[curr_node].depth, try_path);
            bool adj = adjacent (res_node, curr_node);
            if (ntree[res_node].depth < ntree[curr_node].depth) {
              if (adj) {
                //if (ntree[curr_node].child == -1)
                Uset.insert(res_node);
                //else { ; }
              }
              else {
                Xset.insert(res_node);
              }
            }
            if (ntree[res_node].depth == ntree[curr_node].depth) {
              if (!adj) {
                Index3 bb (ntree[res_node].path2node - ntree[curr_node].path2node);
                assert (bb.linfty() <= 3);
                Vset.insert (res_node);
              }
              else {
                // if (ntree[curr_node].child == -1) {
                queue<int> rest;
                rest.push (res_node);
                while (rest.empty() == false) {
                  int front_node = rest.front();
                  rest.pop();
                  if (adjacent (front_node, curr_node) == false)
                    Wset.insert (front_node);
                  else {
                    // if (ntree[front_node].child == -1)
                    Uset.insert (front_node);
                    if (ntree[curr_node].child == -1)
                      if (ntree[front_node].child != -1) {
                        for (int a = 0; a < 2; a++)
                          for (int b = 0; b < 2; b++)
                            for (int c = 0; c < 2; c++)
                              rest.push (child(front_node, Index3(a,b,c)));
                      }
                  }
              }
              }
            }
          }
        }
  }

  if (ntree[curr_node].child == -1)
    Uset.insert (curr_node);

  for (set<int>::iterator si = Uset.begin(); si != Uset.end(); si++)
    ntree[nodeid].Unodes.push_back (*si);
  for (set<int>::iterator si = Vset.begin(); si != Vset.end(); si++)
    ntree[nodeid].Vnodes.push_back (*si);
  for (set<int>::iterator si = Wset.begin(); si != Wset.end(); si++)
    ntree[nodeid].Wnodes.push_back (*si);
  for (set<int>::iterator si = Xset.begin(); si != Xset.end(); si++)
    ntree[nodeid].Xnodes.push_back (*si);

  return (0);
}

/* Power function:  2^l.  Uses shifts */
template <typename Boundary>
inline
  int
OTree<Boundary>::pow2 (int l)
{
  assert (l >= 0);
  return (1 << l);
}

template <typename Boundary>
inline
  real_t
OTree<Boundary>::radius()
{
  int root_level = 0;
  return pow (2.0, root_level);
}

/* Return the radius of the given ntree */
template <typename Boundary>
inline
  real_t
OTree<Boundary>::radius (int nodeid)
{
  vector<NodeTree>& ntree = this->node_data;
  return radius() / real_t (pow2(ntree[nodeid].depth));
}

/* Return the Center of the given node in the tree */
/* Laleh
template <typename Boundary>
inline
  Point3
OTree<Boundary>::center_p (int nodeid)
{
  Point3 center;
  int dim = 3;
  for(int i = 0; i < dim ; i++) {
    center(i) = centers[nodeid][i];
  }
}
*/
/* Return the Center of the given ntree */
template <typename Boundary>
inline
  Point3
OTree<Boundary>::center (int nodeid)
{
  vector<NodeTree>& ntree = this->node_data;
  Point3 center(0.0);
  int dim = 3;

  Point3 ll (center - Point3 (radius()));
  int tmp = pow2 (ntree[nodeid].depth);
  Index3 pathLcl (ntree[nodeid].path2node);
  Point3 res;
  for (int d = 0; d < dim; d++) {
    res(d) = ll(d) + (2 * radius()) * (pathLcl(d) + 0.5) / real_t(tmp);
  }
  return res;
}
/* Laleh
template <typename Boundary>
inline
  void
OTree<Boundary>::centroids (NodeTree& s)
{
  int dim = 4; // Including x,y,z and m as mass size
  vector<real_t> data_temp1(dim, 0.0);
  real_t total_mass = 0;

  for (size_t d = 0; d < dim; ++d)
    centers[s.index()][d] = 0;

  if (s.is_leaf()) {
    for (size_t i = s.begin(); i < s.end(); ++i) {
      for (size_t d = 0; d < dim-1; ++d)
        data_temp1[d] += data_perm[i][d] * data_perm[i][dim-1];
        total_mass += data_perm[i][dim-1];
    }
  }
  else {
    // recursively compute the center on the children
    for (int i= 0 ; i < 8; i++) {
      centroids(this->node_data[s.child+i]);
      for (size_t d = 0; d < dim-1; ++d) {
        data_temp1[d] += centers[s.child+i][d] * centers[s.child+i][dim-1];
        total_mass += centers[s.child+i][dim-1];
      }
     }
   }
   for (size_t d = 0; d < dim-1; ++d)
     centers[s.index()][d] = data_temp1[d]/ total_mass;
   centers[s.index()][dim-1] = total_mass;
}

*/
/* Return the child of given ntree */
template <typename Boundary>
inline
  int
OTree<Boundary>::child (int nodeid, const Index3& idx)
{
  vector<NodeTree>& ntree = this->node_data;
  assert (idx >= Index3(0) && idx < Index3(2));
  return ntree[nodeid].child + (idx(0) * 4 + idx(1) * 2 + idx(2));
}

template <typename Boundary>
inline
  int
OTree<Boundary>::findgnt(int wntdepth, const Index3& wntpath)
{
  int cur = 0;
  Index3 leftpath (wntpath);
  vector<NodeTree>& ntree = this->node_data;
  while (ntree[cur].depth < wntdepth && ntree[cur].child != -1) {
    int dif = wntdepth - ntree[cur].depth;
    int tmp = pow2 (dif - 1);
    Index3 choice (leftpath / tmp);
    leftpath -= choice * tmp;
    cur = child (cur, choice);
  }
  return cur;
}

/* Check whether two nodes are adjacent to each other or not */
template <typename Boundary>
inline
  bool
OTree<Boundary>::adjacent (int a, int b)
{
  vector<NodeTree>& ntree = this->node_data;
  int md = max (ntree[a].depth, ntree[b].depth);
  Index3 one(1);
  Index3 acenter ((2 * ntree[a].path2node + one) * pow2 (md - ntree[a].depth));
  Index3 bcenter ((2 * ntree[b].path2node + one) * pow2(md - ntree[b].depth));
  int aradius = pow2 (md - ntree[a].depth);
  int bradius = pow2 (md - ntree[b].depth);
  Index3 dif (abs(acenter - bcenter));
  int radius  = aradius + bradius;

  return
    (dif <= Index3 (radius)) &&
    (dif.linfty() == radius);
}

/* Top-down ordering of nodes/boxes */
template <typename Boundary>
inline
  int
OTree<Boundary>::order_topdown (vector<int>& boxes)
{
  vector<NodeTree>& ntree = this->node_data;
  boxes.clear();
  for (size_t i = 0; i < ntree.size(); i++)
    boxes.push_back (i);
  assert (boxes.size() == ntree.size());
  return 0;
}

/* Bottom-up ordering of nodes/boxes */
template <typename Boundary>
inline
  int
OTree<Boundary>::order_bottomup (vector<int>& boxes)
{
  vector<NodeTree>& ntree = this->node_data;
  boxes.clear();
  for (int i = ntree.size()-1; i >= 0; i--)
    boxes.push_back (i);
  assert (boxes.size() == ntree.size());
  return 0;
}
