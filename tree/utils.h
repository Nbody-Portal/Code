#ifndef INC_UTILS_H
#define INC_UTILS_H

#include <iostream>
#include <cassert>
#include <cstring>
#include <string>
#include <fstream>



/** Type of tree desired. */
enum class TreeType {kd_tree, ball_tree, octree, cover_tree};

/** Non-zero if single-tree search is desired */
static size_t single_ = 0;
/** Sources and targets coincide?. Must be 0 or 1. */
static size_t c_ = 0;
/** Number of points per leaf box. Must be > 0. */
static size_t leaf_size_ = 0;

/**
 *  \brief If global 'single_' is not yet set to a valid value,
 *  check if user requested a value via the environment variable,
 *  S, and otherwise return some default value.
 */
inline
 TreeType
getenv__tree_type (void)
{
  TreeType type_ = TreeType::kd_tree;
  const char* s = getenv ("TREE");
  if (s) {
    if (strcasecmp (s, "auto"))
      type_ = TreeType::kd_tree;
    else if (strcasecmp (s, "kd-tree"))
      type_ = TreeType::kd_tree;
    else if (strcasecmp (s, "ball-tree"))
      type_ = TreeType::ball_tree;
    else if (strcasecmp (s, "octree"))
      type_ = TreeType::octree;
    else if (strcasecmp (s, "cover-tree"))
      type_ = TreeType::cover_tree;
    else
      fprintf (stderr,
          "*** WARNING: TREE (='%s') is invalid. ***\n"
          "    Should be one of the following: auto, kd-tree,"
          "    ball-tree, octree, cover-tree.\n"
          "    Using default value: %s\n",
          s, "auto");
    }
  return type_;
}

/**
 *  \brief If global 'single_' is not yet set to a valid value,
 *  check if user requested a value via the environment variable,
 *  S, and otherwise return some default value.
 */
inline
 size_t
getenv__single_mode (void)
{
  if (!single_) {
    const char* s = getenv ("S");
    single_ = 0;
    if (s) {
      if (strcasecmp (s, "yes") == 0)
        single_ = 1;
      else {
        int v = atol (s);
        if (v >= 0)
          single_ = v;
        else {
          fprintf (stderr,
            "*** WARNING: S (='%s') is invalid. ***\n"
            "    Should be 'yes' or some value >= 0.\n"
            "    Using default value: %lu\n",
            s, single_);
        }
      }
    }
  }
  return single_;
}

/**
 *  \brief If global 'c_' is not yet set to a valid value,
 *  check if user requested a value via the environment variable,
 *  C, and otherwise return some default value.
 */
inline
 size_t
getenv__coincide (void)
{
  if (!c_) {
    c_ = 1; // default value
    const char* s = getenv ("C");
    if (s) {
      long t = atof (s);
      if (t < 0 || t > 1 ) {
        fprintf (stderr, "*** WARNING: C (=%ld) should be 0 or 1 ***\n", t);
        fprintf (stderr, "    Using default value: %lu\n", (unsigned long)c_);
      }
      else
        c_ = (size_t)t;
    }
  }
  return c_;
}

/**
 *  \brief If global 'leaf_size_' is not yet set to a valid value,
 *  check if user requested a value via the environment variable,
 *  LS, and otherwise return some default value.
 */
inline
 size_t
getenv__leaf_size (void)
{
  if (!leaf_size_) {
    leaf_size_ = 64; // default value
    const char* s = getenv ("LS");
    if (s) {
      long t = atof (s);
      if (t <= 0) {
        fprintf (stderr, "*** WARNING: LS (=%ld) should be > 0 ***\n", t);
        fprintf (stderr, "    Using default value: %lu\n",
                 (unsigned long)leaf_size_);
      }
      else // t > 0
        leaf_size_ = (size_t)t;
    }
  }
  return leaf_size_;
}


#include <random>
#include <type_traits>

namespace util {

static std::mt19937 default_generator;

template <typename T, class Enable = void>
struct random;

template <typename T>
struct random<T,
              typename std::enable_if<std::is_integral<T>::value>::type> {
  typedef T result_type;
  result_type operator()(T a, T b) const { return get(a,b); }
  result_type operator()()         const { return get();    }

  static result_type get(T a, T b) {
    std::uniform_int_distribution<T> dist(a, b);
    return dist(default_generator);
  }
  static result_type get() {
    return get(T(0), std::numeric_limits<T>::max());
  }
};

template <typename T>
struct random<T,
              typename std::enable_if<std::is_floating_point<T>::value>::type> {
  typedef T result_type;
  result_type operator()(T a, T b) const { return get(a,b); }
  result_type operator()()         const { return get();    }

  static result_type get(T a, T b) {
    std::uniform_real_distribution<T> dist(a, b);
    return dist(default_generator);
  }
  static result_type get() {
    return get(T(0), T(1));
  }
};

}

template <typename Points>
void from_file(Points& sources, const char* filename) {

  /* Read the data in from file */
  // WARNING, ERROR PRONE WITH #LINES AND #DIM
  fprintf (stderr, "Loading input dataset from file: %s\n", filename);
  {
    std::ifstream fs(filename);
    load_data(fs, sources, ',');
  }

#ifdef __DEBUG
  for (unsigned i = 0; i < sources.size(); ++i) {
    for (unsigned j = 0; j < sources[i].size(); ++j) {
      std::cout << sources[i][j] << " ";
    }
    std::cout << std::endl;
  }
#endif
}

template <typename Points>
void from_file(Points& sources, Points& targets,
               const char* filename1, const char* filename2) {
  /* Check if the source and target set of points are identical */
  int coincide = 1; //getenv__coincide();

  /* Read the data in from file */
  // WARNING, ERROR PRONE WITH #LINES AND #DIM
  fprintf (stderr, "Loading source dataset from file: %s\n", filename1);
  {
    std::ifstream fs(filename1);
    load_data(fs, sources, ',');
  }

#ifdef __DEBUG
  for (unsigned i = 0; i < sources.size(); ++i) {
    for (unsigned j = 0; j < sources[i].size(); ++j) {
      std::cout << sources[i][j] << " ";
    }
    std::cout << std::endl;
  }
#endif

  if (coincide) {
    fprintf (stderr, "Using same dataset for sources and targets.\n");
    targets = sources;
  } else {
    fprintf (stderr, "Loading target dataset from file: %s\n", filename2);
    std::ifstream fs(filename2);
    load_data(fs, targets, ',');
  }
}

template <typename Points>
void from_file_column_major(Points& sources, Points& targets,
               const char* filename1, const char* filename2) {
  /* Check if the source and target set of points are identical */
  int coincide = 1; //getenv__coincide();

  /* Read the data in from file */
  // WARNING, ERROR PRONE WITH #LINES AND #DIM
  fprintf (stderr, "Loading source dataset from file: %s\n", filename1);
  {
    std::ifstream fs(filename1);
    load_data_column_major(fs, sources, ',');
  }

#ifdef __DEBUG
  for (unsigned i = 0; i < sources.size(); ++i) {
    for (unsigned j = 0; j < sources[i].size(); ++j) {
      std::cout << sources[i][j] << " ";
    }
    std::cout << std::endl;
  }
#endif

  if (coincide) {
    fprintf (stderr, "Using same dataset for sources and targets.\n");
    targets = sources;
  } else {
    fprintf (stderr, "Loading target dataset from file: %s\n", filename2);
    std::ifstream fs(filename2);
    load_data_column_major(fs, targets, ',');
  }
}

#endif
