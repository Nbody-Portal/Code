#include <iostream>
#include "Nbody.h"
#include "utils.h"

using namespace std;
using namespace Nbody;

static inline
  void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <N> <dim> \n", use);
}

int main (int argc, char** argv)
{
  unsigned int num_pts;
  int dim;

  if (argc != 3) {
    usage__ (argv[0]);
    return -1;
  }

  /** Command line input */
  num_pts = atoi (argv[1]);
  dim = atoi (argv[2]);

  Var x;
  RDom r(0, 10);
  Func f;

  cout << "Reduction function...\n";
  f(x) = x*x;
  f(x) += x+r;

  //cout << "Vectorizing loop...\n";
  //kernel.vectorize(x,4);

  cout << "Computing function to lowered form...\n";
  const char* result_file = "reduction_lowered.stmt";
  f.compile_to_lowered_form(result_file);

  cout << "Evaluating function...\n";
  Points<int32_t> output = f.evaluate(num_pts);

  /* Check output */
  for (int i = 0; i < output.m(); ++i) {
    int sum = i*i;
    for (int j = 0; j < 10; ++j)
      sum += i + j;
    cout << i << ": " << output(i) << " " << sum  << '\n';
  }

  cout << "Passed." << endl;
  return 0;
}
