#!/usr/bin/env python

import sys
import csv
import time
sys.path.append("/usr/lib64/python3.4/site-packages/")
import numpy as np
import sklearn
from sklearn.neighbors import KDTree
# from sklearn import tree

fileName = "/home/real_datasets/G2-D2-1000000.csv"
Distance = 1


def main():

    np.random.seed(0)
    #X = np.random.random((30, 3))
    #X = np.genfromtxt("/home/real_datasets/G2-D2-5000.csv", delimiter=',', names=True)
    X = np.array(list(csv.reader(open(fileName, "r"), delimiter=","))).astype("float")
    #print (g)
    #r = np.linspace(0, 1, 1)
    r = Distance
    tree = KDTree(X)
    # tree = BallTree(X)
    # tree = sklearn.neighbors.KDTree(X)
    start_time = time.time()
    a = tree.two_point_correlation(X, r)
    print("--- %s seconds ---" % (time.time() - start_time))
    print (a)

if __name__ == "__main__":
  main()
