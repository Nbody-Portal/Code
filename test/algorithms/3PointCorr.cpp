#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;


static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filename1> <filename2> <filename3> <correlationValue>  \n", use);
}




int main (int argc, char** argv){

       if (argc != 5) {
            usage__ (argv[0]);
            return -1;
        }

        /* input the target dataset */
        std::string filePathT = argv[1];
        Storage fileObjT(filePathT);

        /* input the source dataset */
        std::string filePathS = argv[2];
        Storage fileObjS(filePathS);

        /* input the second source dataset */
        std::string filePathSS = argv[3];
        Storage fileObjSS(filePathSS);

        int h1 = atoi(argv[4]);

        Clock timer;

        /* defining the kernel funcion */
        // Var a;
        // Var b;
        // Var c;
        // Expr EuclidDist = sqrt(pow((a-b),2));

        Expr a = Variable::make("outterSet");
        Expr b = Variable::make("innerSet");
        Expr c = Variable::make("innerSet2");
        Expr q1 = Integer::make(h1);
        // Expr q1 = Flt::make(h1);

        // Expr CorrEuclidDist = ((sqrt(pow((a-b),2) < q1))* (sqrt(pow((a-c),2) < q1)) ) * (sqrt(pow((b-c),2) < q1));
        Expr CorrEuclidDist =  (sqrt(((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1]))) < q1 )*
                                (sqrt(((a[0] - c[0]) * (a[0] - c[0])) + ((a[1] - c[1]) * (a[1] - c[1]))) <q1 )*
                                (sqrt(((c[0] - b[0]) * (c[0] - b[0])) + ((c[1] - b[1]) * (c[1] - b[1]))) );
        // Expr CorrEuclidDist =  (sqrt(((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1])))  < q1) *
        //                         (sqrt(((a[0] - c[0]) * (a[0] - c[0])) + ((a[1] - c[1]) * (a[1] - c[1])))  < q1) *
        //                         (sqrt(((c[0] - b[0]) * (c[0] - b[0])) + ((c[1] - b[1]) * (c[1] - b[1])))  < q1);

        // Expr CorrEuclidDist = a[0] + b[0] + c[0];
        /* defining the N-body expression using portal IRs*/
        PortalExpr expr;
        // expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , a , fileObjT);
        // expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , b , fileObjS);
        // expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , c , fileObjSS , CorrEuclidDist);

        expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "outterSet" , fileObjT);
        expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSet" , fileObjS);
        expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSet2" , fileObjSS , CorrEuclidDist);

        const char* result_file = "test/3PointCorr.stmt";
        expr.compile_to_lowered_form(result_file);


        /* N-body caculation with portal IRs using the tree traversal */
        timer.start();
        expr.executeTraverse();
        cout << "Total time: " << timer.seconds() << endl;

        Storage output = expr.getOutput();
        bool valid = expr.Validate(output);

        if (valid) {
          cout << "Passed!" << endl;
        }
        else {
          cout << "Failed!" << endl;
        }

	return 0;
}
