#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "utils.h"
#include "Hrect_bounds.h"
#include "boruvka_emst.h"
#include "Clock.hpp"

using namespace Nbody;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s  <filenameT>  <N> <dim>\n", use);
}

/* this function just compares the result of two different version of computation such as bruteforce vs traversal in portal*/
bool validate(Storage result, Storage compute) {
    bool valid = true;
    int failedPoints = 0;
    for (int i = 0; i < result.size(); i++) {
      bool validPoint = true;
      for(int j = 0; j < result.pointSize(); j++) {
         if (result[i][j] != compute[i][j] ) {
            cout <<  "correct and computed values: " << i << ", " << j << ":" <<result[i][j] << " , " << compute[i][j] << endl;
            valid =  false;
            validPoint = false;
         }
      if (!validPoint)
        failedPoints++;

      }
    }
    cout << "Point Failed: " << failedPoints << endl;
    return valid;
}



class kNN_distance {
  public:
    real_t squared_distance;  /* Squared distance of the referenced element to an implicit point */
    int index;  /* Index of the feature vector in the data set */

    /* Default and convenience constructors */
    kNN_distance() : squared_distance(DBL_MAX), index(-1) {}
    kNN_distance(int i, real_t sq_dist) : squared_distance(sq_dist), index(i) {}

    /* Distance comparison operator for kNN_distances. Allows kNN_distance objects to be used as STL comparison functors */
    bool operator< (const kNN_distance &v) const {
      return squared_distance < v.squared_distance;
    }
};

typedef vector<kNN_distance> kNeighbors;
typedef vector<kNeighbors> resultNeighbors;


/* compares the result with the C++ computation */
bool validateCBruteForce(double** results, Storage data , int num_pts, int dim ){

  bool error = false;
  const real_t tolerance = 1e-2;

  std::vector<EdgePair> edges;
  edges.reserve(num_pts-1);
  Mat naive_results(num_pts-1, 3);

  Neighbors neighbors(num_pts);
  for (size_t i = 0; i < num_pts; ++i)
    neighbors[i].squared_distance = DBL_MAX;
  UnionFind connections(num_pts);

  /* Compute the naive minimum spanning tree */
  while (edges.size() < (num_pts - 1)) {
    for (size_t i = 0; i < num_pts; ++i) {
      /* Find the query component index */
      size_t query_component_index = connections.find(i);
      for (size_t j = 0; j < num_pts; ++j) {
        /* Find the reference component index */
        size_t reference_component_index = connections.find(j);
        /* Check if the points are in different components and if so, compute distance between them */
        if (query_component_index != reference_component_index) {
          real_t dist = 0.0;
          for (size_t d = 0; d < dim; d++)
            dist += (data[j][d] - data[i][d]) * (data[j][d] - data[i][d]);
          dist = sqrt(dist);
          /* Update current nearest neighbor */
          if (dist < neighbors[query_component_index].squared_distance) {
            assert (i != j);
            neighbors[query_component_index] = ComponentData(i, j, dist);
          }
        }
      }
    }
    /* Add all edges found to the list of neighbors */
    for (size_t i = 0; i < num_pts; ++i) {
      size_t component = connections.find(i);
      size_t inedge = neighbors[component].in;
      size_t outedge = neighbors[component].out;
      if (connections.find(inedge) != connections.find(outedge)) {
        if (inedge < outedge)
          edges.push_back(EdgePair(inedge, outedge, neighbors[component].squared_distance));
        else
          edges.push_back(EdgePair(outedge, inedge, neighbors[component].squared_distance));
        connections.merge(inedge, outedge);
      }
    }
    /* Reset the neighbors after each iteration */
    for (size_t i = 0; i < num_pts; ++i)
      neighbors[i].squared_distance = DBL_MAX;
  }
  /* Sort the edges */
  std::sort (edges.begin(), edges.end(), CompEdges);
  /* Output the results */
  for (size_t i = 0; i < edges.size(); ++i) {
    naive_results[i][0] = edges[i].first();
    naive_results[i][1] = edges[i].second();
    naive_results[i][2] = edges[i].distance();
  }

  /* Compare naive emst with the fast tree algorithm */
  for (size_t i = 0; i < naive_results.size(); ++i) {
    if (!(naive_results[i][0] != results[i][0] ||
        naive_results[i][1] != results[i][1] ||
        fabs(naive_results[i][2] - results[i][2]) < tolerance)) {
          cerr << "Correct in edge: " << naive_results[i][0] << endl;
          cerr << "Correct out edge: " << naive_results[i][1] << endl;
          cerr << "Correct distance: " << naive_results[i][2] << endl;
          cerr << "Computed in edge: " << results[i][0] << endl;
          cerr << "Computed out edge: " << results[i][1] << endl;
          cerr << "Computed distance: " << results[i][2] << endl;
          error = true;
          exit(0);
    }
  }
  return error;

}




int main (int argc, char** argv){

  if (argc != 4) {
      usage__ (argv[0]);
      return -1;
  }

  /* input the target dataset */
  std::string filePathT = argv[1];
  Storage fileObjT(filePathT);


  int num_pts  = atoi(argv[2]);
  int dim  = atoi(argv[3]);


  std::vector<EdgePair> edges;
  edges.reserve(num_pts-1);
  Mat naive_results(num_pts-1, 3);

  Neighbors neighbors(num_pts);
  for (size_t i = 0; i < num_pts; ++i)
    neighbors[i].squared_distance = DBL_MAX;
  UnionFind connections(num_pts);
  Storage output, outputArg;


    /* defining the kernel funcion */
    Var a;
    Var b;
    Expr EuclidDist = sqrt(pow((a-b),2));

    /* defining the N-body expression using portal IRs*/
    PortalExpr expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , a, fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::MIN) , b , fileObjT , EuclidDist);

    PortalExpr exprArg;
    exprArg.addLayer(PortalOperator(PortalOperator::OP::FORALL) , a, fileObjT);
    exprArg.addLayer(PortalOperator(PortalOperator::OP::ARGMIN) , b , fileObjT , EuclidDist);

    /* Compute the naive minimum spanning tree */
    while (edges.size() < (num_pts - 1)) {

    /* N-body caculation with portal IRs using brute force calculations */
    expr.execute();
    output = expr.getOutput();

    exprArg.execute();
    outputArg = exprArg.getOutput();

    for (size_t i = 0; i < num_pts; ++i) {
      /* Find the query component index */
      size_t query_component_index = connections.find(i);
          /* Find the reference component index */
        size_t reference_component_index = connections.find(outputArg[i][0]);
        if ((query_component_index != reference_component_index)  && (i != outputArg[i][0])){
              neighbors[query_component_index] = ComponentData(i, outputArg[i][0], output[i][0]);

        }
    }

    /* Add all edges found to the list of neighbors */
    for (size_t i = 0; i < num_pts; ++i) {
      size_t component = connections.find(i);
      size_t inedge = neighbors[component].in;
      size_t outedge = neighbors[component].out;
      if (!((inedge > num_pts-1) || (outedge > num_pts-1))){
        if (connections.find(inedge) != connections.find(outedge)) {
          if (inedge < outedge)
            edges.push_back(EdgePair(inedge, outedge, neighbors[component].squared_distance));
          else
            edges.push_back(EdgePair(outedge, inedge, neighbors[component].squared_distance));
          connections.merge(inedge, outedge);
        }
      }
    }
    /* Reset the neighbors after each iteration */
    for (size_t i = 0; i < num_pts; ++i)
      neighbors[i].squared_distance = DBL_MAX;
  }


  /* Sort the edges */
  std::sort (edges.begin(), edges.end(), CompEdges);
  /* Output the results */
  double** out = new double*[edges.size()];
  for(size_t j = 0; j < edges.size(); ++j)
    out[j] = new double[3];
  for (size_t i = 0; i < edges.size(); ++i) {
    out[i][0] = edges[i].first();
    out[i][1] = edges[i].second();
    out[i][2] = edges[i].distance();
  }



    bool valid = !validateCBruteForce(out,fileObjT, num_pts, dim);

    if (valid) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }

    return 0;
}
