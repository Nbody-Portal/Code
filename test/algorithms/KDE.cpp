#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "JITModule.h"
#include <math.h>
#include "Clock.hpp"
// #include "Gaussian_kernel.h"


//using namespace std;
using namespace Nbody;
// typedef GaussianKernel KernelType;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <kernelBandwidth>\n", use);
}


int main (int argc, char** argv){

	if (argc != 4) {
            usage__ (argv[0]);
            return -1;
    }

    std::string filePathT = argv[1];
    Storage fileObjT(filePathT);


    std::string filePathS = argv[2];
    Storage fileObjS(filePathS);

    int kernelBandwidth = atoi(argv[3]);

    int dim = 2;
    Clock timer;


    Var a;
    Var b;

    Expr norm = 1.0 / pow((sqrt(2.0 * M_PI) * kernelBandwidth), dim);
    Expr Dist = exp(-0.5 * pow(kernelBandwidth, -2.0) * (sqrt(pow((a-b),2))));

    Expr q = Integer::make(fileObjS.size());
    Expr outer =  1 / q;

    // Expr p   =  Variable::make("coefficinet");
    // Expr cov = Variable::make("covariance");
    // Expr Gauss = Gaussian(a,b,Cov,1)
    // expr.attachStorage( "coefficinet", filePathCoef);
    // expr.attachStorage( "covariance", filePathCov);;

    PortalExpr expr;
    expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , a, fileObjT, outer);
    // expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , a , fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , b , fileObjS , Dist);


    const char* result_file = "test/KDE.stmt";
    expr.compile_to_lowered_form(result_file);


    timer.start();
    expr.executeTraverse();
    cout << "Total time: " << timer.seconds() << endl;

    Storage output = expr.getOutput();
    bool valid = expr.Validate(output);
    // bool valid2 = expr.FastValidate(output);

    if (valid) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }

	return 0;
}
