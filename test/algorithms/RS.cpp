#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;


static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <min> <max> \n", use);
}



int main (int argc, char** argv){

       if (argc != 5) {
            usage__ (argv[0]);
            return -1;
        }

        /* input the target dataset */
        std::string filePathT = argv[1];
        Storage fileObjT(filePathT);

        /* input the source dataset */
        std::string filePathS = argv[2];
        Storage fileObjS(filePathS);

        int h1 = atoi(argv[3]);
        int h2 = atoi(argv[4]);

        Clock timer;

        /* defining the kernel funcion */
        Var a;
        Var b;
        Expr q1 = Integer::make(h1);
        Expr q2 = Integer::make(h2);
        Expr UnionEuclidDist =  ((sqrt(pow((a-b),2))> q1) && (sqrt(pow((a-b),2))< q2));


        /* defining the N-body expression using portal IRs*/
        PortalExpr expr;
        expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , a , fileObjT);
        expr.addLayer(PortalOperator(PortalOperator::OP::UNIONARG) , b , fileObjS , UnionEuclidDist);

        const char* result_file = "test/RangeSearch.stmt";
        expr.compile_to_lowered_form(result_file);


        /* N-body caculation with portal IRs using the tree traversal */
        timer.start();
        expr.executeTraverse();
        cout << "Total time: " << timer.seconds() << endl;

        Storage output = expr.getOutput();
        bool valid = expr.Validate(output);

        if (valid) {
          cout << "Passed!" << endl;
        }
        else {
          cout << "Failed!" << endl;
        }

	return 0;
}
