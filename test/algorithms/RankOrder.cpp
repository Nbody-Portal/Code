#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <K> <filenameT> <filenameS> \n", use);
}

extern "C"
float RankDistance(float * s1 , float * s2) {

  int K = 300;
	int intersect = 0;
	int find=0;
  int threshold = 30;
  int d = K;
	for (size_t i = 1; i < K+1; i++) {
			for (size_t j = 1; j < K+1; j++) {
				if (s1[i] == s2[j])
				intersect++;
			}
			if ((s1[0] == s2[i])|| (s2[0] == s2[i]))
				find++;

	}
	if (find == 2)
		d = K-intersect;
	return (K<threshold);
}

int main (int argc, char** argv){

    if (argc != 4) {
        usage__ (argv[0]);
        return -1;
    }
    bool valid;
    /* K represents the number of neighbors */
    int K  = atoi(argv[1]);
    /* input the target dataset */
    std::string filePathT = argv[2];
    Storage fileObjT(filePathT);

    /* input the source dataset */
    std::string filePathS = argv[3];
    Storage fileObjS(filePathS);
    int dim = fileObjS.pointSize();
    Clock timer;


  /* defining the kernel funcion */
   // Expr a = Variable::make("outterSet");
   // Expr b = Variable::make("innerSet");

   /* defining the N-body expression using portal IRs*/
   PortalExpr expr;
   expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , fileObjT);
   expr.addLayer(PortalOperator(PortalOperator::OP::KMIN, K) , fileObjS , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));

   const char* result_file = "test/KNN.stmt";
   expr.compile_to_lowered_form(result_file);

    /* N-body caculation with portal IRs using the tree traversal */
    timer.start();

    // expr.execute();
    expr.executeTraverse();
    cout << "Total time: " << timer.seconds() << endl;
    Storage images = expr.getOutput();

    PortalExpr rankorder;
    rankorder.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet2", images);
    rankorder.addLayer(PortalOperator(PortalOperator::OP::UNIONARG) , "innerSet2", images, reinterpret_cast<void*> (&RankDistance));

    const char* result_file_RO = "test/RO.stmt";
    rankorder.compile_to_lowered_form(result_file_RO);

    rankorder.execute();
    Storage output = rankorder.getOutput();


    return 0;
}
