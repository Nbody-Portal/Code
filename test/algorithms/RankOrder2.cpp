#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <K> <filenameT> <filenameS> \n", use);
}

int main (int argc, char** argv){

    if (argc != 4) {
        usage__ (argv[0]);
        return -1;
    }
    bool valid;
    /* K represents the number of neighbors */
    int K  = atoi(argv[1]);
    int threshold = 30;
    /* input the target dataset */
    std::string filePathT = argv[2];
    Storage fileObjT(filePathT);

    /* input the source dataset */
    std::string filePathS = argv[3];
    Storage fileObjS(filePathS);
    int dim = fileObjS.pointSize();
    Clock timer;


   /* defining the N-body expression using portal IRs*/
   PortalExpr expr;
   expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , fileObjT);
   expr.addLayer(PortalOperator(PortalOperator::OP::KMIN, K) , fileObjS , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));

   const char* result_file = "test/KNN.stmt";
   expr.compile_to_lowered_form(result_file);

    /* N-body caculation with portal IRs using the tree traversal */
    timer.start();

    // expr.execute();
    expr.executeTraverse();
    cout << "Total time: " << timer.seconds() << endl;
    Storage images = expr.getOutput();

    /* defining the kernel funcion */
     // Expr a = Variable::make("outterSet");
     // Expr b = Variable::make("innerSet");
    Var a,b;
    Expr RankDistance = (exist(a,b) && exist(b,a)) && (intersection(a,b) < threshold);


    PortalExpr rankorder;
    rankorder.addLayer(PortalOperator(PortalOperator::OP::FORALL), a , images);
    rankorder.addLayer(PortalOperator(PortalOperator::OP::UNIONARG) , b , images, RankDistance);

    const char* result_file_RO = "test/RO2.stmt";
    rankorder.compile_to_lowered_form(result_file_RO);

    // rankorder.executeCoverTraverse();
    rankorder.executeTraverse();
    Storage output = rankorder.getOutput();


    return 0;
}
