#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "JITModule.h"
#include <math.h>
#include <assert.h>
#include "Clock.hpp"

//using namespace std;
using namespace Nbody;


extern "C"
float EucDist(float * s1 , float * s2) {
	float x = s1[0] - s2[0];
	float y = s1[1] - s2[1];
	x *= x;
	y *= y;
	return std::sqrt(x + y);
}

int main (int argc, char** argv){
	Storage reference("DataTest/reference.csv");
	Storage query("DataTest/query.csv");

	/**
		Operator Correctness Tests

		ForAll of Forall
		[[3, 3.16228, 4.12311, 9.21954]
		[4.12311, 3.16228, 3, 3.60555]
		[7.28011, 5.65685, 6.08276, 2.23607]
		[3.60555, 1.41421, 4.12311, 8.06226]
		[2.5, 3.04138, 3.64005, 8.8459]
		[12.53, 11.3137, 11.1803, 5.38516]]

	**/


	//K Operators
	PortalExpr ForAllOfKMax;
	ForAllOfKMax.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setOne" , reference);
	ForAllOfKMax.addLayer(PortalOperator(PortalOperator::OP::KMAX , 3) , "setTwo" , query , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));

	ForAllOfKMax.execute();

	assert(ForAllOfKMax.getOutput() == Storage("DataTest/ForAllKMax.csv") && "ForAll of KMax failed!");

	PortalExpr ForAllOfKMin;
	ForAllOfKMin.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setOne" , reference);
	ForAllOfKMin.addLayer(PortalOperator(PortalOperator::OP::KMIN , 3) , "setTwo" , query , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));

	ForAllOfKMin.execute();

	ForAllOfKMin.getOutput().toFile("DataTest/test.csv");

	assert(ForAllOfKMin.getOutput() == Storage("DataTest/ForAllKMin.csv") && "ForAll of KMax failed!");

	std::cout << "K Operator tests passed!";


	/**
		Storage Tests
	**/


	///Set Conversions
	std::string filePath1 = "/home/real_datasets/synthetic/point/G2-D2-10000.csv";

	Storage fileObj1(filePath1);

	assert(fileObj1 == Storage(fileObj1.points()) && "Storage to Points Conversion Check");
	std::cout << "Storage to Points Conversion Check Passed!\n";

	assert(fileObj1 == Storage(fileObj1.getVectorOfArray()) && "Storage to Vector Of Array Conversion Check");
	std::cout << "Storage to Vector of Array Conversion Check Passed!\n";

	std::string filePath2 = "/home/real_datasets/synthetic/point/G2-D2-50000.csv";
	Storage fileObj2(filePath2);

	///Set Creation
	std::vector<std::vector<float>> ssoVectorCreationObj;
	std::vector<float> ssoVectorCreationRow1;
	std::vector<float> ssoVectorCreationRow2;

	ssoVectorCreationRow1.push_back(0);
	ssoVectorCreationRow1.push_back(1);
	ssoVectorCreationRow2.push_back(1);
	ssoVectorCreationRow2.push_back(2);

	ssoVectorCreationObj.push_back(ssoVectorCreationRow1);
	ssoVectorCreationObj.push_back(ssoVectorCreationRow2);

	Storage vectorCreationObj(ssoVectorCreationObj);
	assert((vectorCreationObj[0][0] == 0 && vectorCreationObj[0][1] == 1) && "Storage Vector Creation Check");
	assert((vectorCreationObj[1][0] == 1 && vectorCreationObj[1][1] == 2) && "Storage Vector Creation Check");

	/**
		Kernel Function Input Types Tests
	**/

	PortalExpr EucDistPortalFunctionExpression;
	PortalExpr EucDistExternalFunctionExpression;
	PortalExpr EucDistLoweredFunctionExpression;

	EucDistPortalFunctionExpression.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setOne" , reference);
	EucDistPortalFunctionExpression.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setTwo" , query , PortalFunction(PortalFunction::TYPE::EUCLIDEAN));

	EucDistExternalFunctionExpression.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setOne" , reference);
	EucDistExternalFunctionExpression.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setTwo" , query , reinterpret_cast<void*>(&EucDist));

	Expr a = Variable::make("setOne");
	Expr b = Variable::make("setTwo");
	Expr calc = sqrt(pow(a[0] - b[0] , 2) + pow(a[1] - b[1] , 2));
	EucDistLoweredFunctionExpression.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setOne" , reference);
	EucDistLoweredFunctionExpression.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setTwo" , query , calc);

	Clock timer;
	timer.start();
	EucDistPortalFunctionExpression.execute();
	cout << "PortalFunctionExpression:" << timer.seconds() << endl;
	timer.start();
	EucDistExternalFunctionExpression.execute();
	cout << "ExternalFunctionExpression:" << timer.seconds() << endl;
	timer.start();
	EucDistLoweredFunctionExpression.execute();
	cout << "LoweredFunctionExpression:" << timer.seconds() << endl;

	assert(EucDistPortalFunctionExpression.getOutput() == EucDistExternalFunctionExpression.getOutput() && "PortalFunction and ExternalFunction not producing same output!");
	assert(EucDistPortalFunctionExpression.getOutput() == EucDistLoweredFunctionExpression.getOutput() && "PortalFunction and LoweredFunction not producing same output!");
	std::cout << "All Function Types are producing results consistent with each other!\n";

	///Testing adding external storage
	SSO extStoreVar = SSO::make("ssovco");
	Expr extStoreCalc1 = extStoreVar[1][1] * calc;
	Expr extStoreCalc2 = (extStoreVar[0][1] + extStoreVar[1][0]) * calc;

	PortalExpr ExtStoreExpression1;
	PortalExpr ExtStoreExpression2;

	ExtStoreExpression1.attachStorage("ssovco" , vectorCreationObj);
	ExtStoreExpression1.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setOne" , reference);
	ExtStoreExpression1.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setTwo" , query , extStoreCalc1);

	ExtStoreExpression2.attachStorage("ssovco" , vectorCreationObj);
	ExtStoreExpression2.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "setOne" , reference);
	ExtStoreExpression2.addLayer(PortalOperator(PortalOperator::OP::MIN) , "setTwo" , query , extStoreCalc2);

	ExtStoreExpression1.execute();
	ExtStoreExpression2.execute();

	assert(ExtStoreExpression1.getOutput() == ExtStoreExpression2.getOutput() && "Attaching Storage to Expr Not Working!");





	std::cout << "Kernel Function Tests All Pass!";





	return 0;
}
