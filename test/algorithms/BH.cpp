#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "JITModule.h"
#include <math.h>
#include "Clock.hpp"

//using namespace std;
using namespace Nbody;
// typedef GaussianKernel KernelType;

static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <threshold> <treeType: 0 - kdtree, 1 - octree >\n", use);
}


int main (int argc, char** argv){

	if (argc != 5) {
            usage__ (argv[0]);
            return -1;
    }
    cout << "test 1 \n";
    std::string filePathT = argv[1];
    Storage fileObjT(filePathT);
    // fileObjT.set_tree_model(atoi(4));

    cout << "test 2 \n";
    std::string filePathS = argv[2];
    Storage fileObjS(filePathS);
    // fileObjS.set_tree_model(atoi(4));
    cout << "test 3 \n";
    real_t threshold = atof(argv[3]);
    Clock timer;
    PortalExpr expr;
    // expr.set_tree_model(atoi(4));

    cout << "test 4 \n";
    Var a;
    Var b;
    Expr gravitational_constant = Flt::make(6.67e-11);
    Expr regulizer = Flt::make(6.67e-14);

    cout << "test 5 \n";
    /* computing Potential instead of 3 dimenstion of forces */
    Expr denum = sqrt(pow(a[0] - b[0] , 2) + pow(a[1] - b[1] , 2) + pow(a[2] - b[2] , 2)) + regulizer ;
    // Expr denum = sqrt((a - b) * (a - b)) + regulizer ;
    Expr force = gravitational_constant * (a[1] * b[1])/denum;
    // Expr force = gravitational_constant * (a[3] * b[3])/denum;


    cout << "test 6 \n";
    expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
    expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSet" , fileObjS , force);

    cout << "test 7 \n";
    const char* result_file = "test/BH.stmt";
    expr.compile_to_lowered_form(result_file);

    cout << "test 8 \n";
    timer.start();
    expr.executeTraverse();
    cout << "Total time: " << timer.seconds() << endl;

    cout << "test 9 \n";
    Storage output = expr.getOutput();
    bool valid = expr.Validate(output);


    cout << "test 10 \n";
    if (!(valid)) {
      cout << "Passed!" << endl;
    }
    else {
      cout << "Failed!" << endl;
    }


	return 0;
}
