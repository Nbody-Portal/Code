#include <iostream>
#include "Nbody.h"
#include "PortalExpr.h"
#include "PortalOperator.h"
#include "Storage.h"
#include "Data.h"
#include "Clock.hpp"

using namespace Nbody;


static inline
void
usage__ (const char* use)
{
  fprintf (stderr, "usage: %s <filenameT> <filenameS> <finite_distance>\n", use);
}

/* this function just compares the result of two different version of computation such as bruteforce vs traversal*/
bool validate(Storage result, Storage compute) {
        bool valid = true;
        for (int i = 0; i < result.size(); i++) {
          for(int j = 0; j < result.pointSize(); j++) {
             if (result[i][j] != compute[i][j] )
                 return false;
          }
        }
        return valid;
}


/* compares the result with the C++ computation of RangeSearch */
bool validateCBruteForce (Storage result, Storage outter, Storage inner , int h1){

  bool error = false;
  /* Set tolerance value for validation */
  const real_t tolerance = 1e-2;

  /* Evaluate test/target point one by one */
  for (int i = 0; i < outter.size() ; i++) {
    /* Calculate brute-force nearest neighbor for validation */
    double force = 0;
    for (int j = 0; j < inner.size() ; j++) {
      real_t dist = 0.0;
      for (int d = 0; d < inner.pointSize(); d++)
        dist += (outter[j][d]-inner[i][d]) * (outter[j][d]-inner[i][d]);
      force += pow((h1/sqrt(dist)), 12) - pow((h1/sqrt(dist)), 12);
    }
    if (fabs(result[i][0] - force) >= tolerance)
      error = true;
  }
  return error;
}




int main (int argc, char** argv){

       if (argc != 4) {
            usage__ (argv[0]);
            return -1;
        }

        /* input the target dataset */
        std::string filePathT = argv[1];
        Storage fileObjT(filePathT);

        /* input the source dataset */
        std::string filePathS = argv[2];
        Storage fileObjS(filePathS);

        int h1 = atoi(argv[3]);


        Clock timer;

        /* defining the kernel funcion */
        Expr a = Variable::make("outterSet");
        Expr b = Variable::make("innerSet");
        Expr q1 = Flt::make(h1);


        Expr Kernel = sqrt(((a[0] - b[0]) * (a[0] - b[0])) + ((a[1] - b[1]) * (a[1] - b[1])));
        Expr KernelFunc = pow((q1/Kernel),12) - pow((q1/Kernel),6);

        /* defining the N-body expression using portal IRs*/
        PortalExpr expr;
        expr.addLayer(PortalOperator(PortalOperator::OP::FORALL) , "outterSet", fileObjT);
        expr.addLayer(PortalOperator(PortalOperator::OP::SUM) , "innerSet" , fileObjS , KernelFunc);

        const char* result_file = "test/Lennard-Jones.stmt";
        expr.compile_to_lowered_form(result_file);


        /* N-body caculation with portal IRs using the tree traversal */
        timer.start();
        expr.executeTraverse();
        Storage output1 = expr.getOutput();
        cout << "Total time: " << timer.seconds() << endl;

        bool valid = validateCBruteForce(output1, fileObjT, fileObjS, h1);

        if (!valid) {
          cout << "Passed!" << endl;
        }
        else {
          cout << "Failed!" << endl;
        }

	return 0;
}
